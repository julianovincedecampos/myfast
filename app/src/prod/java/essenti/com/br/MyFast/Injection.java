package essenti.com.br.MyFast;

import android.content.Context;
import android.support.annotation.NonNull;

import essenti.com.br.MyFast.a_migration.data.cobrancas.CobrancaRepositories;
import essenti.com.br.MyFast.a_migration.data.cobrancas.CobrancaService;

/**
 * @author Filipe Bezerra
 */

public class Injection {
    public static CobrancaService provideCobrancaService(@NonNull Context context) {
        return CobrancaRepositories.getService(context);
    }
}

package essenti.com.br.MyFast.app;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import butterknife.BindView;
import butterknife.OnClick;
import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.android.activity.ActivityNavigator;
import essenti.com.br.MyFast.android.activity.BaseActivity;
import essenti.com.br.MyFast.android.widget.MensagemDialogo.OnMensagemClickListener;
import essenti.com.br.MyFast.usuario.UsuarioHelper;

/**
 * @author Filipe Bezerra
 */
public class PainelActivity extends BaseActivity {
    @BindView(R.id.toolbar) Toolbar mToolbar;

    @Override
    protected int provideLayoutResource() {
        return R.layout.activity_painel;
    }

    @Override
    protected int provideUpIndicator() {
        return R.drawable.ic_dashboard;
    }

    @Override
    protected boolean shouldDisplayHomeAsUpEnabled() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        App.getInstancia()
                .exibirQuestionamento(this, "Atenção", "Deseja sair?",
                        new OnMensagemClickListener() {
                            @Override
                            public void onConfirmarClick() {
                                UsuarioHelper.logoutUser(PainelActivity.this);
                                PainelActivity.super.onBackPressed();
                            }

                            @Override
                            public void onCancelarClick() {}
                        });
    }

    @OnClick({ R.id.btnRelatorios, R.id.btnAtendimento, R.id.btnTabelaPrecos })
    void onClickButtons(View view) {
        switch (view.getId()) {
            case R.id.btnRelatorios: {
                ActivityNavigator.startRelatoriosActivity(this);
                break;
            }
            case R.id.btnAtendimento: {
                ActivityNavigator.startMesasActivity(this);
                break;
            }
            case R.id.btnTabelaPrecos: {
                ActivityNavigator.startConsultaTabelaPrecoActivity(this);
            }
        }
    }
}

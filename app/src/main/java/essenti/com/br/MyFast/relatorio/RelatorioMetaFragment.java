package essenti.com.br.MyFast.relatorio;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.NumberFormat;

import butterknife.ButterKnife;
import essenti.com.br.MyFast.R;

public class RelatorioMetaFragment extends Fragment {
    private static final NumberFormat NUMBER_FORMAT = NumberFormat.getCurrencyInstance();

    /*@Bind(R.id.progressMeta) ArcProgress mProgressMetaAtingida;
    @Bind(R.id.somaTotalVenda) TextView mTextSomaTotalVenda;
    @Bind(R.id.somaComissao) TextView mTextSomaComissao;*/

    public RelatorioMetaFragment() {
    }

    public static RelatorioMetaFragment newInstance() {
        return new RelatorioMetaFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View fragmentView = inflater.inflate(R.layout.fragment_relatorio_meta, container, false);
        ButterKnife.bind(this, fragmentView);
        return fragmentView;
    }

    private void updateGoalData(/*Meta meta*/) {
        /*final Double valorMeta = meta.getValorMeta();
        final Double totalVendasRealizada = meta.getTotalVendasRealizada();

        float porcentagem = (float) ((valorMeta / totalVendasRealizada) * 100);

        mProgressMetaAtingida.setProgress((int) porcentagem);
        mTextSomaTotalVenda.setText(NUMBER_FORMAT.format(totalVendasRealizada));
        mTextSomaComissao.setText(NUMBER_FORMAT.format(valorMeta));*/
    }

    public void updateData(/*Meta meta*/) {
        updateGoalData(/*meta*/);
    }
}

package essenti.com.br.MyFast.mesa;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;

import essenti.com.br.MyFast.util.FeedbackHelper;
import essenti.com.br.MyFast.util.NetworkUtil;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import butterknife.BindView;
import butterknife.ButterKnife;
import essenti.com.br.MyFast.app.App;
import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.android.activity.ActivityNavigator;
import essenti.com.br.MyFast.produto.ProdutosActivity;
import essenti.com.br.MyFast.android.recyclerview.DividerDecoration;
import essenti.com.br.MyFast.android.activity.BaseActivity;
import essenti.com.br.MyFast.android.widget.MensagemDialogo;
import essenti.com.br.MyFast.usuario.Usuario;
import essenti.com.br.MyFast.usuario.UsuarioHelper;
import essenti.com.br.MyFast.webservice.ServiceGenerator;
import io.fabric.sdk.android.Fabric;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static essenti.com.br.MyFast.android.widget.MensagemDialogo.MENSAGEM_ALERTA;
import static essenti.com.br.MyFast.android.widget.MensagemDialogo.MENSAGEM_SUCESSO;

/**
 * Controlador da tela que contém a lista de mesas obtidas do servidor. As mesas visualizadas podem
 * ser filtradas pelo número da mesa na barra de busca ou tipo de mesa no menu de opções na parte
 * inferior da tela que é controlado pelo {@link MenuMesaFragment}.<br><br>
 *
 * A tela controlada por esta atividade apresenta uma lista com os principais dados das {@link
 * Mesa}s.
 *
 * @author Filipe Bezerra, Diego
 * @version 1.4.0, 29/04/2016
 * @since 1.0.0
 */
public class MesasActivity extends BaseActivity
        implements MenuMesaFragment.OnMenuMesaSelecionado, MesaAdapter.MesaCallbacks {

    private static final String TAG = MesasActivity.class.getSimpleName();

    private static final int CODIGO_CARDAPIO = App.getInstancia().getCodigoCardapio();

    private static final int ERRO_ATUALIZACAO_MESAS = 1;
    private static final int ERRO_FECHAMENTO_MESA = 2;
    private static final int ERRO_ATUALIZACAO_MESA_ATENDIDA = 3;

    public static final int TODAS_MESAS = 500;
    public static final int MINHAS_MESAS = 501;
    public static final int MESAS_DISPONIVEIS = 502;
    public static final int MESAS_EM_CONSUMO = 503;
    public static final int MESAS_EM_FECHAMENTO = 504;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({
            TODAS_MESAS,
            MINHAS_MESAS,
            MESAS_DISPONIVEIS,
            MESAS_EM_CONSUMO,
            MESAS_EM_FECHAMENTO
    })
    private @interface FiltroStatus {
    }

    @FiltroStatus
    private int FILTRO_STATUS_SELECIONADO;

    private MesaAdapter mMesaAdapter;

    private Fragment mFragmentMenuMesa;

    private MensagemDialogo mDialogoProgresso;

    private MesaService mMesaService;

    private CompositeSubscription mCompositeSubscription;

    private Usuario mUsuarioLogado;

    @BindView(R.id.listViewMesas) RecyclerView mMesasListView;
    @BindView(R.id.txtFiltroMesa) TextView mFiltroMesaView;

    private void showError(Throwable e, @IntRange(from = 1, to = 3) int tipoErro) {
        dismissDialogoProgresso();

        if (Fabric.isInitialized()) {
            Crashlytics.logException(e);
        }

        String mensagem = "Infelizmente houve uma falha ";
        switch (tipoErro) {
            case ERRO_ATUALIZACAO_MESAS:
                mensagem += "ao tentar atualizar informações das mesas.";
                break;

            case ERRO_FECHAMENTO_MESA:
                mensagem += "ao tentar realizar o fechamento da mesa.";
                break;

            case ERRO_ATUALIZACAO_MESA_ATENDIDA:
                mensagem += "ao tentar atualizar informações da mesa atendida.";
                break;
        }

        mensagem += "\nPor favor tente novamente. Esta falha será reportada para o "
                + "administrador do sistema!";

        App.getInstancia().exibirMensagem(this, MensagemDialogo.MENSAGEM_ERRO,
                "Atenção", mensagem);
    }

    private void showDialogoProgresso(String mensagem) {
        if (mDialogoProgresso == null) {
            mDialogoProgresso = App.getInstancia().exibirProgresso(this, "Processando", mensagem);
            mDialogoProgresso.exibir();
        } else {
            mDialogoProgresso.setTexto(mensagem);

            if (!mDialogoProgresso.isExibindo()) {
                mDialogoProgresso.exibir();
            }
        }
    }

    private void dismissDialogoProgresso() {
        if (mDialogoProgresso != null) {
            if (mDialogoProgresso.isExibindo()) {
                mDialogoProgresso.esconder();
            }

            mDialogoProgresso = null;
        }
    }

    private void requestMesasDoTipo(@FiltroStatus final int tipoFitro) {
        if (mMesaService == null) {
            mMesaService = ServiceGenerator.createService(MesaService.class, this);
        }

        if (mMesaService == null) {
            App.getInstancia()
                    .exibirMensagem(this, MensagemDialogo.MENSAGEM_ALERTA,
                            "Configuração não realizada",
                            "É necessário fazer a configuração primeiramente!");
            return;
        }

        String statusMesa;
        switch (tipoFitro) {
            case TODAS_MESAS:
            case MINHAS_MESAS:
                statusMesa = MesaService.STATUS_TODOS;
                break;

            case MESAS_EM_CONSUMO:
                statusMesa = MesaService.STATUS_EM_CONSUMO;
                break;

            case MESAS_DISPONIVEIS:
                statusMesa = MesaService.STATUS_DISPONIVEL;
                break;

            case MESAS_EM_FECHAMENTO:
                statusMesa = MesaService.STATUS_EM_FECHAMENTO;
                break;

            default:
                showError(new IllegalArgumentException("O filtro de status da mesa " + tipoFitro +
                        " é inválido!"), ERRO_ATUALIZACAO_MESAS);
                return;
        }

        requestMesas(tipoFitro, statusMesa);
    }

    private void requestMesas(@FiltroStatus final int tipoFitro, final String statusMesa) {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            Subscription subscription = mMesaService
                    .mesas(App.getInstancia().getCodigoCardapio(), statusMesa)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                            new Subscriber<ListaMesasStatus>() {
                                @Override
                                public void onStart() {
                                    showDialogoProgresso("Atualizando informações das mesas...");
                                }

                                @Override
                                public void onError(Throwable e) {
                                    showError(e, ERRO_ATUALIZACAO_MESAS);
                                }

                                @Override
                                public void onNext(ListaMesasStatus listaMesas) {
                                    App.getInstancia().setMesas(listaMesas.mesas);
                                    displayMesasComFiltroSelecionado(tipoFitro);
                                }

                                @Override
                                public void onCompleted() {
                                    dismissDialogoProgresso();
                                }
                            }
                    );
            mCompositeSubscription.add(subscription);
        } else {
            FeedbackHelper.showOfflineMessage(this, null,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            requestMesas(tipoFitro, statusMesa);
                        }
                    });
        }
    }

    private void displayMesasComFiltroSelecionado(@FiltroStatus int tipoFiltro) {
        FILTRO_STATUS_SELECIONADO = tipoFiltro;

        if (mMesaAdapter == null) {
            mMesaAdapter = new MesaAdapter(this, App.getInstancia().getMesas(), this);
            mMesasListView.setAdapter(mMesaAdapter);
        } else {
            mMesaAdapter.setMesas(App.getInstancia().getMesas());
        }

        if (tipoFiltro != MINHAS_MESAS) {
            mMesaAdapter.getFilter().filter("");
        }

        switch (tipoFiltro) {
            case TODAS_MESAS:
                mFiltroMesaView.setText("TODAS AS MESAS");
                break;

            case MINHAS_MESAS:
                if (mUsuarioLogado != null) {
                    mMesaAdapter.getFilter().filter(String.valueOf(mUsuarioLogado.getMatricula()));
                }
                mFiltroMesaView.setText(getString(R.string.filtro_minhas_mesas).toUpperCase());
                break;

            case MESAS_EM_CONSUMO:
                mFiltroMesaView.setText("MESAS EM CONSUMO");
                break;

            case MESAS_DISPONIVEIS:
                mFiltroMesaView.setText("MESAS DISPONÍVEIS");
                break;

            case MESAS_EM_FECHAMENTO:
                mFiltroMesaView
                        .setText("MESAS EM FECHAMENTO");
                break;
        }
    }

    private void requestInformacoesMesa(final int numeroMesa) {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            if (mMesaService == null) {
                mMesaService = ServiceGenerator.createService(MesaService.class, this);
            }

            if (mMesaService != null) {
                mMesaService.obterMesa(numeroMesa, CODIGO_CARDAPIO)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(
                                new Subscriber<ResultadoObterMesa>() {
                                    @Override
                                    public void onStart() {
                                        showDialogoProgresso(
                                                "Atualizando informações da mesa atendida...");
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        showError(e, ERRO_ATUALIZACAO_MESA_ATENDIDA);
                                    }

                                    @Override
                                    public void onNext(ResultadoObterMesa resultado) {
                                        App.getInstancia().setMesa(resultado.mesa);

                                        if (FILTRO_STATUS_SELECIONADO != TODAS_MESAS
                                                && FILTRO_STATUS_SELECIONADO != MINHAS_MESAS) {
                                            if (mMesaAdapter.removeIfStatusMudou(resultado.mesa)) {
                                                return;
                                            }
                                        }

                                        mMesaAdapter.setMesa(resultado.mesa);
                                    }

                                    @Override
                                    public void onCompleted() {
                                        dismissDialogoProgresso();
                                    }
                                }
                        );
            }
        }
    }

    private void fecharMesa(final int numeroMesa) {
        if (mMesaService == null) {
            mMesaService = ServiceGenerator.createService(MesaService.class, this);
        }

        if (mMesaService != null) {
            mMesaService.fecharMesa(numeroMesa, CODIGO_CARDAPIO)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                            new Subscriber<ResultadoFecharMesa>() {
                                @Override
                                public void onStart() {
                                    showDialogoProgresso("Fechando o consumo da mesa...");
                                }

                                @Override
                                public void onError(Throwable e) {
                                    showError(e, ERRO_FECHAMENTO_MESA);
                                }

                                @Override
                                public void onNext(ResultadoFecharMesa resultado) {
                                    if (resultado.mensagem.equalsIgnoreCase("OK")) {
                                        App.getInstancia()
                                                .exibirMensagem(MesasActivity.this,
                                                        MENSAGEM_SUCESSO,
                                                        "Atenção", "Mesa em fechamento!",
                                                        new MensagemDialogo.OnMensagemClickListener() {
                                                            @Override
                                                            public void onConfirmarClick() {
                                                                requestInformacoesMesa(numeroMesa);
                                                            }

                                                            @Override
                                                            public void onCancelarClick() {
                                                            }
                                                        });
                                    } else {
                                        App.getInstancia()
                                                .exibirMensagem(MesasActivity.this,
                                                        MENSAGEM_ALERTA, "Atenção",
                                                        resultado.mensagem);
                                    }
                                }

                                @Override
                                public void onCompleted() {
                                    dismissDialogoProgresso();
                                }
                            }
                    );
        }
    }

    @Override
    protected int provideLayoutResource() {
        return R.layout.activity_mesas;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCompositeSubscription = new CompositeSubscription();

        EditText txtMesaBusca = ButterKnife.findById(this, R.id.txtMesaBusca);
        TextView txtNomeOperador = ButterKnife.findById(this, R.id.txtNomeOperador);

        mUsuarioLogado = UsuarioHelper.retrieveUsuarioLogado(this);
        if (mUsuarioLogado != null) {
            txtNomeOperador.setText(mUsuarioLogado.getNome());
        } else {
            if (Fabric.isInitialized()) {
                Crashlytics.log(Log.INFO, TAG, "Não foi encontrado usuário logado no aplicativo.");
            }
        }

        ImageView imgAtualizar = ButterKnife.findById(this, R.id.imgAtualizar);
        imgAtualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestMesasDoTipo(TODAS_MESAS);
            }
        });

        mFiltroMesaView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                mFragmentMenuMesa = fm.findFragmentById(R.id.fragcontainer);
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                if (mFragmentMenuMesa == null || !mFragmentMenuMesa.isVisible()) {
                    mFragmentMenuMesa = MenuMesaFragment.newInstance();

                    fragmentTransaction.setCustomAnimations(
                            R.animator.slide_in_up, R.animator.slide_out_up);
                    fragmentTransaction.replace(R.id.fragcontainer, mFragmentMenuMesa);
                    fragmentTransaction.commit();
                    fm.executePendingTransactions();
                } else {
                    fragmentTransaction.setCustomAnimations(
                            R.animator.slide_out_up, R.animator.slide_in_up);
                    fragmentTransaction.remove(mFragmentMenuMesa);
                    fragmentTransaction.commit();
                }
            }
        });

        mMesasListView.setHasFixedSize(true);
        mMesasListView.setLayoutManager(new LinearLayoutManager(this));
        mMesasListView.addItemDecoration(new DividerDecoration(this));

        txtMesaBusca.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mMesaAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        requestMesasDoTipo(TODAS_MESAS);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == App.PRODUTO_ADICIONADO || resultCode == App.ATENDIMENTO_MESA) {

            if (data.hasExtra(App.EXTRA_MESA_ATENDIMENTO)) {
                final Mesa mesa = data.getParcelableExtra(App.EXTRA_MESA_ATENDIMENTO);
                if (mesa != null) {
                    requestInformacoesMesa(mesa.getNumero());
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mCompositeSubscription.isUnsubscribed()) {
            mCompositeSubscription.unsubscribe();
        }
    }

    @Override
    public void onSelected(int menuSelecionado) {
        mFragmentMenuMesa.getFragmentManager()
                .beginTransaction()
                .remove(mFragmentMenuMesa)
                .commit();
        requestMesasDoTipo(menuSelecionado);
    }

    @Override
    public void onClickAdicionarItem(Mesa mesa) {
        if ((mesa.getStatusMesa() == Mesa.MESA_DISPONIVEL)
                || (mesa.getStatusMesa() == Mesa.MESA_EM_CONSUMO)) {
            Intent intent = new Intent(getApplicationContext(), ProdutosActivity.class);
            intent.putExtra(App.EXTRA_MESA_ATENDIMENTO, mesa);
            startActivityForResult(intent, App.ADICIONAR_PRODUTO);
        } else {
            App.getInstancia()
                    .exibirMensagem(MesasActivity.this, MensagemDialogo.MENSAGEM_ALERTA,
                            "Atenção", "Só é permitido inserir novos produtos em mesas "
                                    + "disponível ou em mesas que já estão em consumo!");
        }
    }

    @Override
    public void onClickFecharConta(final Mesa mesa) {
        if ((mesa.getStatusMesa() == Mesa.MESA_EM_CONSUMO)) {
            App.getInstancia()
                    .exibirQuestionamento(MesasActivity.this, "Atenção",
                            "Tem certeza que deseja finalizar esta mesa?",
                            new MensagemDialogo.OnMensagemClickListener() {
                                @Override
                                public void onConfirmarClick() {
                                    fecharMesa(mesa.getNumero());
                                }

                                @Override
                                public void onCancelarClick() {
                                }
                            });
        } else {
            App.getInstancia()
                    .exibirMensagem(MesasActivity.this, MensagemDialogo.MENSAGEM_ALERTA,
                            "Atenção", "Esta mesa não está em consumo!");
        }
    }

    @Override
    public void onClickItem(Mesa mesa) {
        ActivityNavigator.startAtendimentoMesaActivity(MesasActivity.this, mesa);
    }
}

package essenti.com.br.MyFast.produto;

import android.net.Uri;
import android.support.annotation.NonNull;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import essenti.com.br.MyFast.android.asynctask.AbstractAsyncTask;
import essenti.com.br.MyFast.android.asynctask.TaskCallback;
import essenti.com.br.MyFast.android.asynctask.TaskException;
import essenti.com.br.MyFast.app.App;

/**
 * Tarefa assíncrona para obtenção da listagem de {@link Produto}s que estão fora do cardápio via
 * Web service.
 *
 * @author Filipe Bezerra
 * @since 14/12/2015
 */
public class GetProdutosAvulsosTask extends AbstractAsyncTask<String, Void, List<Produto>> {
    private static final String PATH_SERVICE_PRODUTOS = "ServiceProduto.svc/ProdutosAvulso";

    public GetProdutosAvulsosTask(@NonNull TaskCallback<List<Produto>> callback) {
        super(callback);
    }

    private List<Produto> getProdutosAvulsos(String jsonString) throws JSONException {
        final List<Produto> produtosAvulsos = new ArrayList<>();

        final JSONObject jsonObject = new JSONObject(jsonString);
        final JSONArray jsonArrayProdutoAvulso = jsonObject.getJSONArray(
                "ListarProdutosAvulsoResult");

        JSONObject jsonProduto;
        JSONObject jsonComplemento;

        JSONArray jsonArrayComplementos;
        for (int i = 0; i < jsonArrayProdutoAvulso.length(); i++) {
            jsonProduto = jsonArrayProdutoAvulso.getJSONObject(i);
            Produto produto = new Produto();
            produto.setDescricao(jsonProduto.getString("Descricao"));
            produto.setCodigo(jsonProduto.getInt("Codprod"));
            produto.setValorUnitario(jsonProduto.getDouble("Preco"));
            produto.setCodigoAuxiliar(jsonProduto.getString("Codauxiliar"));
            produto.setSequencia(jsonProduto.getInt("Codigo"));

            jsonArrayComplementos = jsonProduto.getJSONArray("Complementos");
            for (int j = 0; j < jsonArrayComplementos.length(); j++) {
                jsonComplemento = jsonArrayComplementos.getJSONObject(j);
                Complemento complemento = new Complemento();
                complemento.setDescricao(jsonComplemento.getString("Nomecomplemento"));
                complemento.setCodigo(jsonComplemento.getInt("Codcomp"));
                complemento.setValorUnitario(jsonComplemento.getDouble("Precocomp"));

                produto.getComplementos().add(complemento);
            }

            produtosAvulsos.add(produto);
        }

        return produtosAvulsos;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected List<Produto> doInBackground(String... params) {
        if (!checkParamsNotNull(params)) {
            return null;
        }

        if (params.length != 2) {
            mException = TaskException.of(
                    new IllegalArgumentException("A lista de argumentos está incorreta. " +
                            "O primeiro argumento é o código da embalagem e " +
                            "o segundo a descrição do produto."));
            return null;
        }

        final String codigoEmbalagem = params[0];
        final String descricaoProduto = params[1];

        final String url = Uri.parse(App.getInstancia().getURL())
                .buildUpon()
                .appendPath(PATH_SERVICE_PRODUTOS)
                .appendPath(String.valueOf(App.getInstancia().getCodigoCardapio()))
                .appendPath(codigoEmbalagem)
                .appendPath(descricaoProduto)
                .build()
                .toString()
                .replaceAll(" ", "%20");

        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(url);

        try {
            HttpResponse response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream instream = entity.getContent();
                String json = getStreamData(instream);
                instream.close();
                return getProdutosAvulsos(json);
            }
        } catch (JSONException | IOException e) {
            mException = TaskException.of(e);
        }

        return null;
    }
}

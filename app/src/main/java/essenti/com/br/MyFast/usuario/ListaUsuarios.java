package essenti.com.br.MyFast.usuario;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListaUsuarios {
    @SerializedName("ListarUsuariosResult")
    public List<Usuario> usuarios;
}

package essenti.com.br.MyFast.mesa;

import android.os.Parcel;
import android.os.Parcelable;
import com.hannesdorfmann.parcelableplease.annotation.ParcelablePlease;

/**
 * Entidade cliente.
 *
 * @author Filipe Bezerra
 * @version 20/01/2016
 * @since 20/01/2016
 */
@ParcelablePlease
public class Cliente implements Parcelable {
    long codigo;
    String email;
    String nome;

    public long getCodigo() {
        return codigo;
    }

    public Cliente setCodigo(int codigo) {
        this.codigo = codigo;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Cliente setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getNome() {
        return nome;
    }

    public Cliente setNome(String nome) {
        this.nome = nome;
        return this;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ClienteParcelablePlease.writeToParcel(this, dest, flags);
    }

    public static final Creator<Cliente> CREATOR = new Creator<Cliente>() {
        public Cliente createFromParcel(Parcel source) {
            Cliente target = new Cliente();
            ClienteParcelablePlease.readFromParcel(target, source);
            return target;
        }

        public Cliente[] newArray(int size) {
            return new Cliente[size];
        }
    };
}

package essenti.com.br.MyFast.pagamento;

/**
 * Classe que representa um pagamento com cartão de crédito obtida
 * através da integração com Cielo Mobile.
 *
 * @author Filipe Bezerra
 * @version 1.4.0, 29/02/2016
 * @since 1.4.0
 */
public class PagamentoCielo {
    public String captura;

    public String codAutorizacao;

    public String codProdutoMatriz;

    public String codProdutoSecundario;

    public String codResposta;

    public String comprovanteCliente;

    public String comprovanteVendedor;

    public String dataRequisicao;

    public String dataServidor;

    public String estAcquirer;

    public String estVenda;

    public String fluxo;

    public String idAplicacao;

    public String idTransacao;

    public String modoCaptura;

    public String nomeAplicacao;

    public String nsu;

    public String pan;

    public String referencia;

    public String retornoAplicacao;

    public String tipoTransacao;

    public String valor;

    public String versaoAppFinanceira;

    public String email;

    public String nomeBandeira;

    public String nomeProdutoMatriz;

    public String nomeProdutoSecundario;
}

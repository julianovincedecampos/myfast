package essenti.com.br.MyFast.produto;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.android.activity.BaseActivity;
import essenti.com.br.MyFast.android.asynctask.TaskCallback;
import essenti.com.br.MyFast.android.asynctask.TaskException;
import essenti.com.br.MyFast.android.widget.MensagemDialogo;
import essenti.com.br.MyFast.app.App;
import essenti.com.br.MyFast.atendimento.ConfirmaPedidosDialog;
import essenti.com.br.MyFast.barcode.CaptureActivityAnyOrientation;
import essenti.com.br.MyFast.mesa.Mesa;
import essenti.com.br.MyFast.mesa.MesaService;
import essenti.com.br.MyFast.mesa.PostPedido;
import essenti.com.br.MyFast.mesa.ResultadoBloqueioMesa;
import essenti.com.br.MyFast.mesa.ResultadoDesbloqueioMesa;
import essenti.com.br.MyFast.usuario.Usuario;
import essenti.com.br.MyFast.usuario.UsuarioHelper;
import essenti.com.br.MyFast.util.FeedbackHelper;
import essenti.com.br.MyFast.util.NetworkUtil;
import essenti.com.br.MyFast.webservice.ServiceGenerator;
import io.fabric.sdk.android.Fabric;
import java.util.List;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

import static essenti.com.br.MyFast.android.widget.MensagemDialogo.MENSAGEM_ALERTA;
import static essenti.com.br.MyFast.android.widget.MensagemDialogo.MENSAGEM_ERRO;
import static essenti.com.br.MyFast.android.widget.MensagemDialogo.MENSAGEM_SUCESSO;
import static essenti.com.br.MyFast.usuario.UsuarioHelper.retrieveUsuarioLogado;

/**
 * Controlador da tela que contém a lista de {@link Produto}s obtidos do servidor. Os produtos
 * visualizados são consultados no servidor através da {@link SearchView} da {@link Toolbar} e podem
 * ser filtrados no próprio {@link ProdutosAvulsosAdapter}.<br><br>
 *
 * A tela controlada por esta atividade apresenta uma {@link RecyclerView} com os dados do {@link
 * Produto} avulso (fora do cardápio).
 *
 * @author Filipe Bezerra
 * @version 1.4.0, 30/03/2016
 * @since 14/12/2015
 */
public class ProdutosAvulsosActivity extends BaseActivity
        implements ConfirmaPedidosDialog.ConfirmaPedidosCallback {

    private static final String TAG = ProdutosAvulsosActivity.class.getSimpleName();

    private static final int ERRO_CARREGAMENTO_PRODUTOS = 1;
    private static final int ERRO_ENVIANDO_PEDIDO = 2;
    private static final int ERRO_BLOQUEIO_MESA = 3;
    private static final int ERRO_DESBLOQUEIO_MESA = 4;

    @IntDef({
            ERRO_CARREGAMENTO_PRODUTOS,
            ERRO_ENVIANDO_PEDIDO,
            ERRO_BLOQUEIO_MESA,
            ERRO_DESBLOQUEIO_MESA
    })
    private @interface TipoError{}

    private ProdutosAvulsosAdapter mProdutosAvulsosAdapter;

    private MensagemDialogo mDialogoProgresso;

    private Mesa mMesaEmAtendimento;

    private Usuario mUsuarioLogado;

    private MesaService mMesaService;

    private boolean mActivityResumed;

    private Pair<Throwable, Integer> mPendingErrorMessage;
    private Pair<String, MensagemDialogo.OnMensagemClickListener> mPendingExcecaoNegocioMessage;
    private String mPendingDialogoMessage;

    private TaskCallback<List<Produto>> mGetProdutosAvulsosTaskCallback =
            new TaskCallback<List<Produto>>() {
                @Override
                public void onBegin() {
                    removerAjuda();
                    showDialogoProgresso("Buscando produtos disponíveis...");
                }

                @Override
                public void onSuccess(List<Produto> result) {
                    dismissDialogoProgresso();
                    App.getInstancia().setProdutosAvulsos(result);
                    mProdutosAvulsosAdapter.addAll(result);
                    removerAjuda();
                }

                @Override
                public void onResultNothing() {
                    dismissDialogoProgresso();
                    mProdutosAvulsosAdapter.clear();
                    App.getInstancia()
                            .exibirMensagem(ProdutosAvulsosActivity.this, MENSAGEM_ALERTA,
                                    "Atenção", "Nenhum produto foi encontrado!", false, true, null);
                    indicarAjudar("Nenhum produto fora do cardápio foi localizado " +
                            "para a descrição pesquisada!");
                }

                @Override
                public void onError(TaskException e) {
                    showError(e, ERRO_CARREGAMENTO_PRODUTOS);
                }
            };

    private TaskCallback<String> mPostPedidoCallback =
            new TaskCallback<String>() {
                @Override
                public void onBegin() {
                    removerAjuda();
                    showDialogoProgresso("Enviando pedido...");
                }

                @Override
                public void onSuccess(String result) {
                    dismissDialogoProgresso();
                    App.getInstancia()
                            .exibirMensagem(ProdutosAvulsosActivity.this, MENSAGEM_SUCESSO,
                                    "Atenção", "Produtos adicionados com sucesso na mesa!",
                                    new MensagemDialogo.OnMensagemClickListener() {
                                        @Override
                                        public void onConfirmarClick() {
                                            finishWithProdutoAdicionado();
                                        }

                                        @Override
                                        public void onCancelarClick() {
                                        }
                                    });
                }

                @Override
                public void onResultNothing() {
                }

                @Override
                public void onError(TaskException e) {
                    showError(e, ERRO_ENVIANDO_PEDIDO);
                }
            };

    @BindView(R.id.toolbar) Toolbar mToolbarActionBar;
    @BindView(R.id.root_layout) CoordinatorLayout mRootLayout;
    @BindView(R.id.list) RecyclerView mProdutosAvulsosView;
    @BindView(R.id.container_ajuda) LinearLayout mAjudaContainer;

    private void removerAjuda() {
        if (mAjudaContainer.getVisibility() == View.VISIBLE) {
            mAjudaContainer.setVisibility(View.GONE);
            mProdutosAvulsosView.setVisibility(View.VISIBLE);

            AppBarLayout.LayoutParams params =
                    (AppBarLayout.LayoutParams) mToolbarActionBar.getLayoutParams();
            params.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
                    | AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS);
        }
    }

    private void indicarAjudar(@Nullable String textoAjuda) {
        if (textoAjuda != null) {
            ButterKnife.<TextView>findById(this, R.id.texto_ajuda).setText(textoAjuda);
        }

        if (mAjudaContainer.getVisibility() == View.GONE) {
            mAjudaContainer.setVisibility(View.VISIBLE);
            mProdutosAvulsosView.setVisibility(View.GONE);

            AppBarLayout.LayoutParams params =
                    (AppBarLayout.LayoutParams) mToolbarActionBar.getLayoutParams();
            params.setScrollFlags(0);
        }
    }

    private void showError(Throwable e, @TipoError int tipoErro) {
        dismissDialogoProgresso();

        if (Fabric.isInitialized()) {
            Crashlytics.logException(e);
        }

        String mensagem = "Infelizmente houve uma falha ";
        switch (tipoErro) {
            case ERRO_CARREGAMENTO_PRODUTOS:
                mensagem += "ao tentar carregar os produtos.";
                break;

            case ERRO_ENVIANDO_PEDIDO:
                mensagem += "ao tentar enviar o pedido.";
                break;
        }

        mensagem += "\nPor favor tente novamente. Esta falha será reportada para o "
                + "administrador do sistema!";

        indicarAjudar(mensagem);
        App.getInstancia().exibirMensagem(this, MENSAGEM_ERRO, "Atenção", mensagem);

        mPendingErrorMessage = null;
    }

    private void showExcecaoNegocio(@NonNull String mensagem,
            @Nullable MensagemDialogo.OnMensagemClickListener listener) {
        if (Fabric.isInitialized()) {
            Crashlytics.log(Log.WARN, TAG, mensagem);
        }

        Timber.w(mensagem);

        App.getInstancia().exibirMensagem(this, MENSAGEM_ALERTA, "Atenção", mensagem,
                false, false, listener);

        mPendingExcecaoNegocioMessage = null;
    }

    private void showDialogoProgresso(String mensagem) {
        if (mDialogoProgresso == null) {
            mDialogoProgresso = App.getInstancia().exibirProgresso(this,
                    "Processando", mensagem);
            mDialogoProgresso.exibir();
        } else {
            mDialogoProgresso.setTexto(mensagem);
        }
        mPendingDialogoMessage = null;
    }

    private void dismissDialogoProgresso() {
        if (mDialogoProgresso != null) {
            if (mDialogoProgresso.isExibindo()) {
                mDialogoProgresso.esconder();
            }

            mDialogoProgresso = null;
        }
    }

    private void finishWithProdutoAdicionado() {
        final Bundle extras = new Bundle();
        extras.putParcelable(App.EXTRA_MESA_ATENDIMENTO, mMesaEmAtendimento);
        setResult(App.PRODUTO_AVULSO_ADICIONADO, new Intent().putExtras(extras));
        finish();
    }

    @Override
    protected int provideUpIndicator() {
        return R.drawable.ic_arrow_back_white_24dp;
    }

    @Override
    protected int provideLayoutResource() {
        return R.layout.activity_produtos_avulsos;
    }

    @Override
    protected boolean shouldDisplayHomeAsUpEnabled() {
        return true;
    }

    @SuppressLint("PrivateResource")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!getIntent().hasExtra(App.EXTRA_MESA_ATENDIMENTO)) {
            Toast.makeText(getApplicationContext(), "A mesa em atendimento não foi "
                            + "passada via Intent.putExtra(App.EXTRA_MESA_ATENDIMENTO, Parcelable)",
                    Toast.LENGTH_LONG).show();
            finish();
        }

        mUsuarioLogado = UsuarioHelper.retrieveUsuarioLogado(this);

        mMesaEmAtendimento = getIntent().getParcelableExtra(App.EXTRA_MESA_ATENDIMENTO);

        mProdutosAvulsosView.setHasFixedSize(true);
        mProdutosAvulsosView.setLayoutManager(new LinearLayoutManager(this));
        mProdutosAvulsosView.setAdapter(mProdutosAvulsosAdapter = new ProdutosAvulsosAdapter());
    }

    @Override
    protected void onPause() {
        super.onPause();
        mActivityResumed = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        mActivityResumed = false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        requestBloqueioMesa();
    }

    private void requestBloqueioMesa() {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            initMesaService();
            if (mMesaService != null) {
                mMesaService
                        .bloquearMesa(
                                mMesaEmAtendimento.getNumero(),
                                mUsuarioLogado.getMatricula(),
                                App.getInstancia().getCodigoCardapio()
                        )
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Subscriber<ResultadoBloqueioMesa>() {
                                    @Override
                                    public void onError(Throwable e) {
                                        showError(e, ERRO_BLOQUEIO_MESA);
                                    }

                                    @Override
                                    public void onNext(ResultadoBloqueioMesa resultado) {
                                        if (!resultado.mensagem.equalsIgnoreCase("OK")) {
                                            showMensagemBloqueioMesa(resultado.mensagem);
                                        }
                                    }

                                    @Override
                                    public void onCompleted() {}
                                }
                        );
            }
        } else {
            FeedbackHelper.showOfflineMessage(this,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            finish();
                        }
                    },
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            requestBloqueioMesa();
                        }
                    });
        }
    }

    private void initMesaService() {
        if (mMesaService == null) {
            mMesaService = ServiceGenerator.createService(MesaService.class, this);
        }
    }

    private void showMensagemBloqueioMesa(String mensagem) {
        final MensagemDialogo.OnMensagemClickListener listener =
                new MensagemDialogo.OnMensagemClickListener() {
                    @Override
                    public void onConfirmarClick() {
                        finish();
                    }

                    @Override
                    public void onCancelarClick() {
                    }
                };
        if (mActivityResumed) {
            showExcecaoNegocio(mensagem, listener);
        } else {
            mPendingExcecaoNegocioMessage = Pair.create(mensagem, listener);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mActivityResumed = true;

        if (mPendingErrorMessage != null) {
            showError(mPendingErrorMessage.first, mPendingErrorMessage.second);
        }

        if (mPendingExcecaoNegocioMessage != null) {
            showExcecaoNegocio(mPendingExcecaoNegocioMessage.first,
                    mPendingExcecaoNegocioMessage.second);
        }

        if (mPendingDialogoMessage != null) {
            showDialogoProgresso(mPendingDialogoMessage);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IntentIntegrator.REQUEST_CODE) {
            final IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode,
                    resultCode, data);

            if (intentResult != null && !TextUtils.isEmpty(intentResult.getContents())) {
                requestListaProdutosAvulsos("T", intentResult.getContents());
            }
        }
    }

    private void sendPedidoRapido(List<Produto> produtosPedido, String outraMatriculaGarcom) {
        if (TextUtils.isEmpty(outraMatriculaGarcom)) {
            final Usuario usuarioLogado = retrieveUsuarioLogado(ProdutosAvulsosActivity.this);

            outraMatriculaGarcom = usuarioLogado != null ?
                    String.valueOf(usuarioLogado.getMatricula()) : "";
        }

        requestEnvioPedido(produtosPedido, outraMatriculaGarcom);
    }

    private void requestEnvioPedido(final List<Produto> produtosPedido, final String outraMatriculaGarcom) {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            new PostPedido(
                    true,
                    mMesaEmAtendimento,
                    outraMatriculaGarcom,
                    produtosPedido,
                    mPostPedidoCallback
            ).execute();
        } else {
            FeedbackHelper.showOfflineMessage(this,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            finish();
                        }
                    },
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            requestEnvioPedido(produtosPedido, outraMatriculaGarcom);
                        }
                    });
        }
    }

    @OnClick(R.id.fab)
    void onFabClick() {
        if (mProdutosAvulsosAdapter.isProdutosSelecionadosEmpty()) {
            App.getInstancia()
                    .exibirMensagem(this, MENSAGEM_ALERTA, "Atenção",
                            "Nenhum produto foi selecionado!");
            return;
        }

        final List<Produto> produtosSelecionados
                = mProdutosAvulsosAdapter.getProdutosSelecionados();

        new ConfirmaPedidosDialog(this, produtosSelecionados)
                .callback(this)
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_produtos_avulsos, menu);
        MenuItem menuItem = menu.findItem(R.id.menu_pesquisa);

        menuItem.expandActionView();
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setQueryHint("Descrição do produto");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.length() >= 3) {
                    requestListaProdutosAvulsos(query, "0");

                    searchView.setQuery(query, false);
                    searchView.clearFocus();
                    return true;
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(final String newText) {
                if (!mProdutosAvulsosAdapter.getProdutos().isEmpty()) {
                    removerAjuda();
                    mProdutosAvulsosAdapter.getFilter()
                            .filter(newText, new Filter.FilterListener() {
                                @Override
                                public void onFilterComplete(int count) {
                                    if (mProdutosAvulsosAdapter.isEmpty()) {
                                        searchView.setQuery(newText, false);
                                        searchView.clearFocus();
                                        indicarAjudar(
                                                "Nenhum produto fora do cardápio foi localizado " +
                                                        "para o filtro informado!");
                                    }
                                }
                            });
                }
                return false;
            }
        });

        return true;
    }

    private void requestListaProdutosAvulsos(final String query, final String s) {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            new GetProdutosAvulsosTask(mGetProdutosAvulsosTaskCallback)
                    .execute(s, query);
        } else {
            FeedbackHelper.showOfflineMessage(this,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            finish();
                        }
                    },
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            requestListaProdutosAvulsos(query, s);
                        }
                    });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.menu_pesquisa_codigo_barras:
                IntentIntegrator integrator = new IntentIntegrator(this);
                integrator.setCaptureActivity(CaptureActivityAnyOrientation.class);
                integrator.setOrientationLocked(false);
                integrator.setBeepEnabled(true);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
                integrator.setPrompt("Posicione o código de barras corretamente.");
                integrator.initiateScan();
                return true;

            default: return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPedidoConfirmado(String matriculaGarcom) {
        List<Produto> produtosSelecionados =
                mProdutosAvulsosAdapter.getProdutosSelecionados();

        if (produtosSelecionados.isEmpty()) {
            App.getInstancia()
                    .exibirMensagem(ProdutosAvulsosActivity.this, MENSAGEM_ALERTA,
                            "Atenção", "Todos itens foram removidos!");
            return;
        }

        sendPedidoRapido(produtosSelecionados, matriculaGarcom);
    }

    @Override
    public void onItemPedidoCancelado(int posicao) {
        mProdutosAvulsosAdapter.removerProdutoSelecionado(posicao);
    }

    @Override
    public void onBackPressed() {
        requestDesbloqueioMesa();
    }

    private void requestDesbloqueioMesa() {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            initMesaService();
            if (mMesaService != null) {
                mMesaService
                        .desbloquearMesa(
                                mMesaEmAtendimento.getNumero(),
                                mUsuarioLogado.getMatricula(),
                                App.getInstancia().getCodigoCardapio()
                        )
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Subscriber<ResultadoDesbloqueioMesa>() {
                                    @Override
                                    public void onError(Throwable e) {
                                        showError(e, ERRO_DESBLOQUEIO_MESA);
                                    }

                                    @Override
                                    public void onNext(ResultadoDesbloqueioMesa resultado) {
                                        if (resultado.mensagem.equalsIgnoreCase("OK")) {
                                            ProdutosAvulsosActivity.super.onBackPressed();
                                        } else {
                                            showExcecaoNegocio(resultado.mensagem, null);
                                        }
                                    }

                                    @Override
                                    public void onCompleted() {
                                    }
                                }
                        );
            }
        } else {
            FeedbackHelper.showOfflineMessage(this, null,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            requestDesbloqueioMesa();
                        }
                    });
        }
    }
}

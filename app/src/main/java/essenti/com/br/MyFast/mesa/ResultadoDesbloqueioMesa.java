package essenti.com.br.MyFast.mesa;

import com.google.gson.annotations.SerializedName;

/**
 * @author Filipe Bezerra
 */
public class ResultadoDesbloqueioMesa {
    @SerializedName("DesbloquearMesaResult")
    public String mensagem;
}

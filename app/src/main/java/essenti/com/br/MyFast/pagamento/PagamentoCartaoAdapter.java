package essenti.com.br.MyFast.pagamento;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.squareup.picasso.Picasso;
import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.outrospagamentos.BandeirasCartao;
import essenti.com.br.MyFast.util.number.NumberUtil;
import java.util.ArrayList;
import java.util.List;

/**
 * Adaptador da listagem de pagamentos feitos com cartão da tela {@link PagamentosRealizadosActivity}.
 *
 * @author Filipe Bezerra
 */
public class PagamentoCartaoAdapter extends RecyclerView.Adapter<PagamentoCartaoAdapter.ViewHolder> {
    @NonNull private Context mContext;
    @NonNull private List<Pagamento> mPagamentos;

    public PagamentoCartaoAdapter(@NonNull Context context, @NonNull Pagamento pagamento) {
        mContext = context;
        mPagamentos = new ArrayList<>();
        mPagamentos.add(pagamento);
    }

    public PagamentoCartaoAdapter(@NonNull Context context, @NonNull List<Pagamento> pagamentos) {
        mContext = context;
        mPagamentos = pagamentos;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(mContext)
                .inflate(R.layout.item_pagamento_cartao, parent, false);
        return new PagamentoCartaoAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Pagamento pagamento = mPagamentos.get(position);

        holder.pan.setText(!TextUtils.isEmpty(pagamento.getPan()) ?
                pagamento.getPan() : pagamento.getNomeProdutoMatriz());

        holder.valorPagamento.setText(NumberUtil.formatAsCurrency(pagamento.getValor()));

        int iconeBandeiraRes;

        if (pagamento.getNomeProdutoMatriz().equalsIgnoreCase("DINHEIRO")) {
            iconeBandeiraRes = R.drawable.ic_cash_multiple_black_24dp;
        } else {
            iconeBandeiraRes = BandeirasCartao.getIconeBandeira(pagamento.getNomeBandeira());
        }

        Picasso.with(mContext)
                .load(iconeBandeiraRes)
                .into(holder.bandeiraCartao);
    }

    @Override
    public int getItemCount() {
        return mPagamentos.size();
    }

    public void clearData() {
       if (!mPagamentos.isEmpty()) {
           mPagamentos.clear();
           notifyDataSetChanged();
       }
    }

    public void addItem(Pagamento pagamento) {
        if (pagamento != null) {
            final int lastPosition = getItemCount();
            mPagamentos.add(pagamento);
            notifyItemInserted(lastPosition);
        }
    }

    public void swapItems(List<Pagamento> pagamentos) {
        if (pagamentos != null) {
            mPagamentos.clear();
            if(mPagamentos.addAll(pagamentos)) {
                notifyDataSetChanged();
            }
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.bandeiraCartao) ImageView bandeiraCartao;
        @BindView(R.id.pan) TextView pan;
        @BindView(R.id.valorPagamento) TextView valorPagamento;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

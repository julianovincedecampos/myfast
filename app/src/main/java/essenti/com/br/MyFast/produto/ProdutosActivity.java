package essenti.com.br.MyFast.produto;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Filter;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;
import com.h6ah4i.android.widget.advrecyclerview.decoration.SimpleListDividerDecorator;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;
import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.android.activity.BaseActivity;
import essenti.com.br.MyFast.android.asynctask.TaskCallback;
import essenti.com.br.MyFast.android.asynctask.TaskException;
import essenti.com.br.MyFast.android.drawable.DrawableHelper;
import essenti.com.br.MyFast.android.widget.MensagemDialogo;
import essenti.com.br.MyFast.android.widget.MensagemDialogo.OnMensagemClickListener;
import essenti.com.br.MyFast.app.App;
import essenti.com.br.MyFast.atendimento.ConfirmaPedidosDialog;
import essenti.com.br.MyFast.mesa.Mesa;
import essenti.com.br.MyFast.mesa.MesaService;
import essenti.com.br.MyFast.mesa.PostPedido;
import essenti.com.br.MyFast.mesa.ResultadoBloqueioMesa;
import essenti.com.br.MyFast.mesa.ResultadoDesbloqueioMesa;
import essenti.com.br.MyFast.usuario.Usuario;
import essenti.com.br.MyFast.usuario.UsuarioHelper;
import essenti.com.br.MyFast.util.FeedbackHelper;
import essenti.com.br.MyFast.util.NetworkUtil;
import essenti.com.br.MyFast.webservice.ServiceGenerator;
import io.fabric.sdk.android.Fabric;
import java.util.List;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

import static essenti.com.br.MyFast.android.widget.MensagemDialogo.MENSAGEM_ALERTA;

public class ProdutosActivity extends BaseActivity
        implements ConfirmaPedidosDialog.ConfirmaPedidosCallback {

    private static final String TAG = ProdutosActivity.class.getSimpleName();

    private RecyclerView.LayoutManager mListLayoutManager;

    private RecyclerView.Adapter mWrappedAdapter;

    private RecyclerViewExpandableItemManager mListItemManager;

    private ProdutosAdapter mProdutosAdapter;

    private Usuario mUsuarioLogado;

    private Mesa mMesaEmAtendimento;

    private MensagemDialogo mDialogoProgresso;

    private MesaService mMesaService;

    private boolean mActivityResumed;

    private Throwable mPendingErrorMessage;
    private Pair<String, OnMensagemClickListener> mPendingExcecaoNegocioMessage;
    private String mPendingDialogoMessage;

    @BindView(R.id.list) RecyclerView mListViewProdutos;

    private void showError(Throwable e) {
        dismissDialogoProgresso();

        if (Fabric.isInitialized()) {
            Crashlytics.logException(e);
        }

        App.getInstancia().exibirMensagem(this, MensagemDialogo.MENSAGEM_ERRO,
                "Atenção", e.getMessage() + ". Por favor tente novamente.\n" +
                        "Esta falha será reportada para o administrador do sistema!");

        mPendingErrorMessage = null;
    }

    private void showExcecaoNegocio(@NonNull String mensagem,
            @Nullable OnMensagemClickListener listener) {
        if (Fabric.isInitialized()) {
            Crashlytics.log(Log.WARN, TAG, mensagem);
        }

        Timber.w(mensagem);

        App.getInstancia().exibirMensagem(this, MENSAGEM_ALERTA, "Atenção", mensagem,
                false, false, listener);

        mPendingExcecaoNegocioMessage = null;
    }

    private void showDialogoProgresso(String mensagem) {
        if (mDialogoProgresso == null) {
            mDialogoProgresso = App.getInstancia().exibirProgresso(this,
                    "Processando", mensagem);
            mDialogoProgresso.exibir();
        } else {
            mDialogoProgresso.setTexto(mensagem);
        }
        mPendingDialogoMessage = null;
    }

    private void dismissDialogoProgresso() {
        if (mDialogoProgresso != null) {
            if (mDialogoProgresso.isExibindo()) {
                mDialogoProgresso.esconder();
            }

            mDialogoProgresso = null;
        }
    }

    private void limparDadosProdutos(ProdutosAdapter adapter) {
        for (Produto produto : adapter.getProdutosSelecionados()) {
            produto.setQuantidade(0);
        }
    }

    private void finishWithProdutoAdicionado() {
        setResult(App.PRODUTO_ADICIONADO,
                new Intent().putExtra(App.EXTRA_MESA_ATENDIMENTO, mMesaEmAtendimento));
        finish();
    }

    private void enviarPedido(List<Produto> produtos, String outraMatriculaGarcom) {
        if (TextUtils.isEmpty(outraMatriculaGarcom)) {
            outraMatriculaGarcom = mUsuarioLogado != null ?
                    String.valueOf(mUsuarioLogado.getMatricula()) : "";
        }

        requestEnvioPedido(produtos, outraMatriculaGarcom);
    }

    private void requestEnvioPedido(final List<Produto> produtos, final String outraMatriculaGarcom) {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            new PostPedido(
                    false,
                    mMesaEmAtendimento,
                    outraMatriculaGarcom,
                    produtos,
                    new TaskCallback<String>() {
                        @Override
                        public void onBegin() {
                            showDialogoProgresso("Enviando pedido...");
                        }

                        @Override
                        public void onSuccess(String result) {
                            dismissDialogoProgresso();
                            App.getInstancia().zerarQuantidadeProdutos(produtos);
                            App.getInstancia().exibirMensagem(ProdutosActivity.this,
                                    MensagemDialogo.MENSAGEM_SUCESSO, "Atenção",
                                    "Produtos adicionados com sucesso na mesa!", new
                                            OnMensagemClickListener() {
                                                @Override
                                                public void onConfirmarClick() {
                                                    finishWithProdutoAdicionado();
                                                }

                                                @Override
                                                public void onCancelarClick() {
                                                }
                                            });
                        }

                        @Override
                        public void onResultNothing() {
                        }

                        @Override
                        public void onError(TaskException e) {
                            showError(e);
                        }
                    }
            ).execute();
        } else {
            FeedbackHelper.showOfflineMessage(this,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            finish();
                        }
                    },
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            requestEnvioPedido(produtos, outraMatriculaGarcom);
                        }
                    });
        }
    }

    @Override
    protected int provideLayoutResource() {
        return R.layout.activity_produtos;
    }

    @Override
    protected int provideUpIndicator() {
        return R.drawable.ic_arrow_back_white_24dp;
    }

    @Override
    protected boolean shouldDisplayHomeAsUpEnabled() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!getIntent().hasExtra(App.EXTRA_MESA_ATENDIMENTO)) {
            Toast.makeText(getApplicationContext(), "A mesa em atendimento não foi "
                            + "passada via Intent.putExtra(App.EXTRA_MESA_ATENDIMENTO, Parcelable)",
                    Toast.LENGTH_LONG).show();
            finish();
        }

        mUsuarioLogado = UsuarioHelper.retrieveUsuarioLogado(this);

        mMesaEmAtendimento = getIntent().getParcelableExtra(App.EXTRA_MESA_ATENDIMENTO);

        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Mesa nº " + mMesaEmAtendimento.getNumero());
            actionBar.setSubtitle(mUsuarioLogado.getNome());
        }

        FloatingActionButton fab = ButterKnife.findById(this, R.id.fabConfirmaPedido);

        DrawableHelper
                .withContext(this)
                .withColor(R.color.white)
                .withDrawable(fab.getDrawable())
                .tint()
                .applyTo(fab);

        //region RecyclerView
        mListLayoutManager = new LinearLayoutManager(this);

        mListItemManager = new RecyclerViewExpandableItemManager(null);
        mListItemManager.setOnGroupCollapseListener(
                new RecyclerViewExpandableItemManager.OnGroupCollapseListener() {
                    @Override
                    public void onGroupCollapse(int groupPosition, boolean fromUser) {

                    }
                });
        mListItemManager.setOnGroupExpandListener(
                new RecyclerViewExpandableItemManager.OnGroupExpandListener() {
                    @Override
                    public void onGroupExpand(int groupPosition, boolean fromUser) {
                        if (fromUser) {
                            mListItemManager.collapseAll();
                            mListItemManager.expandGroup(groupPosition);

                            // TODO mListItemManager.scrollToGroup
                        }
                    }
                });

        mProdutosAdapter = new ProdutosAdapter(this, App.getInstancia().getGrupos(),
                mListItemManager);
        mWrappedAdapter = mListItemManager.createWrappedAdapter(mProdutosAdapter);

        mListViewProdutos.setLayoutManager(mListLayoutManager);
        mListViewProdutos.setAdapter(mWrappedAdapter);
        mListViewProdutos.setHasFixedSize(false);
        mListViewProdutos.addItemDecoration(
                new SimpleListDividerDecorator(ContextCompat.getDrawable(this,
                        R.drawable.list_divider_horizontal), true));
        mListItemManager.attachRecyclerView(mListViewProdutos);
        //endregion
    }

    @Override
    protected void onPause() {
        super.onPause();
        mActivityResumed = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        mActivityResumed = false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        requestBloqueioMesa();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mActivityResumed = true;

        if (mPendingErrorMessage != null) {
            showError(mPendingErrorMessage);
        }

        if (mPendingExcecaoNegocioMessage != null) {
            showExcecaoNegocio(mPendingExcecaoNegocioMessage.first,
                    mPendingExcecaoNegocioMessage.second);
        }

        if (mPendingDialogoMessage != null) {
            showDialogoProgresso(mPendingDialogoMessage);
        }
    }

    private void requestBloqueioMesa() {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            initMesaService();
            if (mMesaService != null) {
                mMesaService
                        .bloquearMesa(
                                mMesaEmAtendimento.getNumero(),
                                mUsuarioLogado.getMatricula(),
                                App.getInstancia().getCodigoCardapio()
                        )
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Subscriber<ResultadoBloqueioMesa>() {
                                    @Override
                                    public void onError(Throwable e) {
                                        showError(e);
                                    }

                                    @Override
                                    public void onNext(ResultadoBloqueioMesa resultado) {
                                        if (!resultado.mensagem.equalsIgnoreCase("OK")) {
                                            showMensagemBloqueioMesa(resultado.mensagem);
                                        }
                                    }

                                    @Override
                                    public void onCompleted() {
                                    }
                                }
                        );
            }
        } else {
            FeedbackHelper.showOfflineMessage(this,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            finish();
                        }
                    },
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            requestBloqueioMesa();
                        }
                    });
        }
    }

    private void showMensagemBloqueioMesa(String mensagem) {
        final OnMensagemClickListener listener =
                new OnMensagemClickListener() {
                    @Override
                    public void onConfirmarClick() {
                        finish();
                    }

                    @Override
                    public void onCancelarClick() {
                    }
                };
        if (mActivityResumed) {
            showExcecaoNegocio(mensagem, listener);
        } else {
            mPendingExcecaoNegocioMessage = Pair.create(mensagem, listener);
        }
    }

    private void initMesaService() {
        if (mMesaService == null) {
            mMesaService = ServiceGenerator.createService(MesaService.class, this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mListItemManager != null) {
            mListItemManager.release();
            mListItemManager = null;
        }
    }

    @OnClick(R.id.fabConfirmaPedido)
    void onClickFabConfirmaPedido() {
        final List<Produto> lista = mProdutosAdapter.getProdutosSelecionados();

        if (lista.isEmpty()) {
            App.getInstancia().exibirMensagem(this, MensagemDialogo.MENSAGEM_ALERTA, "Atenção",
                    "Nenhum produto foi selecionado!");
            return;
        }

        new ConfirmaPedidosDialog(this, lista)
                .callback(this)
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_produtos, menu);
        MenuItem menuItem = menu.findItem(R.id.menu_pesquisa);

        DrawableHelper
                .withContext(this)
                .withColor(R.color.white)
                .withDrawable(menuItem.getIcon())
                .tint()
                .applyTo(menuItem);

        menuItem.expandActionView();

        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setQueryHint("Descrição ou Código barras");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(final String newText) {
                mProdutosAdapter.getFilter().filter(newText,
                        new Filter.FilterListener() {
                            @Override
                            public void onFilterComplete(int count) {
                                if (!mProdutosAdapter.isEmpty()) {
                                    mListItemManager.expandGroup(0);
                                }
                            }
                        }
                );

                return true;
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                mListItemManager.collapseAll();
                return true;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        limparDadosProdutos(mProdutosAdapter);
        requestDesbloqueioMesa();
    }

    private void requestDesbloqueioMesa() {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            initMesaService();
            if (mMesaService != null) {
                mMesaService
                        .desbloquearMesa(
                                mMesaEmAtendimento.getNumero(),
                                mUsuarioLogado.getMatricula(),
                                App.getInstancia().getCodigoCardapio()
                        )
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Subscriber<ResultadoDesbloqueioMesa>() {
                                    @Override
                                    public void onError(Throwable e) {
                                        showError(e);
                                    }

                                    @Override
                                    public void onNext(ResultadoDesbloqueioMesa resultado) {
                                        if (resultado.mensagem.equalsIgnoreCase("OK")) {
                                            ProdutosActivity.super.onBackPressed();
                                        } else {
                                            showExcecaoNegocio(resultado.mensagem, null);
                                        }
                                    }

                                    @Override
                                    public void onCompleted() {
                                    }
                                }
                        );
            }
        } else {
            FeedbackHelper.showOfflineMessage(this, null,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            requestDesbloqueioMesa();
                        }
                    });
        }
    }

    @Override
    public void onPedidoConfirmado(String matriculaGarcom) {
        List<Produto> produtosSelecionados =
                mProdutosAdapter.getProdutosSelecionados();

        if (produtosSelecionados.isEmpty()) {
            App.getInstancia()
                    .exibirMensagem(ProdutosActivity.this, MENSAGEM_ALERTA,
                            "Atenção", "Todos itens foram removidos!");
            return;
        }

        enviarPedido(produtosSelecionados, matriculaGarcom);
    }

    @Override
    public void onItemPedidoCancelado(int posicao) {
        mProdutosAdapter.removerProdutoSelecionado(posicao);
    }
}

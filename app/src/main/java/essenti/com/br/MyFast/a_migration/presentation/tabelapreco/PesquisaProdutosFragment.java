package essenti.com.br.MyFast.a_migration.presentation.tabelapreco;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.a_migration.data.produtos.ProdutoService;
import essenti.com.br.MyFast.a_migration.data.produtos.ProdutoService.ListarTodosProdutosResult;
import essenti.com.br.MyFast.android.widget.MensagemDialogo;
import essenti.com.br.MyFast.app.App;
import essenti.com.br.MyFast.barcode.CaptureActivityAnyOrientation;
import essenti.com.br.MyFast.produto.Produto;
import essenti.com.br.MyFast.produto.ProdutosAvulsosAdapter;
import essenti.com.br.MyFast.util.FeedbackHelper;
import essenti.com.br.MyFast.util.NetworkUtil;
import essenti.com.br.MyFast.util.eventbus.BusProvider;
import essenti.com.br.MyFast.webservice.ServiceGenerator;
import io.fabric.sdk.android.Fabric;
import java.util.List;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

import static essenti.com.br.MyFast.a_migration.data.produtos.ProdutoService.SEM_CODIGO_AUXILIAR;
import static essenti.com.br.MyFast.a_migration.data.produtos.ProdutoService.SEM_NOME_PRODUTO;
import static essenti.com.br.MyFast.android.widget.MensagemDialogo.MENSAGEM_ALERTA;
import static essenti.com.br.MyFast.android.widget.MensagemDialogo.MENSAGEM_ERRO;

/**
 * @author Filipe Bezerra
 */
public class PesquisaProdutosFragment extends Fragment {

    @BindView(R.id.list) protected RecyclerView mRecyclerViewProdutos;
    @BindView(R.id.container_ajuda) protected LinearLayout mAjudaContainer;
    @BindView(R.id.imagem_ajuda) protected ImageView mImageViewAjuda;
    @BindView(R.id.texto_ajuda) protected TextView mTextViewAjuda;

    private static final int ERRO_CARREGAMENTO_PRODUTOS = 1;
    private static final int ERRO_ENVIANDO_PEDIDO = 2;
    private static final int ERRO_BLOQUEIO_MESA = 3;
    private static final int ERRO_DESBLOQUEIO_MESA = 4;
    private Unbinder mUnbinder;

    @IntDef({
            ERRO_CARREGAMENTO_PRODUTOS,
            ERRO_ENVIANDO_PEDIDO,
            ERRO_BLOQUEIO_MESA,
            ERRO_DESBLOQUEIO_MESA
    })
    private @interface TipoError{}

    private ProdutosAvulsosAdapter mProdutosAvulsosAdapter;

    private MensagemDialogo mDialogoProgresso;

    private ProdutoService mProdutoService;

    public static PesquisaProdutosFragment newInstance() {
        return new PesquisaProdutosFragment();
    }

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        final View fragmentView
                = inflater.inflate(R.layout.fragment_pesquisa_produtos, container, false);
        mUnbinder = ButterKnife.bind(this, fragmentView);
        return fragmentView;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mRecyclerViewProdutos.setHasFixedSize(true);
        mRecyclerViewProdutos.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerViewProdutos.setAdapter(mProdutosAvulsosAdapter = new ProdutosAvulsosAdapter());
    }

    @Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_pesquisa_produtos, menu);
        MenuItem menuItem = menu.findItem(R.id.menu_pesquisa);
        menuItem.expandActionView();
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setQueryHint(getString(R.string.hint_descricao_produto));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.length() >= 3) {
                    requestListaProdutos(SEM_CODIGO_AUXILIAR, query);

                    searchView.setQuery(query, false);
                    searchView.clearFocus();
                    return true;
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    private void requestListaProdutos(final String pCodigoEmbalagem, final String pDescricao) {
        if (NetworkUtil.isDeviceConnectedToInternet(getContext())) {
            if (mProdutoService == null) {
                mProdutoService = ServiceGenerator
                        .createService(ProdutoService.class, getContext());
            }

            mProdutoService
                    .listarTodosProdutos(
                            App.getInstancia().getCodigoCardapio(),
                            pCodigoEmbalagem,
                            pDescricao)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            new Subscriber<ListarTodosProdutosResult>() {
                                @Override
                                public void onStart() {
                                    removerAjuda();
                                    showDialogoProgresso(
                                            getString(R.string.mensagem_buscando_produtos));
                                }

                                @Override
                                public void onError(Throwable e) {
                                    showError(e, ERRO_CARREGAMENTO_PRODUTOS);
                                }

                                @Override
                                public void onNext(ListarTodosProdutosResult pResult) {
                                    dismissDialogoProgresso();
                                    handleResponseListaProdutos(pResult);
                                }

                                @Override
                                public void onCompleted() {}
                            }
                    );
        } else {
            FeedbackHelper.showOfflineMessage(getContext(),
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            //TODO: Apos cancelar a requisição sem internet
                        }
                    },
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            requestListaProdutos(pCodigoEmbalagem, pDescricao);
                        }
                    });
        }
    }

    private void handleResponseListaProdutos(ListarTodosProdutosResult pResult) {
        List<Produto> produtos = pResult.produtos;

        if (produtos != null && !produtos.isEmpty()) {
            App.getInstancia().setProdutosAvulsos(produtos);
            mProdutosAvulsosAdapter.addAll(produtos);
            removerAjuda();
        } else {
            dismissDialogoProgresso();
            mProdutosAvulsosAdapter.clear();
            App.getInstancia()
                    .exibirMensagem(getActivity(), MENSAGEM_ALERTA,
                            getString(R.string.titulo_atencao),
                            getString(R.string.mensagem_nenhum_produto_encontrado),
                            false, true, null);
            indicarAjudar(getString(R.string.mensagem_ajuda_nenhum_produto_encontrado));
        }
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                return true;

            case R.id.menu_pesquisa_codigo_barras:
                IntentIntegrator
                        .forSupportFragment(this)
                        .setCaptureActivity(CaptureActivityAnyOrientation.class)
                        .setOrientationLocked(false)
                        .setBeepEnabled(true)
                        .setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES)
                        .setPrompt(getString(R.string.prompt_instrucao_leitura_codigo_barras))
                        .initiateScan();
                return true;

            default: return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IntentIntegrator.REQUEST_CODE) {
            final IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode,
                    resultCode, data);

            if (intentResult != null && !TextUtils.isEmpty(intentResult.getContents())) {
                requestListaProdutos(intentResult.getContents(), SEM_NOME_PRODUTO);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    private void removerAjuda() {
        if (mAjudaContainer.getVisibility() == View.VISIBLE) {
            mAjudaContainer.setVisibility(View.GONE);
            mRecyclerViewProdutos.setVisibility(View.VISIBLE);
        }
    }

    private void indicarAjudar(@Nullable String textoAjuda) {
        if (textoAjuda != null) {
            mTextViewAjuda.setText(textoAjuda);
        }

        if (mAjudaContainer.getVisibility() == View.GONE) {
            mAjudaContainer.setVisibility(View.VISIBLE);
            mRecyclerViewProdutos.setVisibility(View.GONE);
        }
    }

    private void showDialogoProgresso(String mensagem) {
        if (mDialogoProgresso == null) {
            mDialogoProgresso = App.getInstancia().exibirProgresso(getActivity(),
                    getString(R.string.titulo_processando), mensagem);
            mDialogoProgresso.exibir();
        } else {
            mDialogoProgresso.setTexto(mensagem);
        }
    }

    private void dismissDialogoProgresso() {
        if (mDialogoProgresso != null) {
            if (mDialogoProgresso.isExibindo()) {
                mDialogoProgresso.esconder();
            }

            mDialogoProgresso = null;
        }
    }

    private void showError(Throwable e, @TipoError int tipoErro) {
        dismissDialogoProgresso();

        if (Fabric.isInitialized()) {
            Crashlytics.logException(e);
        }

        String mensagem = getString(R.string.mensagem_ocorreu_falha);
        String when = "";
        switch (tipoErro) {
            case ERRO_CARREGAMENTO_PRODUTOS: {
                when = getString(R.string.mensagem_erro_carregando_produtos);
                break;
            }
            case ERRO_ENVIANDO_PEDIDO: {
                when = getString(R.string.mensagem_erro_enviado_pedido);
                break;
            }
            case ERRO_BLOQUEIO_MESA: {
                break;
            }
            case ERRO_DESBLOQUEIO_MESA: {
                break;
            }
        }

        Timber.e(e, when);

        mensagem += when + getString(R.string.mensagem_complemento_ocorreu_falha);

        indicarAjudar(mensagem);
        App.getInstancia().exibirMensagem(getActivity(), MENSAGEM_ERRO,
                getString(R.string.titulo_atencao), mensagem);
    }

    @OnClick(R.id.button_incluir_na_mesa) void onClickButtonIncluirNaMesa() {
        if (mProdutosAvulsosAdapter.isProdutosSelecionadosEmpty()) {
            App.getInstancia()
                    .exibirMensagem(getActivity(), MENSAGEM_ALERTA,
                            getString(R.string.titulo_atencao),
                            getString(R.string.mensagem_nenhum_produto_selecionado));
            return;
        }

        BusProvider.getInstance().post(
                ProdutosSelecionadosEvent.newEvent(
                        mProdutosAvulsosAdapter.getProdutosSelecionados()));
    }
}

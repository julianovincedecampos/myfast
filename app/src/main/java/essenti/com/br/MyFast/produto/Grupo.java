package essenti.com.br.MyFast.produto;

import android.os.Parcel;
import android.os.Parcelable;

import com.hannesdorfmann.parcelableplease.annotation.ParcelablePlease;

import java.util.ArrayList;
import java.util.List;

/**
 * Entidade Grupo
 *
 * @author Diego, Filipe Bezerra
 * @version #, 13/01/2016
 * @since 1.0.0
 */
@ParcelablePlease
public class Grupo implements Parcelable {
    int codigo;
    String descricao;
    List<Produto> produtos;

    Grupo() {}

    public Grupo(int codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    public Grupo(String descricao) {
        this.codigo = 1;
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public List<Produto> getProdutos() {
        if (this.produtos == null) {
            this.produtos = new ArrayList<Produto>();
        }
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }

    public double getValorTotal() {
        double valor = 0;
        for (Produto produto : this.getProdutos()) {
            valor += 0d;
        }
        return valor;
    }

    @Override
    public boolean equals(Object o) {
        return ((Grupo) o).getCodigo() == this.getCodigo();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        GrupoParcelablePlease.writeToParcel(this, dest, flags);
    }

    public static final Creator<Grupo> CREATOR = new Creator<Grupo>() {
        public Grupo createFromParcel(Parcel source) {
            Grupo target = new Grupo();
            GrupoParcelablePlease.readFromParcel(target, source);
            return target;
        }

        public Grupo[] newArray(int size) {
            return new Grupo[size];
        }
    };
}

package essenti.com.br.MyFast.mesa;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.implments.SwipeItemRecyclerMangerImpl;
import com.daimajia.swipe.interfaces.SwipeAdapterInterface;
import com.daimajia.swipe.interfaces.SwipeItemMangerInterface;
import com.daimajia.swipe.util.Attributes;
import essenti.com.br.MyFast.R;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Adaptador da listagem de mesas da tela {@link MesasActivity}.
 *
 * @author Filipe Bezerra
 * @see MesasActivity
 */
public class MesaAdapter extends RecyclerView.Adapter<MesaAdapter.ViewHolder>
        implements Filterable, SwipeItemMangerInterface, SwipeAdapterInterface {

    @NonNull private List<Mesa> mMesas;

    @NonNull private Context mContext;

    @Nullable private MesaCallbacks mCallbacks;

    private MesaFilter mMesaFilter;

    private List<Mesa> mOriginalValues;

    public SwipeItemRecyclerMangerImpl mItemManger = new SwipeItemRecyclerMangerImpl(this);

    public MesaAdapter(@NonNull Context context, @NonNull List<Mesa> mesas,
            @Nullable MesaCallbacks callbacks) {
        mMesas = mesas;
        mContext = context;
        mCallbacks = callbacks;
        setMode(Attributes.Mode.Multiple);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(mContext)
                .inflate(R.layout.item_mesa, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Mesa mesa = mMesas.get(position);

        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);

        holder.txtStatusMesa.setText(mesa.getStatus());
        holder.txtNomeCliente.setText(mesa.getNomeCliente());
        holder.txtNumeroMesa.setText(
                String.format(Locale.getDefault(), "%03d", mesa.getNumero()));
        holder.txtValorTotalMesa.setText(
                NumberFormat.getCurrencyInstance().format(mesa.getValorTotal()));

        String nomeOperador = TextUtils.isEmpty(mesa.getNomeUltimoGarcom()) ? "-" :
                mesa.getNomeUltimoGarcom();
        holder.txtOperadorMesa.setText(
                String.format(Locale.getDefault(), "Op.: %s", nomeOperador));

        holder.lnlAdicionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCallbacks != null) {
                    mCallbacks.onClickAdicionarItem(mesa);
                }
            }
        });
        holder.lnlFechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCallbacks != null) {
                    mCallbacks.onClickFecharConta(mesa);
                }
            }
        });

        holder.txtValorTotalMesa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCallbacks != null) {
                    mCallbacks.onClickItem(getMesa(holder.getAdapterPosition()));
                }
            }
        });
        mItemManger.bindView(holder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return mMesas.size();
    }

    @Override
    public long getItemId(int position) {
        return mMesas.get(position).getNumero();
    }

    @Override
    public Filter getFilter() {
        if (mMesaFilter == null) {
            mMesaFilter = new MesaFilter();
        }
        return mMesaFilter;
    }

    public void setMesas(@NonNull List<Mesa> mesas) {
        if (mOriginalValues != null) {
            mOriginalValues.clear();
            mOriginalValues.addAll(mesas);
        } else {
            mMesas.clear();
            mMesas.addAll(mesas);
        }
        notifyDataSetChanged();
    }

    public Mesa getMesa(int position) {
        if (position < 0 || position >= getItemCount()) {
            return null;
        }

        return mMesas.get(position);
    }

    public void setMesa(@NonNull Mesa mesa) {
        final int position = mMesas.indexOf(mesa);

        if (position != -1) {
            mMesas.set(position, mesa);
            notifyItemChanged(position);
        }
    }

    /**
     * Remove a mesa da lista se o status da mesa passada como argumento é diferente do
     * status da mesa na lista.
     * <br><br>
     * Este método é usado para espelhar os itens nesta lista com o
     * {@link MesasActivity#FILTRO_STATUS_SELECIONADO}. Não deverá ser usado caso o
     * {@link MesasActivity#FILTRO_STATUS_SELECIONADO} seja {@link MesasActivity#TODAS_MESAS}
     * ou {@link MesasActivity#MINHAS_MESAS}.
     *
     * @param mesa mesa com status alterado.
     * @return se foi removida <code>true</code>.
     */
    public boolean removeIfStatusMudou(@NonNull Mesa mesa) {
        int position = mMesas.indexOf(mesa);

        if (position != -1) {
            Mesa mesaNaLista = mMesas.get(position);

            if (mesaNaLista.getStatusMesa() != mesa.getStatusMesa()) {
                mMesas.remove(mesaNaLista);
                notifyItemRemoved(position);
                return true;
            }
        }
        return false;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipeLayoutMesa;
    }

    @Override
    public void openItem(int position) {
        mItemManger.openItem(position);
    }

    @Override
    public void closeItem(int position) {
        mItemManger.closeItem(position);
    }

    @Override
    public void closeAllExcept(SwipeLayout layout) {
        mItemManger.closeAllExcept(layout);
    }

    @Override
    public void closeAllItems() {
        mItemManger.closeAllItems();
    }

    @Override
    public List<Integer> getOpenItems() {
        return mItemManger.getOpenItems();
    }

    @Override
    public List<SwipeLayout> getOpenLayouts() {
        return mItemManger.getOpenLayouts();
    }

    @Override
    public void removeShownLayouts(SwipeLayout layout) {
        mItemManger.removeShownLayouts(layout);
    }

    @Override
    public boolean isOpen(int position) {
        return mItemManger.isOpen(position);
    }

    @Override
    public Attributes.Mode getMode() {
        return mItemManger.getMode();
    }

    @Override
    public void setMode(Attributes.Mode mode) {
        mItemManger.setMode(mode);
    }

    private class MesaFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            FilterResults results = new FilterResults();

            if (mOriginalValues == null) {
                mOriginalValues = new ArrayList<>(mMesas);
            }

            if (TextUtils.isEmpty(prefix)) {
                List<Mesa> list = new ArrayList<>(mOriginalValues);
                results.values = list;
                results.count = list.size();
            } else {
                final String prefixString = prefix.toString().toLowerCase();

                List<Mesa> values = new ArrayList<>(mOriginalValues);

                final List<Mesa> newValues = new ArrayList<>();

                for(Mesa mesa : values) {
                    final String valueText = mesa.toString().toLowerCase();

                    if (valueText.startsWith(prefixString)) {
                        newValues.add(mesa);
                    } else {
                        final String[] words = valueText.split(" ");

                        for (String word : words) {
                            if (word.startsWith(prefixString)) {
                                newValues.add(mesa);
                                break;
                            }
                        }
                    }
                }

                results.values = newValues;
                results.count = newValues.size();
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            //noinspection unchecked
            mMesas = (List<Mesa>) results.values;
            notifyDataSetChanged();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.swipeLayoutMesa) SwipeLayout swipeLayout;
        @BindView(R.id.lnlAdicionar) LinearLayout lnlAdicionar;
        @BindView(R.id.lnlFechar) LinearLayout lnlFechar;
        @BindView(R.id.txtNomeCliente) TextView txtNomeCliente;
        @BindView(R.id.txtNumeroMesa) TextView txtNumeroMesa;
        @BindView(R.id.txtValorTotalMesa) TextView txtValorTotalMesa;
        @BindView(R.id.txtOperadorMesa) TextView txtOperadorMesa;
        @BindView(R.id.txtStatusMesa) TextView txtStatusMesa;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mCallbacks != null) {
                        mCallbacks.onClickItem(getMesa(getAdapterPosition()));
                    }
                }
            });
        }
    }

    public interface MesaCallbacks {
        void onClickAdicionarItem(Mesa mesa);

        void onClickFecharConta(Mesa mesa);

        void onClickItem(Mesa mesa);
    }
}

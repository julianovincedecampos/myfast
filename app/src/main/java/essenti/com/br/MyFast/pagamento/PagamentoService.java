package essenti.com.br.MyFast.pagamento;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

/**
 *
 *
 * @author Filipe Bezerra
 * @version 1.4.0, 13/04/2016
 * @since 1.4.0
 */
public interface PagamentoService {
    @POST("ServiceMesa.svc/InserirPagamento")
    Observable<InserirPagamentoResponse> inserirPagamento(@Body Pagamento pagamento);

    @GET("ServiceMesa.svc/ListarPagamento/{numeroMesa}/{codigoCardapio}")
    Observable<ListarPagamentoResponse> listarPagamentos(@Path("numeroMesa") int numeroMesa,
            @Path("codigoCardapio") int codigoCardapio);

    @GET("ServiceMesa.svc/ObterTransacaoPagamento")
    Observable<ObterTransacaoPagamentoResponse> obterTransacaoPagamento();

    final class InserirPagamentoResponse {
        @SerializedName("InserirPagamentoResult")
        public String result;
    }

    final class ListarPagamentoResponse {
        @SerializedName("ListarPagamentoResult")
        public List<Pagamento> result;
    }

    final class ObterTransacaoPagamentoResponse {
        @SerializedName("ObterTransacaoPagamentoResult")
        public String result;
    }
}

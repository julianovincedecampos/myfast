package essenti.com.br.MyFast.mesa;

import com.google.gson.annotations.SerializedName;

/**
 * @author Filipe Bezerra
 */
public class ResultadoBloqueioMesa {
    @SerializedName("BloquearMesaResult")
    public String mensagem;
}

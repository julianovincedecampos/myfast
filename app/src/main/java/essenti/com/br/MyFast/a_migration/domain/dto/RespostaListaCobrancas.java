package essenti.com.br.MyFast.a_migration.domain.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Filipe Bezerra
 */
public class RespostaListaCobrancas {
    @SerializedName("ListarCobrancaResult") @Expose public List<CobrancaDto> cobrancas;
}

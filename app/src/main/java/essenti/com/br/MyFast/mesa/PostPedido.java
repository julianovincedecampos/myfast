package essenti.com.br.MyFast.mesa;

import android.net.Uri;
import android.support.annotation.NonNull;
import essenti.com.br.MyFast.android.asynctask.AbstractAsyncTask;
import essenti.com.br.MyFast.android.asynctask.TaskCallback;
import essenti.com.br.MyFast.android.asynctask.TaskException;
import essenti.com.br.MyFast.app.App;
import essenti.com.br.MyFast.produto.Complemento;
import essenti.com.br.MyFast.produto.Produto;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PostPedido extends AbstractAsyncTask<Void, Void, String> {
    private static final String PATH_INSERIR_PEDIDO = "ServiceMesa.svc/InserirPedido";

    private final boolean mIsProdutoAvulso;
    @NonNull
    private final Mesa mMesaDestino;
    @NonNull
    private final String mMatriculaGarcom;
    @NonNull
    private final List<Produto> mProdutos;

    public PostPedido(boolean isProdutoAvulso, @NonNull Mesa mesaDestino,
            @NonNull String matriculaGarcom,
            @NonNull List<Produto> produtos, @NonNull TaskCallback<String> callback) {
        super(callback);
        mIsProdutoAvulso = isProdutoAvulso;
        mMesaDestino = mesaDestino;
        mMatriculaGarcom = matriculaGarcom;
        mProdutos = produtos;
    }

    @Override
    protected String doInBackground(Void... params) {
        final String url = Uri.parse(App.getInstancia().getURL())
                .buildUpon()
                .appendPath(PATH_INSERIR_PEDIDO)
                .build()
                .toString();

        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(url);
        httppost.setHeader("Content-type", "application/json");

        try {
            JSONObject jsonObject = convertGruposToJson();
            httppost.setEntity(new StringEntity(jsonObject.toString(), HTTP.UTF_8));

            HttpResponse response = httpclient.execute(httppost);
            HttpEntity responseEntity = response.getEntity();

            if (responseEntity != null) {
                InputStream instream = responseEntity.getContent();
                String jsonResponse = getStreamData(instream);
                instream.close();
                JSONObject objeto = new JSONObject(jsonResponse);
                String resultado = objeto.getString("InserirPedidoResult");

                if (resultado.equals("OK")) {
                    return resultado;
                } else {
                    mException = TaskException.of(resultado);
                    return null;
                }
            }
        } catch (JSONException | IOException e) {
            mException = TaskException
                    .of(String.format("Ocorreu um erro ao inserir os itens nesta mesa! Erro: %s",
                            e.getMessage()));
        }

        return null;
    }

    private JSONObject convertGruposToJson() throws JSONException {
        JSONObject jsonPedido = new JSONObject();

        JSONArray jsonListaProduto = new JSONArray();
        for (Produto produto : mProdutos) {
            JSONObject jsonProduto = new JSONObject();
            JSONArray jsonListaComplemento = new JSONArray();
            jsonProduto.put("Codigo", produto.getCodigo());
            jsonProduto.put("Preco", produto.getValorUnitario());
            jsonProduto.put("Quantidade", produto.getQuantidade());
            jsonProduto.put("Codauxiliar", produto.getCodigoAuxiliar());

            if (produto.getObservacao().length() > 0) {
                jsonProduto.put("Observacao", produto.getObservacao());
            }
            if (produto.getComplementos().size() > 0) {
                for (Complemento complemento : produto.getComplementos()) {
                    if (complemento.getSelecionado()) {
                        JSONObject jsonComplemento = new JSONObject();
                        jsonComplemento.put("Codigo", complemento.getCodigo());
                        jsonComplemento.put("Preco", complemento.getValorUnitario());
                        jsonComplemento.put("Quantidade", complemento.getQuantidade());
                        jsonListaComplemento.put(jsonComplemento);
                    }
                }
                if (jsonListaComplemento.length() > 0) {
                    jsonProduto.put("Complementos", jsonListaComplemento);
                }
            }
            jsonListaProduto.put(jsonProduto);
        }
        jsonPedido.put("Produtos", jsonListaProduto);
        jsonPedido.put("Numero", mMesaDestino.getNumero());
        jsonPedido.put("Codcardapio", App.getInstancia().getCodigoCardapio());
        jsonPedido.put("Matricula", mMatriculaGarcom);
        jsonPedido.put("Codambiente", App.getInstancia().getCodigoAmbiente());
        jsonPedido.put("Codcliente", mMesaDestino.getCodigoCliente());

        return jsonPedido;
    }
}

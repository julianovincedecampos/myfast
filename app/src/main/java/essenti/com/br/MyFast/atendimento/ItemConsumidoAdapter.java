package essenti.com.br.MyFast.atendimento;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.produto.Produto;

/**
 * Adaptador da listagem de itens consumidos da tela {@link AtendimentoMesaActivity}.
 *
 * @author Filipe Bezerra
 * @version 1.4.0, 20/04/2016
 * @since 1.4.0
 */
public class ItemConsumidoAdapter
        extends RecyclerView.Adapter<ItemConsumidoAdapter.ViewHolder> {

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtTotalItem) TextView txtTotalItem;
        @BindView(R.id.txtValorUnitario) TextView txtValorUnitario;
        @BindView(R.id.txtValorTotalItem) TextView txtValorTotalItem;
        @BindView(R.id.txtDescricaoItem) TextView txtDescricaoItem;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull private List<Produto> mItensConsumidos;

    public ItemConsumidoAdapter(@NonNull List<Produto> itemConsumidos) {
        mItensConsumidos = itemConsumidos;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_lista_produto_mesa, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Produto itemConsumido = mItensConsumidos.get(position);

        holder.txtTotalItem
                .setText(String.format(Locale.getDefault(), "%03d",
                        itemConsumido.getQuantidade()));
        holder.txtDescricaoItem.setText(itemConsumido.getDescricao());
        holder.txtValorUnitario
                .setText(String.format("Valor Unit. %s",
                        NumberFormat.getCurrencyInstance().format(
                                itemConsumido.getValorUnitario())));
        holder.txtValorTotalItem.setText(
                NumberFormat.getCurrencyInstance().format(itemConsumido.getValorTotal()));
    }

    @Override
    public long getItemId(int position) {
        return mItensConsumidos.get(position).getCodigo();
    }

    @Override
    public int getItemCount() {
        return mItensConsumidos.size();
    }

    @NonNull
    public List<Produto> getItensConsumidos() {
        return mItensConsumidos;
    }

    public void clearData() {
        if (!mItensConsumidos.isEmpty()) {
            mItensConsumidos.clear();
            notifyDataSetChanged();
        }
    }

    public void swapItems(List<Produto> itensMesa) {
        mItensConsumidos.clear();
        if(mItensConsumidos.addAll(itensMesa)) {
            notifyDataSetChanged();
        }
    }
}

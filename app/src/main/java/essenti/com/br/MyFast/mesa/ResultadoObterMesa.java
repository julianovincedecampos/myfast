package essenti.com.br.MyFast.mesa;

import com.google.gson.annotations.SerializedName;

/**
 * .
 *
 * @author Filipe Bezerra
 * @version 1.4.0, 30/03/2016
 * @since 1.4.0
 */
public class ResultadoObterMesa {
    @SerializedName("ObterMesaResult")
    public Mesa mesa;
}
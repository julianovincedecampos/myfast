package essenti.com.br.MyFast.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.MaterialDialog.SingleButtonCallback;
import essenti.com.br.MyFast.R;

/**
 * @author Filipe Bezerra.
 */
public class FeedbackHelper {
    private FeedbackHelper() {
        // no constructor
    }

    public static void showOfflineMessage(@NonNull Context context,
            @Nullable SingleButtonCallback cancelCallback,
            @Nullable SingleButtonCallback retryCallback) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(context)
                .title(R.string.offline_message_title)
                .content(R.string.offline_message)
                .positiveText(android.R.string.ok);

        if (cancelCallback != null) {
            builder
                    .negativeText(android.R.string.cancel)
                    .onNegative(cancelCallback);
        }

        if (retryCallback != null) {
            builder
                    .neutralText(R.string.retry)
                    .onNeutral(retryCallback);
        }


        builder.show();
    }
}
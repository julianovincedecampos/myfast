package essenti.com.br.MyFast.produto;

import android.net.Uri;
import android.support.annotation.NonNull;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import essenti.com.br.MyFast.android.asynctask.AbstractAsyncTask;
import essenti.com.br.MyFast.app.App;
import essenti.com.br.MyFast.android.asynctask.TaskCallback;
import essenti.com.br.MyFast.android.asynctask.TaskException;

public class GetGrupos extends AbstractAsyncTask<Void, Void, List<Grupo>> {
    private static final String PATH_GRUPOS = "ServiceProduto.svc/Grupos";

    public GetGrupos(@NonNull TaskCallback<List<Grupo>> callback) {
        super(callback);
    }

    @Override
    protected List<Grupo> doInBackground(Void... params) {
        final String url = Uri.parse(App.getInstancia().getURL())
                .buildUpon()
                .appendPath(PATH_GRUPOS)
                .appendPath(String.valueOf(App.getInstancia().getCodigoCardapio()))
                .build()
                .toString();

        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(url);

        try {
            HttpResponse response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream instream = entity.getContent();
                String json = getStreamData(instream);
                instream.close();
                return convertJsonToGrupos(json);
            }
        } catch (JSONException | IOException e) {
            mException = TaskException.of(e);
        }

        return null;
    }

    private List<Grupo> convertJsonToGrupos(String jsonString) throws JSONException {
        List<Grupo> listaGrupo = new ArrayList<>();

        JSONObject jsonResultado = new JSONObject(jsonString);
        JSONArray jsonListaGrupo = jsonResultado.getJSONArray("ListarGruposResult");

        JSONObject jsonGrupo;
        JSONArray jsonListaProduto;
        JSONObject jsonProduto;
        JSONArray jsonListaComplemento;
        JSONObject jsonComplemento;

        for (int i = 0; i < jsonListaGrupo.length(); i++) {
            jsonGrupo = jsonListaGrupo.getJSONObject(i);
            Grupo grupo = new Grupo(jsonGrupo.getInt("Codgrupo"), jsonGrupo.getString("Nomegrupo"));

            jsonListaProduto = jsonGrupo.getJSONArray("Produtos");
            for (int x = 0; x < jsonListaProduto.length(); x++) {
                jsonProduto = jsonListaProduto.getJSONObject(x);
                Produto produto = new Produto();

                produto.setDescricao(jsonProduto.getString("Descricao"));
                produto.setCodigo(jsonProduto.getInt("Codprod"));
                produto.setValorUnitario(jsonProduto.getDouble("Preco"));
                produto.setCodigoAuxiliar(jsonProduto.getString("Codauxiliar"));
                produto.setSequencia(jsonProduto.getInt("Codigo"));

                jsonListaComplemento = jsonProduto.getJSONArray("Complementos");
                for (int y = 0; y < jsonListaComplemento.length(); y++) {
                    jsonComplemento = jsonListaComplemento.getJSONObject(y);
                    Complemento complemento = new Complemento();
                    complemento.setDescricao(jsonComplemento.getString("Nomecomplemento"));
                    complemento.setCodigo(jsonComplemento.getInt("Codcomp"));
                    complemento.setValorUnitario(jsonComplemento.getDouble("Precocomp"));
                    produto.getComplementos().add(complemento);
                }

                grupo.getProdutos().add(produto);
            }

            listaGrupo.add(grupo);
        }

        return listaGrupo;
    }
}

package essenti.com.br.MyFast.relatorio;

import java.util.Calendar;
import java.util.Locale;

/**
 * @author Filipe Bezerra
 * @version 1.4.0, 04/04/2016
 */
public class FiltroRelatorio {
    private final Calendar calendar;
    private final String monthName;
    private final int month;
    private final int year;

    public FiltroRelatorio(Calendar calendar) {
        this.calendar = calendar;
        monthName = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
        month = calendar.get(Calendar.MONTH) + 1;
        year = calendar.get(Calendar.YEAR);
    }

    public String toMonthAndYear() {
        return String.format("%02d%d", month, year);
    }

    @Override
    public String toString() {
        return String.format("%s/%d", monthName, year);
    }
}

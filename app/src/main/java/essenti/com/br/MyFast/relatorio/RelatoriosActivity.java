package essenti.com.br.MyFast.relatorio;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import essenti.com.br.MyFast.util.FeedbackHelper;
import essenti.com.br.MyFast.util.NetworkUtil;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import essenti.com.br.MyFast.app.App;
import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.util.eventbus.BusProvider;
import essenti.com.br.MyFast.android.fragment.FragmentAdapter;
import essenti.com.br.MyFast.usuario.Usuario;
import essenti.com.br.MyFast.usuario.UsuarioHelper;
import essenti.com.br.MyFast.webservice.ServiceGenerator;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * Adaptador da lista de {@link essenti.com.br.MyFast.relatorio.Comissao}.
 *
 * @author Filipe Bezerra
 * @version 1.4.0
 * @since 1.4.0
 */
public class RelatoriosActivity extends AppCompatActivity {

    private FragmentAdapter mFragmentAdapter;

    private RelatorioService mRelatorioService;

    private Usuario mUsuarioLogado;

    @BindView(R.id.toolbar) Toolbar mToolbar;

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private List<FiltroRelatorio> generateFilterOptions() {
        List<FiltroRelatorio> filters = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        filters.add(new FiltroRelatorio(calendar));

        int count = 1;
        do {
            calendar.add(Calendar.MONTH, -1);
            filters.add(new FiltroRelatorio(calendar));
            count++;
        } while (count < 13);

        return Collections.unmodifiableList(filters);
    }

    private void requestRelatorio(final String monthAndYear) {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            if (mRelatorioService == null) {
                mRelatorioService = ServiceGenerator.createService(RelatorioService.class, this);
            }

            if (mUsuarioLogado == null) {
                mUsuarioLogado = UsuarioHelper.retrieveUsuarioLogado(this);
            }

            if (mRelatorioService != null && mUsuarioLogado != null) {
                mRelatorioService
                        .listarVendas(
                                App.getInstancia().getCodigoCardapio(),
                                monthAndYear,
                                mUsuarioLogado.getMatricula()
                        )
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Action1<ListaVendas>() {
                                    @Override
                                    public void call(ListaVendas listaVendas) {
                                        BusProvider.getInstance().post(listaVendas);
                                    }
                                }
                        );
            }
        } else {
            FeedbackHelper.showOfflineMessage(this, null,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            requestRelatorio(monthAndYear);
                        }
                    });
        }
    }

    private void setupSpinner() {
        final List<FiltroRelatorio> filtros = generateFilterOptions();

        final FiltroRelatorioAdapter spinnerAdapter =
                new FiltroRelatorioAdapter(mToolbar.getContext(), filtros);

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setAdapter(spinnerAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                FiltroRelatorio filtroRelatorio = spinnerAdapter.getItem(position);
                if (filtroRelatorio != null) {
                    requestRelatorio(filtroRelatorio.toMonthAndYear());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void setupTabs() {
        mFragmentAdapter = new FragmentAdapter(getSupportFragmentManager());
        /*mFragmentAdapter.addFragment(RelatorioMetaFragment.newInstance(),
                getString(R.string.title_fragment_relatorio_meta));*/
        mFragmentAdapter.addFragment(RelatorioComissaoFragment.newInstance(),
                getString(R.string.title_fragment_relatorio_comissao));

        ViewPager viewPager = ButterKnife.findById(this, R.id.container);
        viewPager.setAdapter(mFragmentAdapter);

        TabLayout tabLayout = ButterKnife.findById(this, R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relatorios);
        ButterKnife.bind(this);
        setupToolbar();
        setupSpinner();
        setupTabs();
    }
}

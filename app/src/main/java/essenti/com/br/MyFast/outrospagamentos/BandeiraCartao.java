package essenti.com.br.MyFast.outrospagamentos;

import android.support.annotation.DrawableRes;

/**
 * @author Filipe Bezerra
 */
class BandeiraCartao {
    private final String nome;

    private final int icone;

    public static BandeiraCartao from(String nome, @DrawableRes int icone) {
        return new BandeiraCartao(nome, icone);
    }

    private BandeiraCartao(String nome, int icone) {
        this.nome = nome;
        this.icone = icone;
    }

    public String getNome() {
        return nome;
    }

    public int getIcone() {
        return icone;
    }
}

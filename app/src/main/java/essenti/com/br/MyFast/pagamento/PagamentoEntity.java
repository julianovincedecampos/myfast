package essenti.com.br.MyFast.pagamento;

import essenti.com.br.MyFast.a_migration.data.util.RealmAutoIncrement;
import essenti.com.br.MyFast.util.Preconditions;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Classe que representa uma pagamento feito com cartão de crédito. Esta classe é usada
 * para persistir os dodos em SQLite.
 *
 * @author Filipe Bezerra
 * @since 1.4.0
 */
public class PagamentoEntity extends RealmObject {
    public static final String FIELD_MESA = "mesa";

    @PrimaryKey
    private Integer id = RealmAutoIncrement.getInstance(this.getClass()).getNextIdFromModel();

    private String idTransacao;

    @Required
    private Integer mesa;

    private Integer codCardapio;

    private String captura;

    private String codAutorizacao;

    private String codProdutoMatriz;

    private String codProdutoSecundario;

    private String estAcquirer;

    private String estVenda;

    private String fluxo;

    private String idAplicacao;

    @Required
    private String nomeProdutoMatriz;

    private String nomeProdutoSecundario;

    private String nsu;

    private String referencia;

    private String retornoAplicacao;

    private String tipoTransacao;

    @Required
    private Double valor;

    private String versaoAppFinanceira;

    private String comprovanteCliente;

    private String comprovanteVendedor;

    private String pan;

    private String email;

    private String nomeBandeira;

    private int parcelas;

    private String dataPagamento;

    private String codigoCobranca;

    /**
     * Default constructor required for Realm library.
     */
    public PagamentoEntity() {}

    public PagamentoEntity(Pagamento viewObject) {
        Preconditions.checkNotNull(viewObject, "Cannot convert null Pagamento object to entity.");
        idTransacao = viewObject.getIdTransacao();
        mesa = viewObject.getMesa();
        codCardapio = viewObject.getCodCardapio();
        captura = viewObject.getCaptura();
        codAutorizacao = viewObject.getCodAutorizacao();
        codProdutoMatriz = viewObject.getCodProdutoMatriz();
        codProdutoSecundario = viewObject.getCodProdutoSecundario();
        estAcquirer = viewObject.getEstAcquirer();
        estVenda = viewObject.getEstVenda();
        fluxo = viewObject.getFluxo();
        idAplicacao = viewObject.getIdAplicacao();
        nomeProdutoMatriz = viewObject.getNomeProdutoMatriz();
        nomeProdutoSecundario = viewObject.getNomeProdutoSecundario();
        nsu = viewObject.getNsu();
        referencia = viewObject.getReferencia();
        retornoAplicacao = viewObject.getRetornoAplicacao();
        tipoTransacao = viewObject.getTipoTransacao();
        valor = viewObject.getValor();
        versaoAppFinanceira = viewObject.getVersaoAppFinanceira();
        comprovanteCliente = viewObject.getComprovanteCliente();
        comprovanteVendedor = viewObject.getComprovanteVendedor();
        pan = viewObject.getPan();
        email = viewObject.getEmail();
        nomeBandeira = viewObject.getNomeBandeira();
        parcelas = viewObject.getParcelas();
        dataPagamento = viewObject.getDataPagamento();
        codigoCobranca = viewObject.getCodCobranca();
    }

    public Integer getId() {
        return id;
    }

    public PagamentoEntity setId(Integer pId) {
        if (pId > 0) {
            id = pId;
        }
        return this;
    }

    public String getIdTransacao() {
        return idTransacao;
    }

    public void setIdTransacao(String idTransacao) {
        this.idTransacao = idTransacao;
    }

    public Integer getMesa() {
        return mesa;
    }

    public void setMesa(Integer mesa) {
        this.mesa = mesa;
    }

    public Integer getCodCardapio() {
        return codCardapio;
    }

    public void setCodCardapio(Integer codCardapio) {
        this.codCardapio = codCardapio;
    }

    public String getCaptura() {
        return captura;
    }

    public void setCaptura(String captura) {
        this.captura = captura;
    }

    public String getCodAutorizacao() {
        return codAutorizacao;
    }

    public void setCodAutorizacao(String codAutorizacao) {
        this.codAutorizacao = codAutorizacao;
    }

    public String getCodProdutoMatriz() {
        return codProdutoMatriz;
    }

    public void setCodProdutoMatriz(String codProdutoMatriz) {
        this.codProdutoMatriz = codProdutoMatriz;
    }

    public String getCodProdutoSecundario() {
        return codProdutoSecundario;
    }

    public void setCodProdutoSecundario(String codProdutoSecundario) {
        this.codProdutoSecundario = codProdutoSecundario;
    }

    public String getEstAcquirer() {
        return estAcquirer;
    }

    public void setEstAcquirer(String estAcquirer) {
        this.estAcquirer = estAcquirer;
    }

    public String getEstVenda() {
        return estVenda;
    }

    public void setEstVenda(String estVenda) {
        this.estVenda = estVenda;
    }

    public String getFluxo() {
        return fluxo;
    }

    public void setFluxo(String fluxo) {
        this.fluxo = fluxo;
    }

    public String getIdAplicacao() {
        return idAplicacao;
    }

    public void setIdAplicacao(String idAplicacao) {
        this.idAplicacao = idAplicacao;
    }

    public String getNomeProdutoMatriz() {
        return nomeProdutoMatriz;
    }

    public void setNomeProdutoMatriz(String nomeProdutoMatriz) {
        this.nomeProdutoMatriz = nomeProdutoMatriz;
    }

    public String getNomeProdutoSecundario() {
        return nomeProdutoSecundario;
    }

    public void setNomeProdutoSecundario(String nomeProdutoSecundario) {
        this.nomeProdutoSecundario = nomeProdutoSecundario;
    }

    public String getNsu() {
        return nsu;
    }

    public void setNsu(String nsu) {
        this.nsu = nsu;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getRetornoAplicacao() {
        return retornoAplicacao;
    }

    public void setRetornoAplicacao(String retornoAplicacao) {
        this.retornoAplicacao = retornoAplicacao;
    }

    public String getTipoTransacao() {
        return tipoTransacao;
    }

    public void setTipoTransacao(String tipoTransacao) {
        this.tipoTransacao = tipoTransacao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getVersaoAppFinanceira() {
        return versaoAppFinanceira;
    }

    public void setVersaoAppFinanceira(String versaoAppFinanceira) {
        this.versaoAppFinanceira = versaoAppFinanceira;
    }

    public String getComprovanteCliente() {
        return comprovanteCliente;
    }

    public void setComprovanteCliente(String comprovanteCliente) {
        this.comprovanteCliente = comprovanteCliente;
    }

    public String getComprovanteVendedor() {
        return comprovanteVendedor;
    }

    public void setComprovanteVendedor(String comprovanteVendedor) {
        this.comprovanteVendedor = comprovanteVendedor;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNomeBandeira() {
        return nomeBandeira;
    }

    public void setNomeBandeira(String nomeBandeira) {
        this.nomeBandeira = nomeBandeira;
    }

    public int getParcelas() {
        return parcelas;
    }

    public void setParcelas(int parcelas) {
        this.parcelas = parcelas;
    }

    public String getDataPagamento() {
        return dataPagamento;
    }

    public void setDataPagamento(String dataPagamento) {
        this.dataPagamento = dataPagamento;
    }

    public String getCodigoCobranca() {
        return codigoCobranca;
    }

    public PagamentoEntity setCodigoCobranca(String pCodigoCobranca) {
        codigoCobranca = pCodigoCobranca;
        return this;
    }
}

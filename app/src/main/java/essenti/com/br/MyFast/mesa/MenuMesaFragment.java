package essenti.com.br.MyFast.mesa;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import essenti.com.br.MyFast.R;

/**
 * Fragmento de tela que contém um menu com opções de fitro que define que tipo de mesa deve ser
 * visualizado na tela controlada pela {@link MesasActivity}.<br><br> Os possíveis filtros podem ser:
 * <ul>
 *     <li>Todas mesas {@link MesasActivity#TODAS_MESAS}</li>
 *     <li>Minhas mesas {@link MesasActivity#MINHAS_MESAS}</li>
 *     <li>Mesas em consumo {@link MesasActivity#MESAS_EM_CONSUMO}</li>
 *     <li>Mesas Disponíveis {@link MesasActivity#MESAS_DISPONIVEIS}</li>
 *     <li>Mesas em fechamento {@link MesasActivity#MESAS_EM_FECHAMENTO}</li>
 * </ul>
 *
 * @author Filipe Bezerra, Diego
 * @version 1.4.0, 29/04/2016
 * @since 1.0.0
 */
public class MenuMesaFragment extends Fragment {
    private OnMenuMesaSelecionado mListener;
    private Unbinder mUnbinder;

    public static MenuMesaFragment newInstance() {
        return new MenuMesaFragment();
    }

    public MenuMesaFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_menu_mesa, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnMenuMesaSelecionado) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnMenuMesaSelecionado");
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnMenuMesaSelecionado) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement OnMenuMesaSelecionado");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick({
            R.id.txtMinhasMesas,
            R.id.txtTodasMesas,
            R.id.txtMesaConsumo,
            R.id.txtMesasDisponivel,
            R.id.txtMesaFechamento})
    public void onMenuTextViewClick(View view) {
        switch (view.getId()) {
            case R.id.txtTodasMesas:
                mListener.onSelected(MesasActivity.TODAS_MESAS);
                break;
            case R.id.txtMinhasMesas:
                mListener.onSelected(MesasActivity.MINHAS_MESAS);
                break;
            case R.id.txtMesaConsumo:
                mListener.onSelected(MesasActivity.MESAS_EM_CONSUMO);
                break;
            case R.id.txtMesasDisponivel:
                mListener.onSelected(MesasActivity.MESAS_DISPONIVEIS);
                break;
            case R.id.txtMesaFechamento:
                mListener.onSelected(MesasActivity.MESAS_EM_FECHAMENTO);
                break;
        }
    }

    public interface OnMenuMesaSelecionado {
        void onSelected(int menuSelecionado);
    }

}

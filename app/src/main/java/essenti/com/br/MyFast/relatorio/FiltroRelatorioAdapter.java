package essenti.com.br.MyFast.relatorio;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.ThemedSpinnerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * @author Filipe Bezerra
 * @version 1.4.0, 04/04/2016
 */
public class FiltroRelatorioAdapter extends ArrayAdapter<FiltroRelatorio> implements ThemedSpinnerAdapter {
    private final Helper mDropDownHelper;

    public FiltroRelatorioAdapter(@NonNull Context context, List<FiltroRelatorio> list) {
        super(context, android.R.layout.simple_list_item_1, list);
        mDropDownHelper = new Helper(context);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view;

        if (convertView == null) {
            LayoutInflater inflater = mDropDownHelper.getDropDownViewInflater();
            view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        } else {
            view = convertView;
        }

        TextView textView = (TextView) view.findViewById(android.R.id.text1);
        textView.setText(getItem(position).toString());

        return view;
    }

    @Override
    public Resources.Theme getDropDownViewTheme() {
        return mDropDownHelper.getDropDownViewTheme();
    }

    @Override
    public void setDropDownViewTheme(Resources.Theme theme) {
        mDropDownHelper.setDropDownViewTheme(theme);
    }
}

package essenti.com.br.MyFast.atendimento;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.text.NumberFormat;
import java.util.List;

import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.android.drawable.DrawableHelper;
import essenti.com.br.MyFast.app.App;
import essenti.com.br.MyFast.produto.Produto;
import essenti.com.br.MyFast.usuario.Usuario;
import essenti.com.br.MyFast.usuario.UsuarioHelper;

/**
 * Esta classe apresenta um diálogo de confirmação dos pedidos selecionados pelo garçom.
 *
 * @author Filipe Bezerra
 * @version 1.4.2
 * @since 1.4.1
 */
public class ConfirmaPedidosDialog {

    private final AlertDialog mDialog;

    private final Context mContext;

    private ConfirmaPedidosCallback mCallback;

    private TextInputLayout mInputLayoutGarcomResponsavel;

    private String mMatriculaGarcom = "";

    /**
     * Construtor padrão para criação de um diálogo.
     *
     * @param context  contexto da tela que conterá este diálogo
     * @param produtos uma lista de produtos selecionadas para confirmação do pedido
     */
    public ConfirmaPedidosDialog(final Context context, List<Produto> produtos) {
        mContext = context;

        final View view = inflateDialogView(context);

        final ListView listViewItens = (ListView) view.findViewById(R.id.listViewItens);
        listViewItens.setAdapter(new ConfirmaPedidosAdapter(context, produtos));

        if (App.getInstancia().isPermitidoAlterarGarcom()) {
            final Button buttonAlterarGarcom = (Button) view.findViewById(R.id.alterarGarcom);
            buttonAlterarGarcom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new MaterialDialog.Builder(mContext)
                            .title(R.string.dialog_title_alterando_garcom)
                            .content(R.string.dialog_content_alterando_garcom)
                            .positiveText(android.R.string.ok)
                            .negativeText(android.R.string.cancel)
                            .inputType(EditorInfo.TYPE_CLASS_NUMBER
                                    | EditorInfo.TYPE_NUMBER_FLAG_SIGNED)
                            .input(null, null, false, new MaterialDialog.InputCallback() {
                                @Override
                                public void onInput(@NonNull MaterialDialog dialog,
                                        CharSequence input) {
                                    if (TextUtils.isEmpty(input)) {
                                        //noinspection ConstantConditions
                                        CharSequence content = dialog.getContentView().getText();
                                        if (!content.equals(mContext.getString(
                                                R.string.dialog_content_alterando_garcom))) {
                                            dialog.setContent(
                                                    R.string.dialog_content_alterando_garcom);
                                        }
                                        return;
                                    }

                                    Integer matricula = Integer.valueOf(input.toString());
                                    if (App.getInstancia().hasUsuario(matricula)) {
                                        dialog.getActionButton(DialogAction.POSITIVE)
                                                .setEnabled(true);
                                        dialog.setContent(R.string.dialog_content_alterando_garcom);
                                    } else {
                                        dialog.getActionButton(DialogAction.POSITIVE)
                                                .setEnabled(false);
                                        dialog.setContent(
                                                R.string.dialog_content_matricula_invalida);
                                    }
                                }
                            })
                            .alwaysCallInputCallback()
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog,
                                        @NonNull DialogAction which) {
                                    //noinspection ConstantConditions
                                    String matricula = dialog.getInputEditText()
                                            .getText()
                                            .toString();
                                    setMatriculaGarcom(App.getInstancia()
                                            .getUsuario(Integer.parseInt(matricula)));
                                }
                            })
                            .show();
                }
            });
            buttonAlterarGarcom.setVisibility(View.VISIBLE);
            mInputLayoutGarcomResponsavel = (TextInputLayout) view.findViewById(R.id.garcomResponsavel);
            mInputLayoutGarcomResponsavel.setVisibility(View.VISIBLE);
        }

        final Button btnConfirmar = (Button) view.findViewById(R.id.btnConfirmar);
        btnConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCallback != null) {
                    mCallback.onPedidoConfirmado(mMatriculaGarcom);
                }

                mDialog.dismiss();
            }
        });

        setMatriculaGarcom(UsuarioHelper.retrieveUsuarioLogado(context));

        mDialog = new AlertDialog.Builder(context)
                .setView(view)
                .create();
    }

    private View inflateDialogView(Context context) {
        ViewGroup rootView = null;
        if (context instanceof Activity) {
            rootView = (ViewGroup) ((ViewGroup)
                    ((Activity) context).findViewById(android.R.id.content)).getChildAt(0);
        }

        return LayoutInflater.from(context)
                .inflate(R.layout.dialog_confirmar_pedidos, rootView, false);
    }

    private void setMatriculaGarcom(Usuario usuario) {
        if (usuario != null) {
            mMatriculaGarcom = String.valueOf(usuario.getMatricula());

            if (mInputLayoutGarcomResponsavel != null) {
                //noinspection ConstantConditions
                mInputLayoutGarcomResponsavel.getEditText().setText(usuario.toString());
            }
        }
    }

    /**
     * Define quem deve ser notificado quando o botão de confirmação é pressionado.
     *
     * @param theCallback o observador da confirmação do pedido.
     */
    public ConfirmaPedidosDialog callback(@NonNull ConfirmaPedidosCallback theCallback) {
        mCallback = theCallback;
        return this;
    }

    /**
     * Mostra o diálogo para o usuário.
     */
    public void show() {
        mDialog.show();
    }

    /**
     * Adaptador dos itens da lista de produtos do diálogo
     */
    private class ConfirmaPedidosAdapter extends ArrayAdapter<Produto> {
        private
        @LayoutRes
        int mLayout;

        public ConfirmaPedidosAdapter(Context context, List<Produto> objects) {
            super(context, R.layout.item_lista_confirma_pedidos, objects);
            mLayout = R.layout.item_lista_confirma_pedidos;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(mLayout, parent, false);

                holder = new ViewHolder();
                holder.txtDescricaoItem = (TextView) convertView.findViewById(R.id.txtDescricaoItem);
                holder.txtTotalItem = (TextView) convertView.findViewById(R.id.txtTotalItem);
                holder.txtValorUnitario = (TextView) convertView.findViewById(R.id.txtValorUnitario);
                holder.txtValorTotalItem = (TextView) convertView.findViewById(R.id.txtValorTotalItem);
                holder.btnRemoverItem = (ImageButton) convertView.findViewById(R.id.btnRemoverItem);
                DrawableHelper
                        .withContext(getContext())
                        .withColor(R.color.white)
                        .withDrawable(holder.btnRemoverItem.getDrawable())
                        .tint()
                        .applyTo(holder.btnRemoverItem);

                holder.btnRemoverItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        remove(getItem(position));
                        notifyDataSetChanged();

                        if (mCallback != null) {
                            mCallback.onItemPedidoCancelado(position);
                        }
                    }
                });

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            Produto produto = getItem(position);
            holder.txtDescricaoItem.setText(produto.getDescricao());
            holder.txtTotalItem.setText(String.format(App.BRAZIL_LOCALE, "%03d", produto.getQuantidade()));

            holder.txtValorUnitario.setText(parent.getContext().getString(
                    R.string.valor_unitario,
                    NumberFormat.getCurrencyInstance().format(produto.getValorUnitario())));

            holder.txtValorTotalItem.setText(NumberFormat.getCurrencyInstance().format(produto.getValorTotal()));

            return convertView;
        }

        private class ViewHolder {
            TextView txtTotalItem;
            TextView txtDescricaoItem;
            TextView txtValorUnitario;
            TextView txtValorTotalItem;
            ImageButton btnRemoverItem;
        }
    }

    /**
     * Callbacks para notificar ações/alterações feitas no tela do pedido
     */
    public interface ConfirmaPedidosCallback {
        void onPedidoConfirmado(String matriculaGarcom);

        void onItemPedidoCancelado(int posicao);
    }
}

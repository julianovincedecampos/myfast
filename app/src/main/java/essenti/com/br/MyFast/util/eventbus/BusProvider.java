package essenti.com.br.MyFast.util.eventbus;

import com.squareup.otto.Bus;

/**
 * @author Filipe Bezerra
 * @version 1.4.0, 04/04/2016
 */
public class BusProvider {
    private static final Bus BUS = new Bus();

    public static Bus getInstance() {
        return BUS;
    }

    private BusProvider() {
        // No instances.
    }
}

package essenti.com.br.MyFast.usuario;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import butterknife.OnTouch;
import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.android.activity.ActivityNavigator;
import essenti.com.br.MyFast.android.activity.BaseActivity;
import essenti.com.br.MyFast.android.asynctask.TaskCallback;
import essenti.com.br.MyFast.android.asynctask.TaskException;
import essenti.com.br.MyFast.android.widget.MensagemDialogo;
import essenti.com.br.MyFast.app.App;
import essenti.com.br.MyFast.barcode.CaptureActivityAnyOrientation;
import essenti.com.br.MyFast.mesa.GetTransferenciaMesa;
import essenti.com.br.MyFast.mesa.Mesa;
import essenti.com.br.MyFast.mesa.MesaService;
import essenti.com.br.MyFast.mesa.ResultadoObterMesa;
import essenti.com.br.MyFast.util.FeedbackHelper;
import essenti.com.br.MyFast.util.NetworkUtil;
import essenti.com.br.MyFast.webservice.ServiceGenerator;
import io.fabric.sdk.android.Fabric;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static essenti.com.br.MyFast.android.widget.MensagemDialogo.MENSAGEM_ALERTA;
import static essenti.com.br.MyFast.android.widget.MensagemDialogo.MENSAGEM_ERRO;
import static essenti.com.br.MyFast.usuario.UsuariosActivity.EXTRA_OPERADOR_SELECIONADO;

/**
 * .
 *
 * @author Filipe Bezerra
 * @version 1.4.0, 11/04/2015
 * @since #
 */
public class AutenticacaoOperadorActivity extends BaseActivity {

    public static final int TIPO_TROCA_MESA = 1;

    public static final int TIPO_TROCA_PRODUTOS_MESA = 2;

    public static final String EXTRA_TIPO_AUTENTICACAO = "TipoAutenticacao";

    private static final int CODIGO_CARDAPIO = App.getInstancia().getCodigoCardapio();

    private Mesa mMesaEmAtendimento;

    private TipoAutenticacao mTipoAutenticacao;

    private String mCodigoIdentificacao;

    private Usuario mUsuarioResponsavel;

    private MensagemDialogo mDialogoProgresso;

    private MesaService mMesaService;

    @BindView(R.id.edtMesaDestino) EditText mEdtMesaDestino;
    @BindView(R.id.edtGarcomDestino) EditText mEdtGarcomDestino;
    @BindView(R.id.edtMatriculaOperadorLogado) EditText mEdtMatriculaOperadorLogado;
    @BindView(R.id.edtSenhaOperadorLogado) EditText mEdtSenhaOperadorLogado;

    private void showError(Throwable e) {
        dismissDialogoProgresso();

        if (Fabric.isInitialized()) {
            Crashlytics.logException(e);
        }

        String mensagem = e.getMessage() +
                "\nPor favor tente novamente. Esta falha será reportada para o " +
                "administrador do sistema!";

        App.getInstancia().exibirMensagem(this, MENSAGEM_ERRO,
                "Atenção", mensagem);
    }

    private void showDialogoProgresso(String mensagem) {
        if (mDialogoProgresso == null) {
            mDialogoProgresso = App.getInstancia().exibirProgresso(this, "Processando", mensagem);
            mDialogoProgresso.exibir();
        } else {
            mDialogoProgresso.setTexto(mensagem);

            if (!mDialogoProgresso.isExibindo()) {
                mDialogoProgresso.exibir();
            }
        }
    }

    private void dismissDialogoProgresso() {
        if (mDialogoProgresso != null) {
            if (mDialogoProgresso.isExibindo()) {
                mDialogoProgresso.esconder();
            }

            mDialogoProgresso = null;
        }
    }

    private void cancelarAutenticacao() {
        setResult(RESULT_CANCELED);
        finish();
    }

    private void confirmarAutenticacao() {
        setResult(RESULT_OK, getIntent());
        finish();
    }

    private void iniciarLeituraCodigoBarras() {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setCaptureActivity(CaptureActivityAnyOrientation.class);
        integrator.setOrientationLocked(false);
        integrator.setBeepEnabled(true);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
        integrator.setPrompt("Posicione o código de barras corretamente.");
        integrator.initiateScan();
    }

    private void requestAutenticacaoTranferenciaMesa(final String matriculaOperadorLogado,
            final int numeroMesaDestino) {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            new GetTransferenciaMesa(matriculaOperadorLogado, mMesaEmAtendimento.getNumero(),
                    numeroMesaDestino, mUsuarioResponsavel.getMatricula(),
                    new TaskCallback<String>() {
                        @Override
                        public void onBegin() {
                            showDialogoProgresso("Validando seu acesso...");
                        }

                        @Override
                        public void onSuccess(String result) {
                            dismissDialogoProgresso();
                            confirmarAutenticacao();
                        }

                        @Override
                        public void onResultNothing() {
                            dismissDialogoProgresso();
                        }

                        @Override
                        public void onError(TaskException e) {
                            showError(e);
                        }
                    }
            ).execute();
        } else {
            FeedbackHelper.showOfflineMessage(this,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            finish();
                        }
                    },
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            requestAutenticacaoTranferenciaMesa(
                                    matriculaOperadorLogado, numeroMesaDestino);
                        }
                    });
        }
    }

    private void requestStatusMesa(final String matriculaOperadorLogado,
            final int numeroMesaDestino) {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            if (mMesaService == null) {
                mMesaService = ServiceGenerator.createService(MesaService.class, this);
            }

            if (mMesaService != null) {
                mMesaService.obterMesa(mMesaEmAtendimento.getNumero(), CODIGO_CARDAPIO)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(
                                new Subscriber<ResultadoObterMesa>() {
                                    @Override
                                    public void onStart() {
                                        showDialogoProgresso("Carregando informações da mesa...");
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        showError(e);
                                    }

                                    @Override
                                    public void onNext(ResultadoObterMesa resultado) {
                                        dismissDialogoProgresso();
                                        if (resultado.mesa.getStatusMesa()
                                                == Mesa.MESA_EM_FECHAMENTO) {
                                            App.getInstancia()
                                                    .exibirMensagem(
                                                            AutenticacaoOperadorActivity.this,
                                                            MENSAGEM_ALERTA, "Atenção",
                                                            "Não é possível continuar, esta mesa está em fechamento!");
                                            return;
                                        }

                                        requestAutenticacaoTranferenciaMesa(matriculaOperadorLogado,
                                                numeroMesaDestino);
                                    }

                                    @Override
                                    public void onCompleted() {
                                        dismissDialogoProgresso();
                                    }
                                }
                        );
            }
        } else {
            FeedbackHelper.showOfflineMessage(this,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            finish();
                        }
                    },
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            requestStatusMesa(
                                    matriculaOperadorLogado, numeroMesaDestino);
                        }
                    });
        }
    }

    private void requestAutenticacaoTransferenciaItensMesa(final String matriculaOperadorLogado,
            final String senhaOperadorLogado) {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
        new GetValidacaoControleAcesso(matriculaOperadorLogado, senhaOperadorLogado,
                mCodigoIdentificacao, mTipoAutenticacao, new TaskCallback<String>() {
            @Override
            public void onBegin() {
                showDialogoProgresso("Validando as informações...");
            }

            @Override
            public void onSuccess(String result) {
                dismissDialogoProgresso();
                confirmarAutenticacao();
            }

            @Override
            public void onResultNothing() {
                dismissDialogoProgresso();
            }

            @Override
            public void onError(TaskException e) {
                showError(e);
            }
        }).execute();
        } else {
            FeedbackHelper.showOfflineMessage(this,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            finish();
                        }
                    },
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            requestAutenticacaoTransferenciaItensMesa(
                                    matriculaOperadorLogado, senhaOperadorLogado);
                        }
                    });
        }
    }

    @Override
    protected int provideLayoutResource() {
        return R.layout.activity_autenticao_operador;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Toast.makeText(this,
                    "Nenhuma câmera detectada. É preciso que o dispositivo tenha câmera.",
                    Toast.LENGTH_SHORT).show();
            cancelarAutenticacao();
            return;
        }

        if (!getIntent().hasExtra(EXTRA_TIPO_AUTENTICACAO)) {
            Toast.makeText(this, "Tipo de autenticação não informado. " +
                            "É preciso que o tipo de autenticação seja passado por extras.",
                    Toast.LENGTH_SHORT)
                    .show();
            cancelarAutenticacao();
            return;
        }

        if (!getIntent().hasExtra(App.EXTRA_MESA_ATENDIMENTO)) {
            Toast.makeText(this, "Mesa não informada. É preciso que a mesa seja passada via " +
                    "Intent.putExtra(App.EXTRA_MESA_ATENDIMENTO, Parcelable)", Toast.LENGTH_SHORT)
                    .show();
            cancelarAutenticacao();
            return;
        }

        mMesaEmAtendimento = getIntent().getParcelableExtra(App.EXTRA_MESA_ATENDIMENTO);
        int tipoAutenticacaoInt = getIntent().getIntExtra(EXTRA_TIPO_AUTENTICACAO, TIPO_TROCA_MESA);

        if (tipoAutenticacaoInt == TIPO_TROCA_MESA) {
            mEdtMesaDestino.setVisibility(View.VISIBLE);
            mEdtGarcomDestino.setVisibility(View.VISIBLE);
            mTipoAutenticacao = TipoAutenticacao.TROCA_MESA;
        } else {
            mEdtMatriculaOperadorLogado.requestFocus();
            mTipoAutenticacao = TipoAutenticacao.TROCA_PRODUTOS_MESA;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case IntentIntegrator.REQUEST_CODE:
                final IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode,
                        resultCode, data);

                if (intentResult != null && intentResult.getContents() != null) {
                    mCodigoIdentificacao = intentResult.getContents();
                    onBtnAutenticarClick();
                } else {
                    mEdtSenhaOperadorLogado.requestFocus();
                }
                break;

            case App.SELECIONAR_OPERADOR:
                if (resultCode == App.OPERADOR_SELECIONADO) {
                    Bundle extras = data.getExtras();

                    if ((extras != null) && extras.containsKey(EXTRA_OPERADOR_SELECIONADO)) {
                        mUsuarioResponsavel = extras.getParcelable(EXTRA_OPERADOR_SELECIONADO);
                        if (mUsuarioResponsavel != null) {
                            mEdtGarcomDestino.setText(mUsuarioResponsavel.getNome());
                            mEdtMatriculaOperadorLogado.requestFocus();
                        }
                    }
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnEditorAction(R.id.edtMesaDestino)
    boolean onEdtMesaDestinoEditorAction(int actionId) {
        if (actionId == EditorInfo.IME_ACTION_NEXT) {
            onEdtGarcomDestinoTouch(MotionEvent.obtain(SystemClock.currentThreadTimeMillis(),
                    SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, mEdtMesaDestino.getX(),
                    mEdtMesaDestino.getY(), 0));
        }
        return false;
    }

    @OnEditorAction(R.id.edtSenhaOperadorLogado)
    boolean onEdtSenhaOperadorLogadoEditorAction(int actionId) {
        if (actionId == getResources().getInteger(R.integer.autenticar)) {
            iniciarLeituraCodigoBarras();
            return true;
        }
        return false;
    }

    @OnTouch(R.id.edtGarcomDestino)
    boolean onEdtGarcomDestinoTouch(final MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            ActivityNavigator.startUsuariosActivity(this);
            return true;
        }

        return false;
    }

    @OnClick(R.id.btnAutenticar)
    void onBtnAutenticarClick() {
        if (TextUtils.isEmpty(mCodigoIdentificacao)) {
            App.getInstancia()
                    .exibirMensagem(this, MENSAGEM_ALERTA, "Atenção",
                            "Faça a leitura do código de identificação",
                            new MensagemDialogo.OnMensagemClickListener() {
                                @Override
                                public void onConfirmarClick() {
                                    iniciarLeituraCodigoBarras();
                                }

                                @Override
                                public void onCancelarClick() {
                                }
                            }
                    );
            return;
        }

        final String matriculaOperadorLogado = mEdtMatriculaOperadorLogado.getText().toString();
        final String senhaOperadorLogado = mEdtSenhaOperadorLogado.getText().toString();

        if (TextUtils.isEmpty(matriculaOperadorLogado)) {
            App.getInstancia().exibirMensagem(this, MENSAGEM_ALERTA, "Atenção",
                    "Informe sua matrícula");
            mEdtMatriculaOperadorLogado.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(senhaOperadorLogado)) {
            App.getInstancia().exibirMensagem(this, MENSAGEM_ALERTA, "Atenção",
                    "Informe sua senha");
            mEdtSenhaOperadorLogado.requestFocus();
            return;
        }

        if (mTipoAutenticacao.equals(TipoAutenticacao.TROCA_MESA)) {
            final String numeroMesaDestino = mEdtMesaDestino.getText().toString();

            if (TextUtils.isEmpty(numeroMesaDestino)) {
                App.getInstancia().exibirMensagem(AutenticacaoOperadorActivity.this,
                        MENSAGEM_ALERTA, "Atenção", "Informe a mesa de destino");
                mEdtMesaDestino.requestFocus();
                return;
            }

            if (TextUtils.isEmpty(mEdtGarcomDestino.getText())) {
                App.getInstancia().exibirMensagem(AutenticacaoOperadorActivity.this,
                        MENSAGEM_ALERTA, "Atenção", "Informe o garçom responsável");
                mEdtGarcomDestino.requestFocusFromTouch();
                return;
            }

            requestStatusMesa(matriculaOperadorLogado, Integer.parseInt(numeroMesaDestino));
        } else {
            requestAutenticacaoTransferenciaItensMesa(matriculaOperadorLogado, senhaOperadorLogado);
        }
    }
}

package essenti.com.br.MyFast.util.number;

import android.text.TextUtils;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import timber.log.Timber;

/**
 * Classe utilitária com suporte à métodos auxiliares para formatação de números.
 *
 * @author Filipe Bezerra
 * @version #, 13/01/2016
 * @since #
 */
public class NumberUtil {
    private static final NumberFormat CURRENCY_FORMATTER = NumberFormat.getCurrencyInstance();

    private static final NumberFormat NUMBER_FORMAT = NumberFormat.getNumberInstance();

    static {
        NUMBER_FORMAT.setMinimumFractionDigits(2);
        NUMBER_FORMAT.setMaximumFractionDigits(2);
    }

    private NumberUtil() {}

    /**
     * Formata o {@code value} usando o formato de moeda sendo usado no sistema operacional
     * (Configurações > Idioma e Entrada).
     *
     * @param value o valor à ser formatado.
     * @return a formatação do {@code value} no formato de moeda local.
     */
    // TODO (Filipe Bezerra)>: Procurar correspondências para uso de "NumberFormat" no código e substituir por este método
    public static String formatAsCurrency(double value) {
        return CURRENCY_FORMATTER.format(value);
    }

    public static String formatAsNumber(double value) {
        return NUMBER_FORMAT.format(value);
    }

    public static String formatWithDot(double value) {
        return String.format(Locale.ENGLISH, "%.2f", value);
    }

    public static double formatAsNumber(String value) {
        if (TextUtils.isEmpty(value)) {
            return 0;
        } else if (!value.contains(",")) {
            value = new StringBuilder(value).reverse().insert(2, ",").reverse().toString();
        }

        try {
            return NUMBER_FORMAT.parse(value).doubleValue();
        } catch (ParseException e) {
            Timber.e(e, "Falha ao converter número em texto para valor primitivo double");
            return 0;
        }
    }
}

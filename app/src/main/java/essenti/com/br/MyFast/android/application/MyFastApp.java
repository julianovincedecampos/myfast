package essenti.com.br.MyFast.android.application;

import android.app.Application;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import essenti.com.br.MyFast.BuildConfig;
import essenti.com.br.MyFast.a_migration.data.repositorio.RealmMigrationImpl;
import essenti.com.br.MyFast.util.CrashReportingTree;
import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import timber.log.Timber;

/**
 * Classe da aplicação que mantém o estado global no contexto do framework.
 *
 * @author Filipe Bezerra
 */
public class MyFastApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.USE_CRASHLYTICS) {
            Crashlytics crashlyticsKit = new Crashlytics.Builder()
                    .core(new CrashlyticsCore.Builder().build())
                    .build();

            Fabric.with(this, crashlyticsKit);
            Timber.plant(new CrashReportingTree());
        } else {
            Timber.plant(new Timber.DebugTree());
        }

        RealmConfiguration configuration = new RealmConfiguration.Builder(this)
                .name("myfast.realm")
                .schemaVersion(1)
                .migration(new RealmMigrationImpl())
                .build();
        Realm.setDefaultConfiguration(configuration);
    }
}

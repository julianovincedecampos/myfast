package essenti.com.br.MyFast.a_migration.domain.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author Filipe Bezerra
 */
public class Cobranca implements Parcelable {
    private final String cartao;

    private final String codigo;

    private final String descricao;

    public static final Creator<Cobranca> CREATOR = new Creator<Cobranca>() {
        @Override
        public Cobranca createFromParcel(Parcel in) {
            return new Cobranca(in);
        }

        @Override
        public Cobranca[] newArray(int size) {
            return new Cobranca[size];
        }
    };

    protected Cobranca(Parcel in) {
        cartao = in.readString();
        codigo = in.readString();
        descricao = in.readString();
    }

    public Cobranca(String pCartao, String pCodigo, String pDescricao) {
        cartao = pCartao;
        codigo = pCodigo;
        descricao = pDescricao;
    }

    public String getCartao() {
        return cartao;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(cartao);
        out.writeString(codigo);
        out.writeString(descricao);
    }
}

package essenti.com.br.MyFast.a_migration.domain.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Filipe Bezerra
 */

public class CobrancaDto {
    @SerializedName("Cartao") @Expose public String cartao;

    @SerializedName("Codcob") @Expose public String codigo;

    @SerializedName("DescCobranca") @Expose public String descricao;
}

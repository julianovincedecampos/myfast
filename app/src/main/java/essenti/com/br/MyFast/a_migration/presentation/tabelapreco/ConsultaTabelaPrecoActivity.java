package essenti.com.br.MyFast.a_migration.presentation.tabelapreco;

import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.MenuItem;
import com.squareup.otto.Subscribe;
import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.android.activity.BaseActivity;
import essenti.com.br.MyFast.util.eventbus.BusProvider;

/**
 * @author Filipe Bezerra
 */
public class ConsultaTabelaPrecoActivity extends BaseActivity {

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        putFragment(PesquisaProdutosFragment.newInstance(),
                R.string.title_fragment_pesquisa_produtos);
    }

    @Override protected int provideLayoutResource() {
        return R.layout.activity_consulta_tabela_preco;
    }

    @Override protected boolean shouldDisplayHomeAsUpEnabled() {
        return true;
    }

    @Override protected int provideUpIndicator() {
        return R.drawable.ic_arrow_back_white_24dp;
    }

    @Override protected void onStart() {
        super.onStart();
        BusProvider.getInstance().register(this);
    }

    @Override protected void onStop() {
        super.onStop();
        BusProvider.getInstance().unregister(this);
    }

    @Subscribe public void onProdutosSelecionadosEvent(ProdutosSelecionadosEvent pEvent) {
        putFragment(ConfirmaPedidoFragment.newInstance(pEvent.getProdutos()),
                R.string.title_fragment_confirma_inclusao_produtos);
    }

    @Subscribe public void onPedidoEnviadoEvent(PedidoEnviadoEvent pEvent) {
        getSupportFragmentManager()
                .popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        putFragment(PesquisaProdutosFragment.newInstance(),
                R.string.title_fragment_pesquisa_produtos);
    }

    private void putFragment(Fragment pFragment, @StringRes int pTitleRes) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, pFragment)
                .addToBackStack(null)
                .commit();
        setTitle(pTitleRes);
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }
}

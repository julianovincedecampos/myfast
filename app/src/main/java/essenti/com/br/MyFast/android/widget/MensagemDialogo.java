package essenti.com.br.MyFast.android.widget;

import android.content.Context;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by diego_000 on 01/11/2015.
 */
public class MensagemDialogo {
    private Context contexto;
    private boolean aceitarBotaoVoltar;
    private OnMensagemClickListener listener;
    private SweetAlertDialog alerta;
    public static final int MENSAGEM_NORMAL = 0;
    public static final int MENSAGEM_ERRO = 1;
    public static final int MENSAGEM_SUCESSO = 2;
    public static final int MENSAGEM_ALERTA = 3;
    public static final int MENSAGEM_CUSTOMIZADA_TIPO = 4;
    public static final int MENSAGEM_PROGRESSO = 5;
    public MensagemDialogo(Context contexto, int tipo) {
        this.contexto = contexto;
        alerta = new SweetAlertDialog(this.contexto, tipo);
        alerta.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                if (listener != null) {
                    alerta.dismiss();
                    listener.onConfirmarClick();
                }
                else
                {
                    alerta.dismiss();
                }
            }
        });
        alerta.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                if (listener != null) {
                    alerta.dismiss();
                    listener.onCancelarClick();
                }
                else
                {
                    alerta.dismiss();
                }
            }
        });
    }


    public int getTipo() {
        return alerta.getAlerType();
    }

    public void setTipo(int tipo) {
        alerta.changeAlertType(tipo);
    }

    public String getTitulo() {
        return alerta.getTitleText();
    }

    public void setTitulo(String titulo) {
        alerta.setTitleText(titulo);
    }

    public String getTexto() {
        return alerta.getContentText();
    }

    public void setTexto(String texto) {
        alerta.setContentText(texto);
    }

    public boolean isExibirBotaoCancelar() {
        return alerta.isShowCancelButton();
    }

    public void setExibirBotaoCancelar(boolean exibirBotaoCancelar) {
        alerta.showCancelButton(exibirBotaoCancelar);
    }

    public boolean isAceitarBotaoVoltar() {
        return aceitarBotaoVoltar;
    }

    public void setAceitarBotaoVoltar(boolean aceitarBotaoVoltar) {
        this.aceitarBotaoVoltar = aceitarBotaoVoltar;
        alerta.setCancelable(aceitarBotaoVoltar);
    }

    public String getConfirmarTexto() {
        return alerta.getConfirmText();
    }

    public void setConfirmarTexto(String texto) {
        alerta.setConfirmText(texto);
    }

    public String getCancelarrTexto() {
        return alerta.getCancelText();
    }

    public void setCancelarTexto(String texto) {
        alerta.setCancelText(texto);
    }

    public OnMensagemClickListener getListener() {
        return listener;
    }

    public void setListener(OnMensagemClickListener listener) {
        this.listener = listener;
    }

    public void exibir() {
        alerta.show();
    }

    public void esconder() {
        alerta.dismiss();
    }

    public boolean isExibindo() {
        return alerta.isShowing();
    }

    public interface OnMensagemClickListener {
        public void onConfirmarClick();

        public void onCancelarClick();
    }
}

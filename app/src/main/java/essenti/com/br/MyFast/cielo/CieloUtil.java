package essenti.com.br.MyFast.cielo;

import android.content.pm.PackageManager;

import org.json.JSONException;
import org.json.JSONObject;

import essenti.com.br.MyFast.pagamento.PagamentoCielo;
import essenti.com.br.MyFast.util.number.NumberUtil;
import timber.log.Timber;

public class CieloUtil {
    public static final String URI_STRING =
            "cielomobile://pagar?urlCallback=myfast://retornopagamento";

    public static final String PACKAGE_NAME = "com.m4u.cielomobile";

    public static final String EXTRA_MESSAGE = "MENSAGEM";

    public static final String DATE_PATTERN = "yyMMddHHmmss";

    private static final String TRANSACAO_APROVADA = "000";
    private static final String RETORNO_PROCESSAMENTO_COM_SUCESSO = "R00";

    public static String obtainTransactionFromJson(int transactionId, double value) {
        try {
            return new JSONObject()
                    .put("idTransacao", transactionId)
                    .put("valor", formatCurrenyToString(value))
                    .put("tipoProdutoAdministrativo", false)
                    .put("nomeAplicacao", "MyFast")
                    .put("inibirBotaoEnviarComprovanteVenda", true)
                    .put("referencia", "Venda")
                    .put("bloquearDigitada", false)
                    .put("gerarToken", false)
                    .put("email ", "")
                    .put("bloquearDigitada ", false)
                    .put("backHistoryMode", "CLEAR_TOP")
                    .toString();
        } catch (JSONException e) {
            Timber.e("Obtendo transação com Cielo Mobile como texto", e);
            return null;
        }
    }

    public static String formatCurrenyToString(double value) {
        return String.valueOf(NumberUtil.formatAsNumber(value))
                .replaceAll("\\.", "")
                .replaceAll(",", "");
    }

    public static boolean isPaymentApproved(final PagamentoCielo pagamentoCielo) {
        return TRANSACAO_APROVADA.equals(pagamentoCielo.codResposta) &&
                RETORNO_PROCESSAMENTO_COM_SUCESSO.equals(pagamentoCielo.retornoAplicacao);
    }

    public static String obtainApplicationReturnMessage(final PagamentoCielo pagamentoCielo) {
        switch (pagamentoCielo.retornoAplicacao) {
            case RETORNO_PROCESSAMENTO_COM_SUCESSO:
                return "Processamento com sucesso!";

            case "R01":
                return "Erro interno!";

            case "R02":
                return "Chamada externa não habilitada!";

            case "R03":
                return "Cielo Mobile Bloqueado!";

            case "R04":
                return "Valor Zerado!";

            case "R05":
                return "Transação não encontrada!";

            case "R06":
                return "Transação existente!";

            case "R07":
                return "Terminal inoperante!";

            case "R08":
                return "Nenhuma rede disponível!";

            case "R09":
                return "Parâmetros Inválidos!";

            case "R10":
                return "Limite máximo de transações atingido para exportar o " +
                        "histórico. Deverá ser paginado.!";

            case "R11":
                return "Página inválida.!";

            case "R15":
                return "A Cielo retornou uma mensagem inválida (MI)!";

            case "R16":
                return "Ocorreu timeout ao se comunicar com a Cielo!";

            case "R17":
                return "Ocorreu timeout ao se comunicar com o Eldorado!";

            case "R18":
                return "Ocorreu um erro interno ao se comunicar com o Eldorado!";

            case "R19":
                return "Ocorreu um timeout ao se comunicar com o pinpad!";

            case "R20":
                return "O usuário cancelou a operação pelo pinpad!";

            case "R21":
                return  "Ocorreu um erro não esperado ao se comunicar com o pinpad!";

            case "R22":
                return "O usuário cancelou a operação pela aplicação!";

            case "R23":
                return "Ocorreu um erro ao ler a tarja do cartão!";

            case "R24":
                return "Operação não permitida!";

            case "R25":
                return "Transação duplicada!";

            case "R26":
                return "O cartão do cancelamento foi diferente do usado na última " +
                        "transação!";

            case "R27":
                return "Ocorreu um erro ao ler o cartão!";

            case "R28":
                return  "O terminal não está habilitado para transacionar com chip!";

            case "R29":
                return "Cartão contém chip!";

            case "R30":
                return "Modo Inválido. Fallback não permitido!";

            case "R31":
                return "Erro inicialização!";

            case "R32":
                return "Erro teste de comunicação!";

            case "R33":
                return "Erro enviar pendentes!";

            case "R34":
                return "Venda digitada não permitida!";

            case "R35":
                return "Produto Matriz Inválido!";

            case "R36":
                return "Produto Secundário Inválido!";

            case "R37":
                return "EC inválido!";

            case "R38":
                return "Produto Primário não encontrado!";

            case "R39":
                return "Combinação de Parâmetros inválidos!";

            case "A00":
                return "Autorização Sucesso!";

            case "D00":
                return "Desfazimento Sucesso!";

            default: return "";
        }
    }

    public static boolean isCieloMobileInstalled(PackageManager packageManager) {
        try {
            packageManager.getPackageInfo(PACKAGE_NAME, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}

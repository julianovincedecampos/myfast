package essenti.com.br.MyFast.produto;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.utils.RecyclerViewAdapterUtils;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.app.App;

/**
 * Adaptador de {@link Produto}s para {@link RecyclerView} da tela {@link ProdutosAvulsosActivity}.<br><br>
 *
 * Este adaptador é especializado para trabalhar com {@link Produto}s avulsos e usa a {@link Produto#sequencia}
 * como identificador único do item na {@link RecyclerView}.
 *
 * @author Filipe Bezerra
 * @version 1.4.0, 29/04/2016
 * @since 14/12/2015
 */
public class ProdutosAvulsosAdapter
        extends RecyclerView.Adapter<ProdutosAvulsosAdapter.ProdutosAvulsosViewHolder>
        implements Filterable, View.OnClickListener {

    @NonNull private List<Produto> mProdutos;

    //TODO (filipe bezerra) não preservar estado dos produtos selecionados
    private List<Produto> mProdutosSelecionados = new ArrayList<>();

    /**
     * Filtro ativado pela pesquisa do usuário. Este filtro está desativado por padrão,
     * e sua referência deve ser removida sempre que o filtro for removido.
     */
    private List<Produto> mOriginalValues;

    private ProdutosAvulsosFilter mFilter;

    /**
     * Inicia o adaptador com a lista de produtos vazia.
     */
    public ProdutosAvulsosAdapter() {
        mProdutos = new ArrayList<>();
    }

    /**
     * Inicia o adaptador com uma lista de produtos fornecida. A lista fornecida via argumento
     * não deve ser nula.
     *
     * @param produtos a lista de produtos inicializada.
     */
    public ProdutosAvulsosAdapter(@NonNull List<Produto> produtos) {
        mProdutos = produtos;
    }

    @Override
    public ProdutosAvulsosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_lista_produto_avulso, parent, false);
        return new ProdutosAvulsosViewHolder(itemView, this);
    }

    @Override
    public void onBindViewHolder(ProdutosAvulsosViewHolder holder, int position) {
        final Produto produto = mProdutos.get(position);

        holder.txtTotalItem
                .setText(String.format(App.BRAZIL_LOCALE, "%03d", produto.getQuantidade()));
        holder.txtDescricaoItem.setText(produto.getDescricao());
        holder.txtValorUnitario.setText(String.format("Valor Unit. %s",
                NumberFormat.getCurrencyInstance().format(produto.getValorUnitario())));
        holder.txtValorTotalItem.setText(
                NumberFormat.getCurrencyInstance().format(produto.getValorTotal()));
    }

    @Override
    public int getItemCount() {
        return mProdutos.size();
    }

    @Override
    public long getItemId(int position) {
        return mProdutos.get(position).getSequencia();
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new ProdutosAvulsosFilter();
        }
        return mFilter;
    }

    public void addAll(@NonNull List<Produto> produtos) {
        if (mOriginalValues != null) {
            mOriginalValues.clear();
            mOriginalValues.addAll(produtos);
        } else {
            mProdutos.clear();
            mProdutos.addAll(produtos);
        }

        notifyDataSetChanged();
    }

    /**
     * Obtém a lista padrão de produtos.
     *
     * @return a lista padrão dos produtos.
     */
    @NonNull
    public List<Produto> getProdutos() {
        return mProdutos;
    }

    /**
     * Esvazia a listagem de produtos padrão, dos produtos selecionados (quantidade alterada),
     * desativa o filtro dos produtos e notifica os observadores da alteração
     * realizada.
     */
    public void clear() {
        if (mOriginalValues != null) {
            mOriginalValues.clear();
        } else {
            mProdutos.clear();
        }

        notifyDataSetChanged();
    }

    /**
     * Verifica se existem produtos na lista padrão ou na lista filtrada se estiver ativa.
     *
     * @return se existem produtos na lista padrão ou filtrada.
     */
    public boolean isEmpty() {
        return mProdutos.isEmpty();
    }

    /**
     * Verifica se existem produtos na lista de selecionados (quantidade alterada).
     *
     * @return se existem produtos selecionados.
     */
    public boolean isProdutosSelecionadosEmpty() {
        return mProdutosSelecionados.isEmpty();
    }

    @Override
    public void onClick(View v) {
        RecyclerView.ViewHolder vh = RecyclerViewAdapterUtils.getViewHolder(v);
        final int position = vh.getAdapterPosition();

        switch (v.getId()) {
            case R.id.btnAdicionarItem:
                addOrRemoveProduto(true, position);
                break;

            case R.id.btnRemoverItem:
                addOrRemoveProduto(false, position);
                break;
        }
    }

    /**
     * Adiciona ou remove da lista de produtos selecionados se necessário. A quantidade é
     * incrementada ou decrementada de acordo com a operação determinada e uma notificação
     * da alteração realizada é enviada para os observadores.
     *
     * @param toAdd se a operação é para incrementar (adicionar se necessário) ou
     *              decrementar (remover se necessário)
     * @param position posição do produto na lista padrão.
     */
    private void addOrRemoveProduto(boolean toAdd, int position) {
        final Produto produto = mProdutos.get(position);

        if (toAdd || produto.getQuantidade() > 0) {
            if (toAdd) {
                produto.setQuantidade(produto.getQuantidade() + 1);

                if (!mProdutosSelecionados.contains(produto)) {
                    mProdutosSelecionados.add(produto);
                }
            } else {
                produto.setQuantidade(produto.getQuantidade() - 1);

                if (produto.getQuantidade() == 0) {
                    mProdutosSelecionados.remove(produto);
                }
            }

            notifyItemChanged(position);
        }
    }

    /**
     * Obtém a lista de produtos selecionados, ou seja, que tiveram sua quantidade incrementada.
     *
     * @return a lista de produtos selecionados.
     */
    public List<Produto> getProdutosSelecionados() {
        return new ArrayList<>(mProdutosSelecionados);
    }

    public void removerProdutoSelecionado(int posicao) {
        if (posicao >= 0 && posicao < mProdutosSelecionados.size()) {
            final Produto produtoSelecionado = mProdutosSelecionados.get(posicao);
            mProdutosSelecionados.remove(produtoSelecionado);

            int indice = mProdutos.indexOf(produtoSelecionado);

            if (indice != -1) {
                final Produto produto = mProdutos.get(indice);
                produto.setQuantidade(0);
                notifyItemChanged(indice);
            }
        }
    }

    /**
     * Classe interna que descreve a visualização do produto na lista.
     */
    class ProdutosAvulsosViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.btnAdicionarItem) ImageButton btnAdicionarItem;
        @BindView(R.id.btnRemoverItem) ImageButton btnRemoverItem;
        @BindView(R.id.txtTotalItem) TextView txtTotalItem;
        @BindView(R.id.txtDescricaoItem) TextView txtDescricaoItem;
        @BindView(R.id.txtValorUnitario) TextView txtValorUnitario;
        @BindView(R.id.txtValorTotalItem) TextView txtValorTotalItem;

        /**
         * Construtor customizado para receber observador do clique nos botões de adicionar
         * produto ou remover da lista.
         *
         * @param itemView visualização do item (layout)
         * @param clickListener observador de evento de clique nos botões deste item
         */
        public ProdutosAvulsosViewHolder(View itemView, View.OnClickListener clickListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            btnAdicionarItem.setOnClickListener(clickListener);
            btnRemoverItem.setOnClickListener(clickListener);
        }
    }

    /**
     * Classe de uso somente interno para operações de filtro sob a lista de produtos padrão
     * ou sob a lista ja filtrada.
     */
    class ProdutosAvulsosFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            final FilterResults results = new FilterResults();

            if (mOriginalValues == null) {
                mOriginalValues = new ArrayList<>(mProdutos);
            }

            if (TextUtils.isEmpty(prefix)) {
                List<Produto> list = new ArrayList<>(mOriginalValues);
                results.values = list;
                results.count = list.size();
            } else {
                final String prefixString = prefix.toString().toLowerCase();

                List<Produto> values = new ArrayList<>(mOriginalValues);

                final List<Produto> newValues = new ArrayList<>();

                for(Produto produto : values) {
                    if (produto.getDescricao().toLowerCase().contains(prefixString)) {
                        newValues.add(produto);
                    }
                }

                results.values = newValues;
                results.count = newValues.size();
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            //noinspection unchecked
            mProdutos = (List<Produto>) results.values;
            notifyDataSetChanged();
        }
    }
}

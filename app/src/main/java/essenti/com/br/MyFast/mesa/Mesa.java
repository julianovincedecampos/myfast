package essenti.com.br.MyFast.mesa;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import com.hannesdorfmann.parcelableplease.annotation.ParcelablePlease;

import java.math.BigDecimal;

@ParcelablePlease
public class Mesa implements Parcelable {
    public static final int MESA_DISPONIVEL = 100;
    public static final int MESA_EM_CONSUMO = 101;
    public static final int MESA_EM_FECHAMENTO = 102;

    @SerializedName("Numero")
    Integer numero;

    @SerializedName("Codcli")
    Long codigoCliente;

    @SerializedName("Nomecliente")
    String nomeCliente;

    @SerializedName("Ultimogarcom")
    String nomeUltimoGarcom;

    @SerializedName("MatriculaGarcom")
    String matriculaGarcom;

    @SerializedName("VlComissao")
    Double valorComissao;

    @SerializedName("Vlpago")
    Double valorPago;

    @SerializedName("Vltotal")
    Double valorTotal;

    @SerializedName("Status")
    String status;

    @SerializedName("TempoPermanencia")
    Double tempoPermanencia;

    public Mesa() {}

    public Mesa(Integer numero, Long codigoCliente, String nomeCliente,
            String nomeUltimoGarcom, String matriculaGarcom, Double valorComissao,
            Double valorPago, Double valorTotal, String status, Double tempoPermanencia) {
        this.numero = numero;
        this.codigoCliente = codigoCliente;
        this.nomeCliente = nomeCliente;
        this.nomeUltimoGarcom = nomeUltimoGarcom;
        this.matriculaGarcom = matriculaGarcom;
        this.valorComissao = valorComissao;
        this.valorPago = valorPago;
        this.valorTotal = valorTotal;
        this.status = status;
        this.tempoPermanencia = tempoPermanencia;
    }

    public Integer getNumero() {
        return numero;
    }

    public Long getCodigoCliente() {
        return codigoCliente;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public String getNomeUltimoGarcom() {
        return nomeUltimoGarcom;
    }

    public String getMatriculaGarcom() {
        return matriculaGarcom;
    }

    public Double getValorComissao() {
        return valorComissao;
    }

    public Double getValorPago() {
        return valorPago;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public String getStatus() {
        return status;
    }

    public Double getTempoPermanencia() {
        return tempoPermanencia;
    }

    public void setCliente(Cliente cliente) {
        codigoCliente = cliente.getCodigo();
        nomeCliente = cliente.getNome();
    }

    public int getStatusMesa() {
        switch (getStatus() ) {
            case "Em Consumo":
                return MESA_EM_CONSUMO;

            case "Disponível":
                return MESA_DISPONIVEL;

            case "Em Fechamento":
                return MESA_EM_FECHAMENTO;

            default: {
                throw new IllegalStateException(
                        String.format("O status da mesa %s não é válido.", getStatus()));
            }
        }
    }

    public double getSaldo() {
        BigDecimal saldo = BigDecimal.valueOf(getValorTotal())
                .subtract(BigDecimal.valueOf(getValorPago()));
        return Math.max(0.0, saldo.doubleValue());
    }

    public boolean isMesaDisponivelOuEmConsumo() {
        return getStatusMesa() == MESA_DISPONIVEL || getStatusMesa() == MESA_EM_CONSUMO;
    }

    public boolean isMesaEmFechamento() {
        return getStatusMesa() == MESA_EM_FECHAMENTO;
    }

    public boolean hasConsumo() {
        return getStatusMesa() == MESA_EM_CONSUMO || getStatusMesa() == MESA_EM_FECHAMENTO;
    }

    @Override
    public String toString() {
        return getNumero() + " " + getMatriculaGarcom();
    }

    @Override
    public boolean equals(Object o) {
        if (o != null && o instanceof Mesa) {
            final Mesa anotherMesa = (Mesa)o;
            return getNumero().compareTo(anotherMesa.getNumero()) == 0;
        }
        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        MesaParcelablePlease.writeToParcel(this, dest, flags);
    }

    public static final Creator<Mesa> CREATOR = new Creator<Mesa>() {
        public Mesa createFromParcel(Parcel source) {
            Mesa target = new Mesa();
            MesaParcelablePlease.readFromParcel(target, source);
            return target;
        }

        public Mesa[] newArray(int size) {
            return new Mesa[size];
        }
    };
}

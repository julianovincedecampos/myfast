package essenti.com.br.MyFast.a_migration.data.cobrancas;

import essenti.com.br.MyFast.a_migration.domain.dto.RespostaListaCobrancas;
import retrofit2.http.GET;
import rx.Observable;

/**
 * @author Filipe Bezerra
 */

public interface CobrancaService {
    @GET("/ServiceMesa.svc/ListarCobranca")
    Observable<RespostaListaCobrancas> listarCobrancas();
}

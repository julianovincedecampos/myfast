package essenti.com.br.MyFast.outrospagamentos;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTouch;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.crashlytics.android.Crashlytics;
import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.a_migration.domain.pojo.Cobranca;
import essenti.com.br.MyFast.android.activity.BaseActivity;
import essenti.com.br.MyFast.android.widget.MensagemDialogo;
import essenti.com.br.MyFast.app.App;
import essenti.com.br.MyFast.mesa.Mesa;
import essenti.com.br.MyFast.pagamento.Pagamento;
import essenti.com.br.MyFast.pagamento.PagamentoService;
import essenti.com.br.MyFast.util.FeedbackHelper;
import essenti.com.br.MyFast.util.NetworkUtil;
import essenti.com.br.MyFast.util.date.DateUtil;
import essenti.com.br.MyFast.webservice.ServiceGenerator;
import io.fabric.sdk.android.Fabric;
import java.util.Calendar;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import smtchahal.materialspinner.MaterialSpinner;
import timber.log.Timber;

import static essenti.com.br.MyFast.android.widget.MensagemDialogo.MENSAGEM_ALERTA;
import static essenti.com.br.MyFast.android.widget.MensagemDialogo.MENSAGEM_ERRO;
import static essenti.com.br.MyFast.android.widget.MensagemDialogo.MENSAGEM_SUCESSO;

/**
 * @author Filipe Bezerra
 */
public class OutrosPagamentosActivity extends BaseActivity {

    public static final String EXTRA_FORMA_PAGAMENTO =
            OutrosPagamentosActivity.class.getSimpleName()+".extraFormaPagamento";

    private static final String TAG = OutrosPagamentosActivity.class.getSimpleName();

    private static final int ERRO_ENVIO_PAGAMENTO = 1;

    @IntDef({ ERRO_ENVIO_PAGAMENTO }) private @interface TipoError{}

    @BindView(R.id.input_layout_nome_cobranca) protected TextInputLayout mInputLayoutNomeCobranca;
    @BindView(R.id.edit_text_nome_cobranca) protected TextInputEditText mEditTextNomeCobranca;

    @BindView(R.id.container_pagamento_com_cartao) protected LinearLayout mContainerPagamentoComCartao;

    @BindView(R.id.input_layout_valor) protected TextInputLayout mInputLayoutValor;
    @BindView(R.id.edit_text_valor) protected TextInputEditText mEditTextValor;

    @BindView(R.id.edit_text_data_transacao) protected TextInputEditText mEditTextDataTransacao;

    @BindView(R.id.spinner_bandeira_cartao) protected MaterialSpinner mSpinnerBandeiraCartao;

    @BindView(R.id.input_layout_nsu) protected TextInputLayout mInputLayoutNsu;
    @BindView(R.id.edit_text_nsu) protected TextInputEditText mEditTextNsu;

    @BindView(R.id.input_layout_cod_autorizacao) protected TextInputLayout mInputLayoutCodAutorizacao;
    @BindView(R.id.edit_text_cod_autorizacao) protected TextInputEditText mEditTextCodAutorizacao;

    private Mesa mMesaEmAtendimento;

    private Cobranca mFormaPagamento;

    private PagamentoService mPagamentoService;

    private BandeiraCartaoAdapter mBandeiraCartaoAdapter;

    private MensagemDialogo mDialogoProgresso;

    private int mResultCode = Activity.RESULT_CANCELED;

    private Calendar mDataPagamento = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindExtras();
        bindAdapterBandeiraCartao();
        mPagamentoService = ServiceGenerator.createService(PagamentoService.class, this);
    }

    private void bindExtras() {
        if (!getIntent().hasExtra(App.EXTRA_MESA_ATENDIMENTO)) {
            Toast.makeText(getApplicationContext(), "A mesa em atendimento não foi "
                            + "passada via Intent.putExtra(App.EXTRA_MESA_ATENDIMENTO, Parcelable)",
                    Toast.LENGTH_LONG).show();
            finish();
        }
        mMesaEmAtendimento = getIntent().getParcelableExtra(App.EXTRA_MESA_ATENDIMENTO);

        if (!getIntent().hasExtra(EXTRA_FORMA_PAGAMENTO)) {
            Toast.makeText(getApplicationContext(), "A forma de pagamento não foi "
                            + "passada via Intent.putExtra(EXTRA_FORMA_PAGAMENTO, Parcelable)",
                    Toast.LENGTH_LONG).show();
            finish();
        }

        mFormaPagamento = getIntent().getParcelableExtra(EXTRA_FORMA_PAGAMENTO);
        getSupportActionBar().setTitle(getString(R.string.new_title_activity_outros_pagamentos,
                mFormaPagamento.getDescricao()));

        if (mFormaPagamento.getCartao().equalsIgnoreCase("S")) {
            mContainerPagamentoComCartao.setVisibility(View.VISIBLE);
            mInputLayoutNomeCobranca.setVisibility(View.GONE);
            bindDataPagamento();
        } else {
            mEditTextNomeCobranca.setText(mFormaPagamento.getDescricao());
        }
    }

    private void bindAdapterBandeiraCartao() {
        mBandeiraCartaoAdapter = new BandeiraCartaoAdapter(this);
        mSpinnerBandeiraCartao.setAdapter(mBandeiraCartaoAdapter);
    }

    @Override
    protected int provideLayoutResource() {
        return R.layout.activity_outros_pagamentos;
    }

    @Override
    protected boolean shouldDisplayHomeAsUpEnabled() {
        return true;
    }

    @Override
    protected int provideUpIndicator() {
        return R.drawable.ic_arrow_back_white_24dp;
    }

    @OnTouch(R.id.edit_text_data_transacao)
    boolean onTouchEditTextDataEmissao(MotionEvent e) {
        if (e.getAction() == MotionEvent.ACTION_UP) {
            new CalendarDatePickerDialogFragment()
                    .setOnDateSetListener(new CalendarDatePickerDialogFragment.OnDateSetListener() {
                        @Override
                        public void onDateSet(CalendarDatePickerDialogFragment dialog,
                                int year, int monthOfYear, int dayOfMonth) {
                            mDataPagamento.set(Calendar.YEAR, year);
                            mDataPagamento.set(Calendar.MONTH, monthOfYear);
                            mDataPagamento.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                            bindDataPagamento();
                        }
                    })
                    .setPreselectedDate(
                            mDataPagamento.get(Calendar.YEAR),
                            mDataPagamento.get(Calendar.MONTH),
                            mDataPagamento.get(Calendar.DAY_OF_MONTH)
                    )
                    .setThemeCustom(R.style.Widget_MyFast_BetterPickersDialogs)
                    .show(getSupportFragmentManager(), "DatePickerDialog");
            return true;
        }
        return false;
    }

    private void bindDataPagamento() {
        mEditTextDataTransacao.setText(DateUtil.formatDateString(mDataPagamento));
    }

    @OnClick(R.id.btnConfirmarPagamento)
    void clickButtonConfirmarPagamento() {
        boolean isCamposRequeridosNaoPreenchidos = false;

        if (mFormaPagamento.getCartao().equalsIgnoreCase("S")) {
            if (mSpinnerBandeiraCartao.getSelectedItemPosition() < 0) {
                mSpinnerBandeiraCartao.setError(R.string.error_bandeira_cartao_requerido);
                isCamposRequeridosNaoPreenchidos = true;
            } else {
                mSpinnerBandeiraCartao.setError(null);
            }

            final String nsu = mEditTextNsu.getText().toString();
            if (TextUtils.isEmpty(nsu)) {
                mInputLayoutNsu.setError(getString(R.string.error_nsu_requerido));
                isCamposRequeridosNaoPreenchidos = true;
            } else {
                mInputLayoutNsu.setErrorEnabled(false);
            }

            final String codAutorizacao = mEditTextCodAutorizacao.getText().toString();
            if (TextUtils.isEmpty(codAutorizacao)) {
                mInputLayoutCodAutorizacao.setError(getString(R.string.error_cod_autorizacao_requerido));
                isCamposRequeridosNaoPreenchidos = true;
            } else {
                mInputLayoutCodAutorizacao.setErrorEnabled(false);
            }
        }

        final String valorPagamentoStr = mEditTextValor.getText().toString();
        if (TextUtils.isEmpty(valorPagamentoStr) || Double.valueOf(valorPagamentoStr) == 0) {
            mInputLayoutValor.setError(getString(R.string.error_valor_pagamento_requerido));
            isCamposRequeridosNaoPreenchidos = true;
        } else {
            final Double valorPagamento = Double.valueOf(valorPagamentoStr);

            if (valorPagamento > mMesaEmAtendimento.getSaldo()) {
                mInputLayoutValor.setError(getString(R.string.error_valor_pagamento_maior_que_saldo));
                isCamposRequeridosNaoPreenchidos = true;
            } else {
                mInputLayoutValor.setErrorEnabled(false);
            }
        }

        if (!isCamposRequeridosNaoPreenchidos) {
            sendPagamento();
        }
    }

    private void sendPagamento() {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            if (mPagamentoService != null) {

                final String formaPagamento = mFormaPagamento.getDescricao();
                final String codigoCobranca = mFormaPagamento.getCodigo();
                final Double valorPagamento = Double.valueOf(mEditTextValor.getText().toString());
                final Integer numeroMesa = mMesaEmAtendimento.getNumero();
                final int codigoCardapio = App.getInstancia().getCodigoCardapio();
                Pagamento pagamento;
                if (mFormaPagamento.getCartao().equalsIgnoreCase("S")) {
                    final String bandeira = mBandeiraCartaoAdapter.getItem(
                            mSpinnerBandeiraCartao.getSelectedItemPosition()).getNome();
                    final String nsu = mEditTextNsu.getText().toString();
                    final String codAutorizacao = mEditTextCodAutorizacao.getText().toString();

                    pagamento = new Pagamento(formaPagamento, valorPagamento, bandeira, nsu,
                            codAutorizacao, numeroMesa, codigoCardapio, codigoCobranca);
                } else {
                    pagamento = new Pagamento(formaPagamento, valorPagamento, numeroMesa,
                            codigoCardapio, codigoCobranca);
                }

                mPagamentoService.inserirPagamento(pagamento)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Subscriber<PagamentoService.InserirPagamentoResponse>() {
                                    @Override
                                    public void onStart() {
                                        showDialogoProgresso(
                                                getString(R.string.message_enviando_pagamento));
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        showExcecao(e, ERRO_ENVIO_PAGAMENTO);
                                    }

                                    @Override
                                    public void onNext(
                                            PagamentoService.InserirPagamentoResponse response) {
                                        if (response.result.equalsIgnoreCase("OK")) {
                                            mResultCode = App.RESULT_PAGAMENTO_EFETUADO;
                                            showDialogoPagamentoEnviado();
                                        } else {
                                            showExcecaoNegocio(response.result);
                                        }
                                    }

                                    @Override
                                    public void onCompleted() {
                                        dismissDialogoProgresso();
                                    }
                                }
                        );
            }
        } else {
            FeedbackHelper.showOfflineMessage(this, null,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            sendPagamento();
                        }
                    });
        }
    }

    private void showDialogoProgresso(String mensagem) {
        if (mDialogoProgresso == null) {
            mDialogoProgresso = App.getInstancia().exibirProgresso(this,
                    getString(R.string.message_processando), mensagem);
            mDialogoProgresso.exibir();
        } else {
            mDialogoProgresso.setTexto(mensagem);
        }
    }

    private void dismissDialogoProgresso() {
        if (mDialogoProgresso != null) {
            if (mDialogoProgresso.isExibindo()) {
                mDialogoProgresso.esconder();
            }

            mDialogoProgresso = null;
        }
    }

    private void showExcecao(Throwable e, @TipoError int tipoErro) {
        dismissDialogoProgresso();

        if (Fabric.isInitialized()) {
            Crashlytics.logException(e);
        }

        String mensagem = getString(R.string.message_template_excecao);
        String when = "";
        switch (tipoErro) {
            case ERRO_ENVIO_PAGAMENTO: {
                when = getString(R.string.message_excecao_enviando_pagamento);
                break;
            }
        }

        Timber.e(e, when);

        mensagem += when + getString(R.string.message_excecao_sera_reportada);

        App.getInstancia()
                .exibirMensagem(this, MENSAGEM_ERRO, getString(R.string.titulo_atencao), mensagem);
    }

    private void showExcecaoNegocio(String mensagem) {
        if (Fabric.isInitialized()) {
            Crashlytics.log(Log.WARN, TAG, mensagem);
        }

        Timber.w(mensagem);

        App.getInstancia()
                .exibirMensagem(this, MENSAGEM_ALERTA, getString(R.string.titulo_atencao), mensagem);
    }

    private void showDialogoPagamentoEnviado() {
        App.getInstancia().exibirMensagem(this, MENSAGEM_SUCESSO, getString(R.string.titulo_atencao),
                getString(R.string.message_pagamento_enviado), new
                        MensagemDialogo.OnMensagemClickListener() {
                            @Override
                            public void onConfirmarClick() {
                                finish();
                            }

                            @Override
                            public void onCancelarClick() {}
                        });
    }

    @Override
    public void finish() {
        setResult(mResultCode);
        super.finish();
    }
}

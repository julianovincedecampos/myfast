package essenti.com.br.MyFast.cardapio;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import essenti.com.br.MyFast.app.App;
import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.app.ConfiguracaoActivity;
import essenti.com.br.MyFast.android.widget.MensagemDialogo;

/**
 * Controlador da tela Configuração do {@link Cardapio} padrão. Esta tela apresenta a listagem
 * dos cardápios cadastrados.
 *
 * @author Diego, Filipe Bezerra
 * @version 1.4.0, 16/03/2016
 * @since 1.0.0
 */
public class CardapiosActivity extends AppCompatActivity {
    public static final String EXTRA_URL = "URL";

    @BindView(R.id.listViewCardapios) ListView mListViewCardapios;

    private CardapioAdapter mCardapioAdapter;
    private List<Cardapio> mCardapios = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cardapios);
        ButterKnife.bind(this);

        mCardapioAdapter = new CardapioAdapter(this, mCardapios);
        mListViewCardapios.setFastScrollEnabled(true);
        mListViewCardapios.setAdapter(mCardapioAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GetCardapiosTask().execute(getIntent().getStringExtra(EXTRA_URL));
    }

    @OnItemClick(R.id.listViewCardapios)
    public void onItemClick(int position) {
        final Cardapio cardapio = mCardapioAdapter.getItem(position);
        Intent intent = new Intent();
        intent.putExtra(ConfiguracaoActivity.EXTRA_CODIGO_CARDAPIO, cardapio.getCodigo());
        setResult(App.CARDAPIO_SELECIONADO, intent);
        finish();
    }

    private class GetCardapiosTask extends AsyncTask<String, Void, List<Cardapio>> {
        private final MensagemDialogo dialogo;

        GetCardapiosTask() {
            this.dialogo = App.getInstancia().exibirProgresso(CardapiosActivity.this,
                    "Processando", "Carregando os cardápios disponíveis...");

        }

        private String toString(InputStream is) throws IOException {
            byte[] bytes = new byte[1024];
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            int lidos;
            while ((lidos = is.read(bytes)) > 0) {
                baos.write(bytes, 0, lidos);
            }
            return new String(baos.toByteArray());
        }

        private List<Cardapio> getRegistros(String jsonString) {
            List<Cardapio> registros = new ArrayList<Cardapio>();
            try {
                JSONObject lista = new JSONObject(jsonString);
                JSONArray grupos = lista.getJSONArray("ListarCardapiosResult");
                JSONObject grupoJson;
                for (int i = 0; i < grupos.length(); i++) {
                    grupoJson = grupos.getJSONObject(i);
                    Cardapio registro = new Cardapio(grupoJson.getInt("Codcardapio"),
                            grupoJson.getString("Descricao"));
                    registros.add(registro);
                }
            } catch (JSONException e) {
                Log.e("MyFastService", "Erro no parsing do JSON", e);
            }
            return registros;
        }

        @Override
        protected void onPreExecute() {
            dialogo.exibir();
        }

        @Override
        protected List<Cardapio> doInBackground(String... params) {
            List<Cardapio> registros = new ArrayList<Cardapio>();
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httpget = new HttpGet(params[0]);
            try {
                HttpResponse response = httpclient.execute(httpget);
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    InputStream instream = entity.getContent();
                    String json = toString(instream);
                    instream.close();
                    registros.addAll(getRegistros(json));
                    return registros;
                }
            } catch (Exception e) {
                Log.e("MyFastWCF", "Falha ao acessar Web service", e);
            }
            try {
           /*     VectorCardapio mCardapios = App.getInstancia().getWsServico().ListarCardapios(App.getInstancia().getOperador(), null);
                if (mCardapios != null) {
                    CardapiosActivity.this.mCardapios.clear();
                    CardapiosActivity.this.mCardapios.addAll(mCardapios);
                    return true;
                } else {
                    App.getInstancia().exibirMensagem(CardapiosActivity.this, MensagemDialogo.MENSAGEM_ERRO, "Atenção", "Não foi retornado nenhum cardápio! Verifique com o responsável pelo TI.");
                }*/

            } catch (final Exception e) {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        App.getInstancia().exibirMensagem(CardapiosActivity.this, MensagemDialogo.MENSAGEM_ERRO, "Atenção", String.format("Ocorreu um erro ao solicitar os cardápios! %s", e.getMessage()));
                    }
                });

            }

            return registros;
        }

        @Override
        protected void onPostExecute(List<Cardapio> registros) {
            mCardapios.clear();
            mCardapios.addAll(registros);
            mCardapioAdapter.notifyDataSetChanged();
            dialogo.esconder();
        }
    }
}


package essenti.com.br.MyFast.a_migration.data.produtos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import essenti.com.br.MyFast.produto.Produto;
import java.util.List;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * @author Filipe Bezerra
 */
public interface ProdutoService {
    String SEM_CODIGO_AUXILIAR = "0";
    String SEM_NOME_PRODUTO = "T";

    @GET("/ServiceProduto.svc/ListarTodosProdutos/{codigoCardapio}/{codigoAuxiliar}/{nomeProduto}")
    Observable<ListarTodosProdutosResult> listarTodosProdutos(
            @Path("codigoCardapio") int codigoCardapio,
            @Path("codigoAuxiliar") String codigoAuxiliar,
            @Path("nomeProduto") String nomeProduto);

    class ListarTodosProdutosResult {
        @SerializedName("ListarTodosProdutosResult") @Expose public List<Produto> produtos;
    }
}

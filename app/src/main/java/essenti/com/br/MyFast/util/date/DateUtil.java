package essenti.com.br.MyFast.util.date;

import essenti.com.br.MyFast.cielo.CieloUtil;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import timber.log.Timber;

import static essenti.com.br.MyFast.app.App.BRAZIL_LOCALE;

/**
 * @author Filipe Bezerra
 */
public class DateUtil {
    private static final DateFormat DATE_FORMATTER_AS_DD_MM_YYYY =
            new SimpleDateFormat("dd/MM/yyyy", BRAZIL_LOCALE);

    public static Date dateStringToDate(String dateString, String pattern) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern, BRAZIL_LOCALE);
        try {
            return dateFormat.parse(dateString);
        } catch (ParseException e) {
            Timber.e(e, "Falha ao converter data em texto para objeto java.util.Date");
            return null;
        }
    }

    public static String formatDateString(String dateString, String fromPattern, String toPattern) {
        try {
            return String.valueOf(
                    android.text.format.DateFormat.format(toPattern,
                            new SimpleDateFormat(fromPattern, BRAZIL_LOCALE).parse(dateString)));
        } catch (ParseException e) {
            Timber.e(e, "Falha ao converter data em texto para objeto java.util.Date");
            return dateString;
        }
    }

    public static String formatCurrentDateAsString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(CieloUtil.DATE_PATTERN, BRAZIL_LOCALE);
        return dateFormat.format(new Date());
    }

    public static String formatDateString(Calendar calendar) {
        return DATE_FORMATTER_AS_DD_MM_YYYY.format(calendar.getTime());
    }
}

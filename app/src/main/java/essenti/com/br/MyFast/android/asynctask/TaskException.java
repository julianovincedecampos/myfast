package essenti.com.br.MyFast.android.asynctask;

import android.support.annotation.NonNull;

/**
 * MyFast.Android
 *
 * @author Filipe Bezerra on 14/12/2015.
 */
public class TaskException extends Exception {
    public TaskException(String error) {
        super(error);
    }

    public TaskException(Throwable throwable) {
        super(throwable);
    }

    public static TaskException of(@NonNull String error) {
        return new TaskException(error);
    }

    public static TaskException of(@NonNull Throwable error) {
        return new TaskException(error);
    }
}

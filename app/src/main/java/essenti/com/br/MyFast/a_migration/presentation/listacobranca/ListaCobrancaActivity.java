package essenti.com.br.MyFast.a_migration.presentation.listacobranca;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;
import essenti.com.br.MyFast.Injection;
import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.a_migration.data.cobrancas.CobrancaService;
import essenti.com.br.MyFast.a_migration.domain.dto.RespostaListaCobrancas;
import essenti.com.br.MyFast.a_migration.domain.factory.CobrancaFactories;
import essenti.com.br.MyFast.a_migration.domain.pojo.Cobranca;
import essenti.com.br.MyFast.a_migration.presentation.widget.recyclerview.OnItemClickListener;
import essenti.com.br.MyFast.a_migration.presentation.widget.recyclerview.OnItemTouchListener;
import essenti.com.br.MyFast.android.activity.ActivityNavigator;
import essenti.com.br.MyFast.android.activity.BaseActivity;
import essenti.com.br.MyFast.android.recyclerview.DividerDecoration;
import essenti.com.br.MyFast.android.widget.MensagemDialogo;
import essenti.com.br.MyFast.app.App;
import essenti.com.br.MyFast.mesa.Mesa;
import essenti.com.br.MyFast.util.FeedbackHelper;
import essenti.com.br.MyFast.util.NetworkUtil;
import io.fabric.sdk.android.Fabric;
import java.util.List;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

import static essenti.com.br.MyFast.android.widget.MensagemDialogo.MENSAGEM_ERRO;

/**
 * @author Filipe Bezerra
 */
public class ListaCobrancaActivity extends BaseActivity implements OnItemClickListener {

    private static final String TAG = ListaCobrancaActivity.class.getSimpleName();

    private static final int ERRO_LISTANDO_COBRANCAS = 1;

    @BindView(R.id.recycler_view_cobrancas) protected RecyclerView mRecyclerViewCobrancas;
    @BindView(R.id.container_ajuda) protected LinearLayout mAjudaContainer;
    @BindView(R.id.texto_ajuda) protected TextView mTextViewAjuda;

    @IntDef({ ERRO_LISTANDO_COBRANCAS }) private @interface TipoError{}

    private Mesa mMesaEmAtendimento;

    private ListaCobrancaAdapter mListaCobrancaAdapter;

    private CobrancaService mCobrancaService;

    private MensagemDialogo mDialogoProgresso;

    private CompositeSubscription mCompositeSubscription;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.tag(TAG);

        mMesaEmAtendimento = getIntent().getParcelableExtra(App.EXTRA_MESA_ATENDIMENTO);

        mRecyclerViewCobrancas.setHasFixedSize(true);
        mRecyclerViewCobrancas.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerViewCobrancas.addItemDecoration(new DividerDecoration(this));

        mCompositeSubscription = new CompositeSubscription();

        mCobrancaService = Injection.provideCobrancaService(this);
    }

    @Override protected int provideLayoutResource() {
        return R.layout.activity_lista_cobranca;
    }

    @Override protected boolean shouldDisplayHomeAsUpEnabled() {
        return true;
    }

    @Override protected int provideUpIndicator() {
        return R.drawable.ic_arrow_back_white_24dp;
    }

    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_lista_cobranca, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_search);

        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setQueryHint(getString(R.string.search_hint_nome_cobranca));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(final String newText) {
                if (mListaCobrancaAdapter != null) {
                    mListaCobrancaAdapter.getFilter().filter(newText,
                            new Filter.FilterListener() {
                                @Override
                                public void onFilterComplete(int count) {
                                    if (count == 0) {
                                        indicarAjudar(getString(
                                                R.string.text_nenhuma_cobranca_encontrada));
                                    } else {
                                        removerAjuda();
                                    }
                                }
                            });
                    return true;
                }
                return false;
            }
        });

        MenuItemCompat.setOnActionExpandListener(menuItem,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        searchView.requestFocus();
                        return true;
                    }

                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        searchView.clearFocus();
                        searchView.setQuery("", false);
                        return true;
                    }
                });

        return true;
    }

    @Override protected void onStart() {
        super.onStart();
        requestCobrancasDisponiveis();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mCompositeSubscription.clear();
    }

    private void requestCobrancasDisponiveis() {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            if (mCobrancaService != null) {
                mCompositeSubscription.add(mCobrancaService
                        .listarCobrancas()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new ListarCobrancasSubscriber()));
            }
        } else {
            FeedbackHelper.showOfflineMessage(this, null,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(
                                @NonNull MaterialDialog d, @NonNull DialogAction action) {
                            requestCobrancasDisponiveis();
                        }
                    });
        }
    }

    private void showCobrancasDisponiveis(final List<Cobranca> cobrancas) {
        if (cobrancas != null && !cobrancas.isEmpty()) {
            mRecyclerViewCobrancas.setAdapter(
                    mListaCobrancaAdapter = new ListaCobrancaAdapter(this, cobrancas));
            mRecyclerViewCobrancas.addOnItemTouchListener(
                    new OnItemTouchListener(this, mRecyclerViewCobrancas, this));
            removerAjuda();
        } else {
            indicarAjudar(getString(R.string.text_empty_lista_cobranca));
        }
    }

    private void removerAjuda() {
        if (mAjudaContainer.getVisibility() == View.VISIBLE) {
            mAjudaContainer.setVisibility(View.GONE);
            mRecyclerViewCobrancas.setVisibility(View.VISIBLE);
        }
    }

    private void indicarAjudar(@Nullable String textoAjuda) {
        if (!TextUtils.isEmpty(textoAjuda)) {
            mTextViewAjuda.setText(textoAjuda);
        }

        if (mAjudaContainer.getVisibility() == View.GONE) {
            mAjudaContainer.setVisibility(View.VISIBLE);
            mRecyclerViewCobrancas.setVisibility(View.GONE);
        }
    }

    @Override public void onSingleTapUp(View view, int position) {
        final Cobranca cobranca = mListaCobrancaAdapter.getItem(position);
        if (cobranca != null) {
            ActivityNavigator.startOutrosPagamentosActivity(this, mMesaEmAtendimento, cobranca);
        }
    }

    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == App.RESULT_PAGAMENTO_EFETUADO
                && requestCode == ActivityNavigator.REQUEST_OUTROS_PAGAMENTOS) {
            setResult(resultCode);
            finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override public void onLongPress(View view, int position) {

    }

    private void showDialogoProgresso(String mensagem) {
        if (mDialogoProgresso == null) {
            mDialogoProgresso = App.getInstancia().exibirProgresso(this, "Processando", mensagem);
            mDialogoProgresso.exibir();
        } else {
            mDialogoProgresso.setTexto(mensagem);
        }
    }

    private void showExcecao(Throwable e, @TipoError int tipoErro) {
        dismissDialogoProgresso();

        if (Fabric.isInitialized()) {
            Crashlytics.logException(e);
        }

        String mensagem = "Infelizmente houve uma falha ";
        String when = "";
        switch (tipoErro) {
            case ERRO_LISTANDO_COBRANCAS: {
                when = "ao listar cobranças disponíveis.";
                break;
            }
        }

        Timber.e(e, when);

        mensagem += when + "\nPor favor tente novamente. Esta falha será reportada para o "
                + "administrador do sistema!";

        App.getInstancia().exibirMensagem(this, MENSAGEM_ERRO, "Atenção", mensagem);
    }

    private void dismissDialogoProgresso() {
        if (mDialogoProgresso != null) {
            if (mDialogoProgresso.isExibindo()) {
                mDialogoProgresso.esconder();
            }

            mDialogoProgresso = null;
        }
    }

    private class ListarCobrancasSubscriber extends Subscriber<RespostaListaCobrancas> {
        @Override
        public void onStart() {
            showDialogoProgresso(getString(R.string.mensagem_listando_cobrancas_disponiveis));
        }

        @Override
        public void onError(Throwable e) {
            showExcecao(e, ERRO_LISTANDO_COBRANCAS);
        }

        @Override
        public void onNext(RespostaListaCobrancas resposta) {
            showCobrancasDisponiveis(CobrancaFactories.createListProduto(resposta.cobrancas));
        }

        @Override
        public void onCompleted() {
            dismissDialogoProgresso();
        }
    }

}

package essenti.com.br.MyFast.a_migration.domain.utils;

/**
 * @author Filipe Bezerra
 */
public class StringUtils {
    private StringUtils() {/* No constructor*/}

    public static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }
}

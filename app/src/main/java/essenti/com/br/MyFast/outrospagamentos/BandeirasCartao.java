package essenti.com.br.MyFast.outrospagamentos;

import android.support.annotation.DrawableRes;
import essenti.com.br.MyFast.R;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author Filipe Bezerra
 */
public class BandeirasCartao {
    private static final List<BandeiraCartao> BANDEIRAS_CARTAO
            = Arrays.asList(
            BandeiraCartao.from("Amex", R.drawable.ic_card_amex),
            BandeiraCartao.from("Aura", R.drawable.ic_card_aura),
            BandeiraCartao.from("Banco do Brasil", R.drawable.ic_card_bancodobrasil),
            BandeiraCartao.from("Banrisul", R.drawable.ic_card_banrisul),
            BandeiraCartao.from("Bradesco", R.drawable.ic_card_bradesco),
            BandeiraCartao.from("Diners", R.drawable.ic_card_diners),
            BandeiraCartao.from("Elo", R.drawable.ic_card_elo),
            BandeiraCartao.from("Hipercard", R.drawable.ic_card_hipercard),
            BandeiraCartao.from("HSBC", R.drawable.ic_card_hsbc),
            BandeiraCartao.from("Itaú", R.drawable.ic_card_itau),
            BandeiraCartao.from("MasterCard", R.drawable.ic_card_mastercard),
            BandeiraCartao.from("Visa", R.drawable.ic_card_visa)
    );

    public static List<BandeiraCartao> getListaBandeiraCartao() {
        return Collections.unmodifiableList(BANDEIRAS_CARTAO);
    }

    @DrawableRes public static int getIconeBandeira(String nome) {
        if (nome == null) {
            return R.drawable.ic_card_unknown;
        }

        for (BandeiraCartao bandeiraCartao : getListaBandeiraCartao()) {
            if (bandeiraCartao.getNome().contains(nome)) {
                return bandeiraCartao.getIcone();
            }
        }
        return R.drawable.ic_card_unknown;
    }
}

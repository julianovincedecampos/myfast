package essenti.com.br.MyFast.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import essenti.com.br.MyFast.android.widget.MensagemDialogo;
import essenti.com.br.MyFast.android.widget.MensagemDialogo.OnMensagemClickListener;
import essenti.com.br.MyFast.mesa.Mesa;
import essenti.com.br.MyFast.produto.Grupo;
import essenti.com.br.MyFast.produto.Produto;
import essenti.com.br.MyFast.usuario.Usuario;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;
import static essenti.com.br.MyFast.android.widget.MensagemDialogo.MENSAGEM_ALERTA;
import static essenti.com.br.MyFast.android.widget.MensagemDialogo.MENSAGEM_PROGRESSO;

/**
 * @author Filipe Bezerra, Diego
 * @since 1.0.0, 25/10/2015
 */
public class App {
    public static final Locale BRAZIL_LOCALE = new Locale("pt", "BR");

    private static final String PREFS_NOME = "MyFast";

    private static final String URL_WEBSERVICE_NOME = "URLWebservice";
    private static final String CODIGO_CARDAPIO_NOME = "CodigoCardapio";
    private static final String CODIGO_AMBIENTE_NOME = "CodigoAmbiente";
    private static final String INDICE_PAGAMENTO_MOBILE = "IndicePagamentoMobile";
    private static final String PREF_PERMITIR_ALTERAR_GARCOM = "PermitirAlterarGarcom";

    public static final int OPERADOR_SELECIONADO = 1;
    public static final int CARDAPIO_SELECIONADO = 3;
    public static final int AMBIENTE_SELECIONADO = 4;
    public static final int SELECIONAR_OPERADOR = 200;
    public static final int ADICIONAR_PRODUTO = 300;
    public static final int PRODUTO_ADICIONADO = 301;
    public static final int CONFIGURAR_URL = 302;
    public static final int ATENDIMENTO_MESA = 303;
    public static final int TROCAR_MESA = 303;
    public static final int SELECIONAR_CARDAPIO = 307;
    public static final int SELECIONAR_AMBIENTE = 308;
    public static final int ADICIONAR_PRODUTO_AVULSO = 309;
    public static final int PRODUTO_AVULSO_ADICIONADO = 310;
    public static final int AUTENTICAR_OPERADOR = 311;

    public static final int REQUEST_PAGAMENTO_CARTAO = 312;
    public static final int RESULT_PAGAMENTO_EFETUADO = 313;

    public static final int PAGAMENTO_MOBILE_DESABILITADO = 0;
    public static final int PAGAMENTO_MOBILE_HABILITADO = 1;

    public static final String SENHA_CONFIRMACAO_CONFIGURACAO = "esmyfastsi@2016";

    public static final String EXTRA_MESA_ATENDIMENTO = "mesaAtendimento";

    private static App sInstancia;

    private List<Mesa> mMesas = new ArrayList<>();
    private List<Grupo> mGrupos;
    private String mUrl;
    private int mCodigoCardapio;
    private int mCodigoAmbiente;
    private Context mContext;
    private List<Produto> mProdutosAvulsos;
    private int mIndicePagamentoMobile;
    private List<Usuario> mUsuarios;

    private boolean mIsPermitidoAlterarGarcom;

    private App() {}

    public static App getInstancia() {
        if (sInstancia == null) {
            sInstancia = new App();
        }

        return sInstancia;
    }

    public void setMesas(List<Mesa> mesas) {
        mMesas = mesas;
    }

    public List<Mesa> getMesas() {
        return mMesas;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        mUsuarios = usuarios;
    }

    public boolean hasUsuario(int matricula) {
        if (mUsuarios == null)
            return false;

        for (Usuario usuario : mUsuarios) {
            if (usuario.getMatricula() == matricula)
                return true;
        }

        return false;
    }

    @Nullable
    public Usuario getUsuario(int matricula) {
        for (Usuario usuario : mUsuarios) {
            if (usuario.getMatricula() == matricula) {
                return usuario;
            }
        }
        return null;
    }

    public boolean setMesa(Mesa mesa) {
        if (mesa != null && mesa.getNumero() != null) {
            final int index = mMesas.indexOf(mesa);

            if (index != -1) {
                mMesas.set(index, mesa);
                return true;
            }
        }
        return false;
    }

    public List<Grupo> getGrupos() {
        if (this.mGrupos == null) {
            this.mGrupos = new ArrayList<>();
        }
        return mGrupos;
    }

    public void setGrupos(List<Grupo> grupos) {
        this.mGrupos = grupos;
    }

    public void zerarQuantidadeProdutos(@NonNull List<Produto> produtos) {
        for (Produto produto : produtos) {
            produto.setQuantidade(0);
        }
    }

    public String getURL() {
        return mUrl;
    }

    public void setURL(String URL) {
        this.mUrl = URL;
        mContext.getSharedPreferences(PREFS_NOME, MODE_PRIVATE)
                .edit()
                .putString(URL_WEBSERVICE_NOME, URL)
                .apply();
    }

    public String getUrlImageCliente() {
        return Uri.parse(mUrl)
                .buildUpon()
                .appendPath("logo.png")
                .toString();
    }

    public int getCodigoCardapio() {
        return this.mCodigoCardapio;
    }

    public void setCodigoCardapio(int codigoCardapio) {
        this.mCodigoCardapio = codigoCardapio;
        mContext.getSharedPreferences(PREFS_NOME, MODE_PRIVATE)
                .edit()
                .putInt(CODIGO_CARDAPIO_NOME, codigoCardapio)
                .apply();
    }

    public int getCodigoAmbiente() {
        return mCodigoAmbiente;
    }

    public void setCodigoAmbiente(int codigoAmbiente) {
        this.mCodigoAmbiente = codigoAmbiente;
        mContext.getSharedPreferences(PREFS_NOME, MODE_PRIVATE)
                .edit()
                .putInt(CODIGO_AMBIENTE_NOME, codigoAmbiente)
                .apply();
    }

    public void inicializar(Context contexto) {
        this.mContext = contexto;
        SharedPreferences configuracao = contexto.getSharedPreferences(PREFS_NOME, MODE_PRIVATE);
        mUrl = configuracao.getString(URL_WEBSERVICE_NOME, "");
        mCodigoCardapio = configuracao.getInt(CODIGO_CARDAPIO_NOME, 0);
        mCodigoAmbiente = configuracao.getInt(CODIGO_AMBIENTE_NOME, 0);
        mIndicePagamentoMobile = configuracao.getInt(INDICE_PAGAMENTO_MOBILE, 0);
        mIsPermitidoAlterarGarcom = configuracao.getBoolean(PREF_PERMITIR_ALTERAR_GARCOM, false);
    }

    public void exibirMensagem(Context contexto, int tipo, String titulo, String texto) {
        exibirMensagem(contexto, tipo, titulo, texto, false, false, null);
    }

    public void exibirMensagem(Context contexto, int tipo, String titulo, String texto,
            OnMensagemClickListener listener) {
        exibirMensagem(contexto, tipo, titulo, texto, false, false, listener);
    }

    public void exibirMensagem(Context contexto, int tipo, String titulo, String texto,
            boolean exibirBotaoCancelar, boolean aceitarBotaoVoltar,
            OnMensagemClickListener listener) {
        final MensagemDialogo alerta = new MensagemDialogo(contexto, tipo);
        alerta.setTitulo(titulo);
        alerta.setTexto(texto);
        alerta.setConfirmarTexto("OK");
        if (exibirBotaoCancelar) {
            alerta.setCancelarTexto("Cancelar");
            alerta.setExibirBotaoCancelar(exibirBotaoCancelar);
        }
        alerta.setAceitarBotaoVoltar(aceitarBotaoVoltar);
        alerta.setListener(listener);
        alerta.exibir();
    }

    public void exibirQuestionamento(Context contexto, String titulo, String texto,
            OnMensagemClickListener listener) {
        final MensagemDialogo alerta = new MensagemDialogo(contexto, MENSAGEM_ALERTA);
        alerta.setTitulo(titulo);
        alerta.setTexto(texto);
        alerta.setConfirmarTexto("Sim");
        alerta.setCancelarTexto("Não");
        alerta.setExibirBotaoCancelar(true);
        alerta.setAceitarBotaoVoltar(false);
        alerta.setListener(listener);
        alerta.exibir();
    }

    public MensagemDialogo exibirProgresso(Context contexto, String titulo, String texto) {
        MensagemDialogo alerta = new MensagemDialogo(contexto, MENSAGEM_PROGRESSO);
        alerta.setTitulo(titulo);
        alerta.setTexto(texto);
        alerta.setAceitarBotaoVoltar(false);
        return alerta;
    }

    public List<Produto> getProdutosAvulsos() {
        return mProdutosAvulsos;
    }

    public void setProdutosAvulsos(List<Produto> produtosAvulsos) {
        mProdutosAvulsos = produtosAvulsos;
    }

    public int getIndicePagamentoMobile() {
        return mIndicePagamentoMobile;
    }

    public boolean isPagamentoMobileHabilitado() {
        return getIndicePagamentoMobile() == PAGAMENTO_MOBILE_HABILITADO;
    }

    public void setIndicePagamentoMobile(int indicePagamentoMobile) {
        mIndicePagamentoMobile = indicePagamentoMobile;
        mContext.getSharedPreferences(PREFS_NOME, MODE_PRIVATE)
                .edit()
                .putInt(INDICE_PAGAMENTO_MOBILE, mIndicePagamentoMobile)
                .apply();
    }

    public boolean isPermitidoAlterarGarcom() {
        return mIsPermitidoAlterarGarcom;
    }

    public void setPermitidoAlterarGarcom(boolean permitido) {
        mIsPermitidoAlterarGarcom = permitido;
        mContext.getSharedPreferences(PREFS_NOME, MODE_PRIVATE)
                .edit()
                .putBoolean(PREF_PERMITIR_ALTERAR_GARCOM, permitido)
                .apply();
    }
}

package essenti.com.br.MyFast.mesa;

import android.net.Uri;
import android.support.annotation.NonNull;
import essenti.com.br.MyFast.android.asynctask.AbstractAsyncTask;
import essenti.com.br.MyFast.android.asynctask.TaskCallback;
import essenti.com.br.MyFast.android.asynctask.TaskException;
import essenti.com.br.MyFast.app.App;
import java.io.IOException;
import java.io.InputStream;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Filipe Bezerra
 * @since 20/01/2016
 */
public class GetClienteTask extends AbstractAsyncTask<String, Void, Cliente> {
    private static final String PATH_SERVICE_MESA_CLIENTE = "ServiceMesa.svc/ObterCliente";
    private final String mCpf;
    private final int mNumeroMesa;

    public GetClienteTask(String cpf, int numeroMesa, @NonNull TaskCallback<Cliente> callback) {
        super(callback);
        mCpf = cpf;
        mNumeroMesa = numeroMesa;
    }

    @Override
    protected Cliente doInBackground(String... params) {
        final String url = Uri
                .parse(App.getInstancia().getURL())
                .buildUpon()
                .appendEncodedPath(PATH_SERVICE_MESA_CLIENTE)
                .appendPath(mCpf)
                .appendPath(String.valueOf(mNumeroMesa))
                .appendPath(String.valueOf(App.getInstancia().getCodigoCardapio()))
                .build()
                .toString();

        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(url);

        try {
            HttpResponse response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream instream = entity.getContent();
                String json = getStreamData(instream);
                instream.close();
                return getCliente(json);
            }
        } catch (JSONException | IOException e) {
            mException = TaskException.of(e);
        }

        return null;
    }

    private Cliente getCliente(String jsonString) throws JSONException {
        final JSONObject jsonObject = new JSONObject(jsonString);
        final JSONObject jsonCliente = jsonObject.getJSONObject("ObterClienteResult");

        Cliente cliente = new Cliente();
        cliente.setCodigo(jsonCliente.getInt("Codigo"));
        cliente.setEmail(jsonCliente.getString("Email"));
        cliente.setNome(jsonCliente.getString("Nome"));

        return cliente;
    }

    @Override
    protected void onPostExecute(Cliente cliente) {
        if (cliente != null && cliente.getCodigo() == 0) {
            mCallback.onResultNothing();
        } else {
            super.onPostExecute(cliente);
        }
    }
}

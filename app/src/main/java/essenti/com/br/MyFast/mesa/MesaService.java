package essenti.com.br.MyFast.mesa;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * .
 *
 * @author Filipe Bezerra
 * @version 1.4.0, 27/03/2016
 * @since 1.4.0
 */
public interface MesaService {
    String STATUS_TODOS = "T";
    String STATUS_EM_CONSUMO = "C";
    String STATUS_DISPONIVEL = "D";
    String STATUS_EM_FECHAMENTO = "F";

    @GET("ServiceMesa.svc/Mesas/{codigoCardapio}")
    Observable<ListaMesas> mesas(@Path("codigoCardapio") int codigoCardapio);

    @GET("ServiceMesa.svc/Mesas/{status}/{codigoCardapio}")
    Observable<ListaMesasStatus> mesas(@Path("codigoCardapio") int codigoCardapio,
            @Path("status") String status);

    @GET("ServiceMesa.svc/ObterMesa/{numeroMesa}/{codigoCardapio}")
    Observable<ResultadoObterMesa> obterMesa(@Path("numeroMesa") int numeroMesa,
            @Path("codigoCardapio") int codigoCardapio);

    @GET("ServiceMesa.svc/FecharMesa/{numeroMesa}/{codigoCardapio}")
    Observable<ResultadoFecharMesa> fecharMesa(@Path("numeroMesa") int numeroMesa,
            @Path("codigoCardapio") int codigoCardapio);

    @GET("ServiceMesa.svc/BloquearMesa/{numeroMesa}/{matricula}/{codigoCardapio}")
    Observable<ResultadoBloqueioMesa> bloquearMesa(@Path("numeroMesa") int numeroMesa,
            @Path("matricula") int matricula, @Path("codigoCardapio") int codigoCardapio);

    @GET("ServiceMesa.svc/DesbloquearMesa/{numeroMesa}/{matricula}/{codigoCardapio}/C")
    Observable<ResultadoDesbloqueioMesa> desbloquearMesa(@Path("numeroMesa") int numeroMesa,
            @Path("matricula") int matricula, @Path("codigoCardapio") int codigoCardapio);
}

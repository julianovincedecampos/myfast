package essenti.com.br.MyFast.mesa;

import android.net.Uri;
import android.support.annotation.NonNull;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.InputStream;

import essenti.com.br.MyFast.android.asynctask.AbstractAsyncTask;
import essenti.com.br.MyFast.android.asynctask.TaskCallback;
import essenti.com.br.MyFast.android.asynctask.TaskException;
import essenti.com.br.MyFast.app.App;

/**
 * Tarefa assíncrona para transferência das informações e itens consumidos da mesa para outra
 * via Web service.
 *
 * @author Filipe Bezerra
 * @version 1.4.0, 11/04/2016
 * @since 1.4.0
 */
public class GetTransferenciaMesa extends AbstractAsyncTask<Void, Void, String> {
    private static final String PATH_TRANSFERENCIA_MESA = "ServiceMesa.svc/TransfereMesa";

    private final String mMatriculaOperadorLogado;
    private final int mMesaOrigem;
    private final int mMesaDestino;
    private final int mMatriculaGarcomDestino;

    public GetTransferenciaMesa(String matricula, int mesaOrigem, int mesaDestino,
            int matriculaGarcomDestino, @NonNull TaskCallback<String> callback) {
        super(callback);
        mMatriculaOperadorLogado = matricula;
        mMesaOrigem = mesaOrigem;
        mMesaDestino = mesaDestino;
        mMatriculaGarcomDestino = matriculaGarcomDestino;
    }

    @Override
    protected String doInBackground(Void... params) {
        final String url = Uri.parse(App.getInstancia().getURL())
                .buildUpon()
                .appendPath(PATH_TRANSFERENCIA_MESA)
                .appendPath(mMatriculaOperadorLogado)
                .appendPath(String.valueOf(mMesaOrigem))
                .appendPath(String.valueOf(mMesaDestino))
                .appendPath(String.valueOf(mMatriculaGarcomDestino))
                .build()
                .toString();

        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(url);

        try {
            HttpResponse response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream instream = entity.getContent();
                String json = getStreamData(instream);
                instream.close();
                JSONObject objeto = new JSONObject(json);

                final String resultado = objeto.getString("TransfereMesaResult");

                if (resultado.equals("OK")) {
                    return resultado;
                } else {
                    mException = TaskException.of(resultado);
                    return null;
                }
            }
        } catch (Exception e) {
            mException = TaskException.of(String
                    .format("Ocorreu um erro ao tranferir a mesa! Erro: %s", e.getMessage()));
        }

        return null;
    }
}

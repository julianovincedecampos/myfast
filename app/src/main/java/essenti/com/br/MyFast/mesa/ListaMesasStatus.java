package essenti.com.br.MyFast.mesa;

import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * .
 *
 * @author Filipe Bezerra
 * @version 1.4.0, 27/03/2016
 * @since 1.4.0
 */
public class ListaMesasStatus {
    @SerializedName("ListarMesasStatusResult")
    public List<Mesa> mesas;
}

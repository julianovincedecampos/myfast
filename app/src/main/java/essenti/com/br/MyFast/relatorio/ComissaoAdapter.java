package essenti.com.br.MyFast.relatorio;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.util.date.DateUtil;
import essenti.com.br.MyFast.util.number.NumberUtil;

/**
 * Adaptador da lista de {@link essenti.com.br.MyFast.relatorio.Comissao}.
 *
 * @author Filipe Bezerra
 * @version 1.4.0
 * @since 1.4.0
 */
public class ComissaoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;

    class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.data) TextView data;
        @BindView(R.id.comissao) TextView comissao;
        @BindView(R.id.totalVenda) TextView totalVenda;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class FooterViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.somaTotalVenda) TextView somaTotalVenda;
        @BindView(R.id.somaComissao) TextView somaComissao;

        public FooterViewHolder(View footerView) {
            super(footerView);
            ButterKnife.bind(this, footerView);
        }
    }

    private double mTotalComissao;

    private double mTotalVenda;

    @NonNull private Context mContext;

    @NonNull private List<Comissao> mListaComissao;

    private boolean isPositionFooter(int position) {
        return position == mListaComissao.size();
    }

    private boolean isPositionOdd(int position) {
        return position % 2 != 0;
    }

    private void calculateTotals() {
        mTotalComissao = 0;
        mTotalVenda = 0;
        for (Comissao comissao : mListaComissao) {
            mTotalComissao += comissao.getTaxaServico();
            mTotalVenda += comissao.getValorVenda();
        }
    }

    public ComissaoAdapter(@NonNull Context context, @NonNull List<Comissao> listaComissao) {
        mContext = context;
        mListaComissao = listaComissao;
        calculateTotals();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_FOOTER:
                final View footerView = LayoutInflater.from(mContext)
                        .inflate(R.layout.footer_comissao, parent, false);
                return new FooterViewHolder(footerView);

            case TYPE_ITEM:
                final View itemView = LayoutInflater.from(mContext)
                        .inflate(R.layout.item_comissao, parent, false);
                return new ItemViewHolder(itemView);

            default: throw new IllegalArgumentException(
                    String.format(Locale.getDefault(), "View type %d not recognized", viewType));
        }
    }

    @SuppressLint("PrivateResource")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder) {
            final Comissao item = mListaComissao.get(position);

            ((ItemViewHolder) holder).data
                    .setText(DateUtil.formatDateString(item.getDataMovimento(),
                            "yyyy-MM-dd HH:mm:ss", "dd/MM/yyyy"));
            ((ItemViewHolder) holder).totalVenda
                    .setText(NumberUtil.formatAsCurrency(item.getValorVenda()));
            ((ItemViewHolder) holder).comissao
                    .setText(NumberUtil.formatAsCurrency(item.getTaxaServico()));

        } else if (holder instanceof FooterViewHolder) {
            ((FooterViewHolder) holder).somaTotalVenda
                    .setText(NumberUtil.formatAsCurrency(mTotalVenda));
            ((FooterViewHolder) holder).somaComissao
                    .setText(NumberUtil.formatAsCurrency(mTotalComissao));
        }

        if (isPositionOdd(position)) {
            holder.itemView.setBackgroundColor(
                    ContextCompat.getColor(mContext, R.color.material_grey_300));
        } else {
            holder.itemView.setBackgroundColor(
                    ContextCompat.getColor(mContext, android.R.color.transparent));
        }
    }

    @Override
    public int getItemCount() {
        return mListaComissao.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionFooter(position)) {
            return TYPE_FOOTER;
        } else {
            return TYPE_ITEM;
        }
    }

    public void changeData(List<Comissao> list) {
        if (mListaComissao.isEmpty() && (list == null || list.isEmpty())) {
            return;
        }

        mListaComissao.clear();

        if (list != null && !list.isEmpty()) {
            mListaComissao.addAll(list);
        }

        calculateTotals();
        notifyDataSetChanged();
    }
}

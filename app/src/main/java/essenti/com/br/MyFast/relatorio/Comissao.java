package essenti.com.br.MyFast.relatorio;

import com.google.gson.annotations.SerializedName;

/**
 *
 *
 * @author Filipe Bezerra
 * @version 1.4.0, 04/04/2016
 */
public class Comissao {
    @SerializedName("Codusur")
    private int codUsuario;

    @SerializedName("Dtmovimento")
    private String dataMovimento;

    @SerializedName("Valortaxaservico")
    private double taxaServico;

    @SerializedName("Valorvenda")
    private double valorVenda;

    public int getCodUsuario() {
        return codUsuario;
    }

    public Comissao setCodUsuario(int codUsuario) {
        this.codUsuario = codUsuario;
        return this;
    }

    public String getDataMovimento() {
        return dataMovimento;
    }

    public Comissao setDataMovimento(String dataMovimento) {
        this.dataMovimento = dataMovimento;
        return this;
    }

    public double getTaxaServico() {
        return taxaServico;
    }

    public Comissao setTaxaServico(double taxaServico) {
        this.taxaServico = taxaServico;
        return this;
    }

    public double getValorVenda() {
        return valorVenda;
    }

    public Comissao setValorVenda(double valorVenda) {
        this.valorVenda = valorVenda;
        return this;
    }
}

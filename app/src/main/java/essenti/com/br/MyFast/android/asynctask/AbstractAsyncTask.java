package essenti.com.br.MyFast.android.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Classe abstrata para execução de tarefas assíncronas em background, ou seja, fora da
 * MAIN THREAD.
 * <br>Todas classes que executam tarefas longas devem descender desta classe e implementar
 * sua lógica de negócio.
 *
 * @author Filipe Bezerra
 * @version 14/12/2015
 */
public abstract class AbstractAsyncTask<Params, Progress, Result>
        extends AsyncTask<Params, Progress, Result> {

    protected TaskCallback<Result> mCallback;

    protected TaskException mException;

    public AbstractAsyncTask(@NonNull TaskCallback<Result> callback) {
        mCallback = callback;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mCallback.onBegin();
    }

    @Override
    protected void onPostExecute(Result result) {
        super.onPostExecute(result);
        if (mException == null) {
            if (result == null ||
                    (result instanceof List && ((List)result).isEmpty() )) {
                mCallback.onResultNothing();
            } else {
                mCallback.onSuccess(result);
            }
        } else {
            mCallback.onError(mException);
        }
    }

    @SafeVarargs
    protected final boolean checkParamsNotNull(Params... params) {
        if (params == null) {
            mException = TaskException.of(new NullPointerException("Parâmetro está nulo."));
            return false;
        }

        return true;
    }

    protected String getStreamData(InputStream is) throws IOException {
        byte[] bytes = new byte[1024];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int lidos;
        while ((lidos = is.read(bytes)) > 0) {
            baos.write(bytes, 0, lidos);
        }
        return new String(baos.toByteArray());
    }
}

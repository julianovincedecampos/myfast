package essenti.com.br.MyFast.usuario;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.InputType;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import essenti.com.br.MyFast.BuildConfig;
import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.android.activity.ActivityNavigator;
import essenti.com.br.MyFast.android.activity.BaseActivity;
import essenti.com.br.MyFast.android.asynctask.TaskCallback;
import essenti.com.br.MyFast.android.asynctask.TaskException;
import essenti.com.br.MyFast.android.widget.MensagemDialogo;
import essenti.com.br.MyFast.app.App;
import essenti.com.br.MyFast.app.ConfiguracaoActivity;
import essenti.com.br.MyFast.mesa.ListaMesas;
import essenti.com.br.MyFast.mesa.MesaService;
import essenti.com.br.MyFast.produto.GetGrupos;
import essenti.com.br.MyFast.produto.Grupo;
import essenti.com.br.MyFast.util.FeedbackHelper;
import essenti.com.br.MyFast.util.NetworkUtil;
import essenti.com.br.MyFast.webservice.ServiceGenerator;
import io.fabric.sdk.android.Fabric;
import java.util.List;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

import static essenti.com.br.MyFast.android.widget.MensagemDialogo.MENSAGEM_ALERTA;

/**
 * @author Filipe Bezerra, Diego
 * @version 1.4.0, 07/03/2016
 * @since 1.0.0
 */
public class LoginActivity extends BaseActivity {
    private static final int ERRO_AUTENTICACAO = 1;
    private static final int ERRO_CARREGAMENTO_MESAS = 2;
    private static final int ERRO_CARREGAMENTO_GRUPOS = 3;

    private Usuario mUsuarioSelecionado;

    private UsuarioService mUsuarioService;

    private MesaService mMesaService;

    private CompositeSubscription mCompositeSubscription;

    private MensagemDialogo mDialogoProgresso;

    @BindView(R.id.btnSelecioneOperador) Button mButtonSelecioneOperador;
    @BindView(R.id.txtSenha) EditText mTxtSenha;
    @BindView(R.id.imgLogoCliente) ImageView imgLogoCliente;

    private void showError(Throwable e, int tipoErro) {
        dismissDialogoProgresso();

        if (Fabric.isInitialized()) {
            Crashlytics.logException(e);
        }

        String mensagem = "Infelizmente houve uma falha ";
        String when = "";
        switch (tipoErro) {
            case ERRO_AUTENTICACAO:
                when = "ao tentar autenticar o operador.";
                break;

            case ERRO_CARREGAMENTO_MESAS:
                when = "ao tentar carregar as mesas.";
                break;

            case ERRO_CARREGAMENTO_GRUPOS:
                when = "ao tentar carregar os grupos.";
                break;
        }

        Timber.e(e, when);

        mensagem += when + "\nPor favor tente novamente. Esta falha será reportada para o "
                + "administrador do sistema!";

        App.getInstancia().exibirMensagem(this, MensagemDialogo.MENSAGEM_ERRO,
                "Atenção", mensagem);
    }

    private void showAuthenticationFailure(final String mensagem) {
        dismissDialogoProgresso();
        App.getInstancia().exibirMensagem(this, MensagemDialogo.MENSAGEM_ALERTA,
                "Atenção", mensagem);
    }

    private void showDialogoProgresso(String mensagem) {
        if (mDialogoProgresso == null) {
            mDialogoProgresso = App.getInstancia().exibirProgresso(this,
                    "Processando", mensagem);
            mDialogoProgresso.exibir();
        } else {
            mDialogoProgresso.setTexto(mensagem);

            if (!mDialogoProgresso.isExibindo()) {
                mDialogoProgresso.exibir();
            }
        }
    }

    private void dismissDialogoProgresso() {
        if (mDialogoProgresso != null) {
            if (mDialogoProgresso.isExibindo()) {
                mDialogoProgresso.esconder();
            }

            mDialogoProgresso = null;
        }
    }

    private void autenticarOperador() {
        if (mUsuarioService == null) {
            mUsuarioService = ServiceGenerator.createService(UsuarioService.class, this);
        }

        if (mUsuarioService == null) {
            App.getInstancia()
                    .exibirMensagem(this, MensagemDialogo.MENSAGEM_ALERTA,
                            "Configuração não realizada",
                            "É necessário fazer a configuração primeiramente!");
            return;
        }

        if (mUsuarioSelecionado == null) {
            App.getInstancia().exibirMensagem(this, MENSAGEM_ALERTA,
                    "Atenção", "Selecione o operador");
            return;
        }

        if (!UsuarioHelper.isValidUser(mUsuarioSelecionado)) {
            App.getInstancia().exibirMensagem(this, MENSAGEM_ALERTA,
                    "Atenção", "O operador selecionado é inválido");
            return;
        }

        if (App.getInstancia().getCodigoCardapio() <= 0) {
            App.getInstancia().exibirMensagem(this, MENSAGEM_ALERTA,
                    "Atenção", "É necessário selecionar o cardápio padrão");
            return;
        }

        String senha;
        if (mUsuarioSelecionado.getSolicitarSenha().equalsIgnoreCase("S")) {
            senha = mTxtSenha.getText().toString();
            if (TextUtils.isEmpty(senha)) {
                App.getInstancia().exibirMensagem(this, MENSAGEM_ALERTA,
                        "Atenção", "Informe a senha");
                mTxtSenha.requestFocus();
                return;
            }
        } else {
            senha = String.valueOf(mUsuarioSelecionado.getMatricula());
        }

        requestAutenticacao(senha);
    }

    private void requestAutenticacao(final String senha) {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            final Subscription subscription = mUsuarioService
                    .autenticar(mUsuarioSelecionado.getMatricula(), senha)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                            new Subscriber<ResultadoAutenticacao>() {
                                @Override
                                public void onStart() {
                                    showDialogoProgresso("Validando credenciais do operador...");
                                }

                                @Override
                                public void onError(Throwable e) {
                                    showError(e, ERRO_AUTENTICACAO);
                                }

                                @Override
                                public void onNext(ResultadoAutenticacao resultadoAutenticacao) {
                                    if ("OK".equalsIgnoreCase(resultadoAutenticacao.mensagem)) {
                                        loadMesasFromServer();
                                    } else {
                                        showAuthenticationFailure(resultadoAutenticacao.mensagem);
                                    }
                                }

                                @Override
                                public void onCompleted() {
                                }
                            }
                    );
            mCompositeSubscription.add(subscription);
        } else {
            FeedbackHelper.showOfflineMessage(this, null,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            requestAutenticacao(senha);
                        }
                    });
        }
    }

    private void loadMesasFromServer() {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            if (mMesaService == null) {
                mMesaService = ServiceGenerator.createService(MesaService.class, this);
            }

            if (mMesaService != null) {
                final Subscription subscription = mMesaService
                        .mesas(App.getInstancia().getCodigoCardapio())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(
                                new Subscriber<ListaMesas>() {
                                    @Override
                                    public void onStart() {
                                        showDialogoProgresso("Carregando as mesas...");
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        showError(e, ERRO_CARREGAMENTO_MESAS);
                                    }

                                    @Override
                                    public void onNext(ListaMesas listaMesas) {
                                        App.getInstancia().setMesas(listaMesas.mesas);
                                    }

                                    @Override
                                    public void onCompleted() {
                                        loadGruposFromServer();
                                    }
                                }
                        );
                mCompositeSubscription.add(subscription);
            }
        } else {
            FeedbackHelper.showOfflineMessage(this, null,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            loadMesasFromServer();
                        }
                    });
        }
    }

    private void loadGruposFromServer() {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
        new GetGrupos(
                new TaskCallback<List<Grupo>>() {
                    @Override
                    public void onBegin() {
                        showDialogoProgresso(
                                "Carregando os produtos disponíveis......");
                    }

                    @Override
                    public void onSuccess(List<Grupo> grupos) {
                        App.getInstancia().setGrupos(grupos);
                        prepareAndLoginUsuario();
                    }

                    @Override
                    public void onResultNothing() {
                    }

                    @Override
                    public void onError(TaskException e) {
                        showError(e, ERRO_CARREGAMENTO_GRUPOS);
                    }
                }).execute();
        } else {
            FeedbackHelper.showOfflineMessage(this, null,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            loadGruposFromServer();
                        }
                    });
        }
    }

    private void prepareAndLoginUsuario() {
        dismissDialogoProgresso();

        UsuarioHelper.login(LoginActivity.this, mUsuarioSelecionado);

        mButtonSelecioneOperador.setText(getString(R.string.selecione_operador));
        mTxtSenha.setText("");
        mUsuarioSelecionado = null;

        ActivityNavigator.startPainelActivity(this);
    }

    private void verificarConfiguracao() {
        if (TextUtils.isEmpty(App.getInstancia().getURL())) {
            abrirConfiguracoes();
        } else {
            carregarImageCliente();
        }
    }

    private void carregarImageCliente() {
        final String urlImageCliente = App.getInstancia().getUrlImageCliente();
        Picasso
                .with(this)
                .load(urlImageCliente)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .fit()
                .into(imgLogoCliente, new Callback() {
                    @Override
                    public void onSuccess() {}

                    @Override
                    public void onError() {
                        Picasso
                                .with(LoginActivity.this)
                                .load(urlImageCliente)
                                .fit()
                                .into(imgLogoCliente, new Callback() {
                                    @Override
                                    public void onSuccess() {}

                                    @Override
                                    public void onError() {
                                        Timber.e("Picasso could not fetch image from %s",
                                                urlImageCliente);
                                    }
                                });
                    }
                });
    }

    private void abrirConfiguracoes() {
        final Intent intent = new Intent(LoginActivity.this, ConfiguracaoActivity.class);

        if (BuildConfig.DEBUG) {
            startActivityForResult(intent, App.CONFIGURAR_URL);
        } else {
            new MaterialDialog.Builder(this)
                    .title("Controle de acesso")
                    .content("Digite a senha para realizar a configuração!")
                    .autoDismiss(false)
                    .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD)
                    .input("Senha de acesso", null, false, new MaterialDialog.InputCallback() {
                        @Override
                        public void onInput(@NonNull MaterialDialog dialog,
                                @NonNull CharSequence input) {
                            if (App.SENHA_CONFIRMACAO_CONFIGURACAO.equals(input.toString())) {
                                dialog.dismiss();
                                Intent intent = new Intent(LoginActivity.this,
                                        ConfiguracaoActivity.class);
                                startActivityForResult(intent, App.CONFIGURAR_URL);
                            } else {
                                App.getInstancia().exibirMensagem(LoginActivity.this,
                                        MENSAGEM_ALERTA, "Atenção",
                                        "A senha informada não é válida!");
                            }
                        }
                    }).show();
        }
    }

    private void selecionarOperador() {
        ActivityNavigator.startUsuariosActivity(this);
    }

    @Override
    protected int provideLayoutResource() {
        return R.layout.activity_login;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCompositeSubscription = new CompositeSubscription();

        App.getInstancia().inicializar(getApplicationContext());

        TextView txtVersao = ButterKnife.findById(this, R.id.txtVersao);
        try {
            ComponentName comp = new ComponentName(this, "essenti.com.br.MyFast");
            PackageInfo pinfo;
            pinfo = this.getPackageManager().getPackageInfo(comp.getPackageName(), 0);
            txtVersao.setText("Versão " + pinfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            txtVersao.setText("ND");
        }

        verificarConfiguracao();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mCompositeSubscription.isUnsubscribed()) {
            mCompositeSubscription.unsubscribe();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == App.SELECIONAR_OPERADOR && resultCode == App.OPERADOR_SELECIONADO) {
            Bundle extras = data.getExtras();
            if (extras != null && extras.containsKey(UsuariosActivity.EXTRA_OPERADOR_SELECIONADO)) {
                mUsuarioSelecionado = extras.getParcelable(
                        UsuariosActivity.EXTRA_OPERADOR_SELECIONADO);
                if (mUsuarioSelecionado != null) {
                    mButtonSelecioneOperador.setText(mUsuarioSelecionado.getNome());

                    if (mUsuarioSelecionado.getSolicitarSenha().equalsIgnoreCase("N")) {
                        autenticarOperador();
                    }
                }
            }
        } else if (requestCode == App.CONFIGURAR_URL) {
            verificarConfiguracao();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick({ R.id.btnSelecioneOperador })
    void onClickTxtOperador() {
        selecionarOperador();
    }

    @OnClick(R.id.imgConfig)
    void onClickImgConfig() {
        abrirConfiguracoes();
    }

    @OnClick(R.id.btnAutenticar)
    void onClickBtnAutenticar() {
        autenticarOperador();
    }

    @OnEditorAction(R.id.txtSenha)
    boolean onEditorActionTxtSenha(int id) {
        if (id == getResources().getInteger(R.integer.autenticar)) {
            autenticarOperador();
            return true;
        }
        return false;
    }
}


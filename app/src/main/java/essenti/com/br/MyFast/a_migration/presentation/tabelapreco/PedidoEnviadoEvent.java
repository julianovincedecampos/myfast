package essenti.com.br.MyFast.a_migration.presentation.tabelapreco;

/**
 * @author Filipe Bezerra
 */
class PedidoEnviadoEvent {
    private PedidoEnviadoEvent(){}

    public static PedidoEnviadoEvent newEvent() { return  new PedidoEnviadoEvent(); }
}

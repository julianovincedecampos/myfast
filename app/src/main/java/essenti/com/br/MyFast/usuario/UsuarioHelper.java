package essenti.com.br.MyFast.usuario;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.content.SharedPreferencesCompat;
import android.text.TextUtils;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;

public class UsuarioHelper {
    private static final String KEY_IS_USUARIO_LOGADO = "isUsuarioLogado";
    private static final String KEY_NOME_USUARIO = "nomeUsuario";
    private static final String KEY_MATRICULA_USUARIO = "matriculaUsuario";
    private static final String KEY_SOLICITA_SENHA_USUARIO = "solicitaSenhaUsuario";

    private UsuarioHelper() {}

    @SuppressLint("CommitPrefEdits")
    public static boolean login(@NonNull Context context, @NonNull Usuario usuario) {
        if (!isValidUser(usuario)) {
            return false;
        }

        SharedPreferencesCompat.EditorCompat
                .getInstance()
                .apply(getDefaultSharedPreferences(context.getApplicationContext())
                        .edit()
                        .putBoolean(KEY_IS_USUARIO_LOGADO, true)
                        .putString(KEY_NOME_USUARIO, usuario.getNome())
                        .putInt(KEY_MATRICULA_USUARIO, usuario.getMatricula())
                        .putString(KEY_SOLICITA_SENHA_USUARIO, usuario.getSolicitarSenha()));

        if (Fabric.isInitialized()) {
            Crashlytics.setUserName(usuario.getNome());
            Crashlytics.setUserIdentifier(String.valueOf(usuario.getMatricula()));
        }

        return true;
    }

    public static Usuario retrieveUsuarioLogado(@NonNull Context context) {
        if (!isUsuarioLogado(context))
            return null;

        final SharedPreferences preferences = getDefaultSharedPreferences(
                context.getApplicationContext());

        return new Usuario(
                preferences.getInt(KEY_MATRICULA_USUARIO, 0),
                preferences.getString(KEY_NOME_USUARIO, null),
                preferences.getString(KEY_SOLICITA_SENHA_USUARIO, "N"));
    }

    @SuppressLint("CommitPrefEdits")
    public static void logoutUser(@NonNull Context context) {
        if (!isUsuarioLogado(context))
            return;

        SharedPreferencesCompat.EditorCompat
                .getInstance()
                .apply(getDefaultSharedPreferences(context.getApplicationContext())
                        .edit()
                        .putBoolean(KEY_IS_USUARIO_LOGADO, false)
                        .remove(KEY_NOME_USUARIO)
                        .remove(KEY_MATRICULA_USUARIO)
                        .remove(KEY_SOLICITA_SENHA_USUARIO));
    }

    public static boolean isUsuarioLogado(@NonNull Context context) {
        final SharedPreferences preferences = getDefaultSharedPreferences(context);
        return preferences.getBoolean(KEY_IS_USUARIO_LOGADO, false);
    }

    public static boolean isValidUser(Usuario usuario) {
        return usuario != null
                && (usuario.getMatricula() != null
                && !TextUtils.isEmpty(usuario.getNome()))
                && !TextUtils.isEmpty(usuario.getSolicitarSenha());
    }
}

package essenti.com.br.MyFast.a_migration.data.cobrancas;

import android.content.Context;
import android.support.annotation.NonNull;

import static essenti.com.br.MyFast.webservice.ServiceGenerator.createService;

/**
 * @author Filipe Bezerra
 */

public class CobrancaRepositories {
    private CobrancaRepositories() {/* No instances */}

    private static CobrancaService sService = null;

    public synchronized static CobrancaService getService(
            @NonNull Context context) {
        if (sService == null) {
            sService = createService(CobrancaService.class, context.getApplicationContext());
        }
        return sService;
    }
}

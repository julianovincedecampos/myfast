package essenti.com.br.MyFast.util;

/**
 * @author Filipe Bezerra
 */

public class Preconditions {
    private Preconditions() {}

    public static <T> T checkNotNull(T reference, String errorMessage) {
        if (reference == null) {
            throw new NullPointerException(errorMessage);
        }
        return reference;
    }
}

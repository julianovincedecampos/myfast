package essenti.com.br.MyFast.usuario;

/**
 * Classe que identifica o tipo de autenticação será feita ao validar controle de acesso
 * de {@link Usuario}.
 *
 * @author Filipe Bezerra
 * @version 1.4.0, 11/04/2016
 * @since 1.4.0
 */
public enum TipoAutenticacao {
    TROCA_MESA("M"), TROCA_PRODUTOS_MESA("I");

    private String mDescricao;

    TipoAutenticacao(String descricao) {
        mDescricao = descricao;
    }

    public String getDescricao() {
        return mDescricao;
    }
}

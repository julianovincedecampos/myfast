package essenti.com.br.MyFast.mesa;

import android.net.Uri;
import android.support.annotation.NonNull;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import essenti.com.br.MyFast.android.asynctask.AbstractAsyncTask;
import essenti.com.br.MyFast.android.asynctask.TaskCallback;
import essenti.com.br.MyFast.android.asynctask.TaskException;
import essenti.com.br.MyFast.app.App;
import essenti.com.br.MyFast.produto.Produto;

/**
 * Tarefa assíncrona para transferência dos itens consumidos da mesa para outra via Web service.
 *
 * @author Filipe Bezerra
 * @version 1.4.0, 11/04/2016
 * @since 1.4.0
 */
public class PostTransferenciaItensMesa extends AbstractAsyncTask<Void, Void, String> {
    private static final String PATH_TRANSFERENCIA_ITENS_MESA = "ServiceMesa.svc/TransfereItemMesa";

    private final int mNumeroMesaOrigem;
    private final int mNumeroMesaDestino;
    private final int mMatricula;
    private final List<Produto> mProdutos;

    public PostTransferenciaItensMesa(int numeroMesaOrigem, int numeroMesaDestino, int matricula,
            List<Produto> produtos, @NonNull TaskCallback<String> callback) {
        super(callback);
        mNumeroMesaOrigem = numeroMesaOrigem;
        mNumeroMesaDestino = numeroMesaDestino;
        mMatricula = matricula;
        mProdutos = produtos;
    }

    private JSONObject convertToJson() throws JSONException {
        JSONObject json = new JSONObject();

        JSONArray produtosArray = new JSONArray();
        for (Produto produto : mProdutos) {
            JSONObject produtoJson = new JSONObject();
            produtoJson.put("Codigo", produto.getCodigo());
            produtoJson.put("Numseq", produto.getNumeroSequencia());
            produtosArray.put(produtoJson);
            produto.setQuantidade(0);
        }
        json.put("Itens", produtosArray);
        json.put("Numeroorigem", mNumeroMesaOrigem);
        json.put("Numerodestino", mNumeroMesaDestino);
        json.put("Matricula", mMatricula);

        return json;
    }

    @Override
    protected String doInBackground(Void... params) {
        final String url = Uri.parse(App.getInstancia().getURL())
                .buildUpon()
                .appendPath(PATH_TRANSFERENCIA_ITENS_MESA)
                .build()
                .toString();

        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(url);
        httppost.setHeader("Content-type", "application/json");

        try {
            JSONObject jsonObject = convertToJson();
            httppost.setEntity(new StringEntity(jsonObject.toString(), HTTP.UTF_8));

            HttpResponse response = httpclient.execute(httppost);
            HttpEntity responseEntity = response.getEntity();

            if (responseEntity != null) {
                InputStream instream = responseEntity.getContent();
                String jsonResponse = getStreamData(instream);
                instream.close();
                JSONObject objeto = new JSONObject(jsonResponse);
                final String resultado = objeto.getString("TransfereItemMesaResult");

                if (resultado.equals("OK")) {
                    return resultado;
                } else {
                    mException = TaskException.of(resultado);
                    return null;
                }
            }
        } catch (JSONException | IOException e) {
            mException = TaskException
                    .of(String.format(App.BRAZIL_LOCALE,
                            "Ocorreu um erro ao transferir itens da %d mesa para %d! Erro: %s",
                            mNumeroMesaDestino, mNumeroMesaDestino, e.getMessage()));
        }

        return null;
    }
}

package essenti.com.br.MyFast.atendimento;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;
import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.android.activity.ActivityNavigator;
import essenti.com.br.MyFast.android.activity.BaseActivity;
import essenti.com.br.MyFast.android.asynctask.TaskCallback;
import essenti.com.br.MyFast.android.asynctask.TaskException;
import essenti.com.br.MyFast.android.recyclerview.OnClickListener;
import essenti.com.br.MyFast.android.recyclerview.OnTouchListener;
import essenti.com.br.MyFast.android.widget.MensagemDialogo;
import essenti.com.br.MyFast.android.widget.MensagemDialogo.OnMensagemClickListener;
import essenti.com.br.MyFast.app.App;
import essenti.com.br.MyFast.mesa.Cliente;
import essenti.com.br.MyFast.mesa.GetClienteTask;
import essenti.com.br.MyFast.mesa.GetItensMesaTask;
import essenti.com.br.MyFast.mesa.Mesa;
import essenti.com.br.MyFast.mesa.MesaService;
import essenti.com.br.MyFast.mesa.PostPedido;
import essenti.com.br.MyFast.mesa.PostTransferenciaItensMesa;
import essenti.com.br.MyFast.mesa.ResultadoBloqueioMesa;
import essenti.com.br.MyFast.mesa.ResultadoDesbloqueioMesa;
import essenti.com.br.MyFast.mesa.ResultadoFecharMesa;
import essenti.com.br.MyFast.mesa.ResultadoObterMesa;
import essenti.com.br.MyFast.mesa.TrocaItensMesaAdapter;
import essenti.com.br.MyFast.produto.Produto;
import essenti.com.br.MyFast.usuario.AutenticacaoOperadorActivity;
import essenti.com.br.MyFast.usuario.Usuario;
import essenti.com.br.MyFast.usuario.UsuarioHelper;
import essenti.com.br.MyFast.util.FeedbackHelper;
import essenti.com.br.MyFast.util.NetworkUtil;
import essenti.com.br.MyFast.util.number.NumberUtil;
import essenti.com.br.MyFast.webservice.ServiceGenerator;
import io.fabric.sdk.android.Fabric;
import java.util.List;
import java.util.Locale;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static essenti.com.br.MyFast.android.widget.MensagemDialogo.MENSAGEM_ALERTA;
import static essenti.com.br.MyFast.android.widget.MensagemDialogo.MENSAGEM_ERRO;
import static essenti.com.br.MyFast.android.widget.MensagemDialogo.MENSAGEM_SUCESSO;
import static essenti.com.br.MyFast.app.App.PAGAMENTO_MOBILE_DESABILITADO;
import static essenti.com.br.MyFast.mesa.Mesa.MESA_EM_CONSUMO;
import static essenti.com.br.MyFast.usuario.AutenticacaoOperadorActivity.EXTRA_TIPO_AUTENTICACAO;
import static essenti.com.br.MyFast.usuario.AutenticacaoOperadorActivity.TIPO_TROCA_MESA;

/**
 * Controlador da tela de atendimento da {@link Mesa}. Esta tela apresenta os itens consumidos,
 * valores de consumo e ações comuns que um garçom desempenha.
 *
 * @author Filipe Bezerra
 * @version 1.4.2, 31/05/2016
 * @since 1.4.0
 */
public class AtendimentoMesaActivity extends BaseActivity
        implements ConfirmaPedidosDialog.ConfirmaPedidosCallback {

    private static final String TAG = AtendimentoMesaActivity.class.getSimpleName();

    private static final int CODIGO_CARDAPIO = App.getInstancia().getCodigoCardapio();

    private static final int ERRO_CARREGAMENTO_INFO_MESA = 1;
    private static final int ERRO_CARREGAMENTO_ITENS_CONSUMIDOS = 2;
    private static final int ERRO_IDENTIFICACAO_CLIENTE = 3;
    private static final int ERRO_FECHAMENTO_MESA = 4;
    private static final int ERRO_ENVIO_PEDIDO_RAPIDO = 5;
    private static final int ERRO_USUARIO_LOGADO_NULO = 6;
    private static final int ERRO_TRANSFERENCIA_ITENS_MESA = 7;
    private static final int ERRO_BLOQUEIO_MESA = 8;
    private static final int ERRO_DESBLOQUEIO_MESA = 9;

    @IntDef({
            ERRO_CARREGAMENTO_INFO_MESA,
            ERRO_CARREGAMENTO_ITENS_CONSUMIDOS,
            ERRO_IDENTIFICACAO_CLIENTE,
            ERRO_FECHAMENTO_MESA,
            ERRO_ENVIO_PEDIDO_RAPIDO,
            ERRO_USUARIO_LOGADO_NULO,
            ERRO_TRANSFERENCIA_ITENS_MESA,
            ERRO_BLOQUEIO_MESA,
            ERRO_DESBLOQUEIO_MESA
    })
    private @interface TipoError{}

    private Mesa mMesaEmAtendimento;

    private ItemConsumidoAdapter mItemConsumidoAdapter;

    private PedidoRapidoAdapter mPedidoRapidoAdapter;

    private MesaService mMesaService;

    private MensagemDialogo mDialogoProgresso;

    private boolean mViewEmModoPedidoRapido;

    private Usuario mUsuarioLogado;

    private OnTouchListener mTouchListenerItensConsumidos;

    @BindView(R.id.listViewProdutos) RecyclerView mRecyclerViewItensConsumidos;
    @BindView(R.id.txtOperador) TextView mTextViewOperador;
    @BindView(R.id.txtNumMesa) TextView mTextViewNumMesa;
    @BindView(R.id.txtNomeCliente) TextView mTextViewNomeCliente;
    @BindView(R.id.txtStatusPedido) TextView mTextViewStatusPedido;
    @BindView(R.id.txtValorTotal) TextView mTextViewValorTotal;
    @BindView(R.id.txtValorPago) TextView mTextViewValorPago;
    @BindView(R.id.txtSaldo) TextView mTextViewSaldo;
    @BindView(R.id.form_lista) LinearLayout mFormLista;
    @BindView(R.id.container_ajuda) RelativeLayout mContainerAjuda;
    @BindView(R.id.fab_confirma_pedido_rapido) FloatingActionButton mFabConfirmaPedidoRapido;
    @BindView(R.id.form_rodape) LinearLayout mFormRodape;
    @BindView(R.id.toolbar_content) RelativeLayout mToolbarContent;

    private void showExcecaoNegocio(@NonNull String mensagem,
            @Nullable OnMensagemClickListener listener) {
        if (Fabric.isInitialized()) {
            Crashlytics.log(Log.WARN, TAG, mensagem);
        }

        Timber.w(mensagem);

        App.getInstancia().exibirMensagem(this, MENSAGEM_ALERTA, "Atenção", mensagem,
                false, false, listener);
    }

    private void showError(Throwable e, @TipoError int tipoErro) {
        dismissDialogoProgresso();

        if (Fabric.isInitialized()) {
            Crashlytics.logException(e);
        }

        String mensagem = "Infelizmente houve uma falha ";
        String when = "";
        switch (tipoErro) {
            case ERRO_CARREGAMENTO_INFO_MESA:
                when = "ao carregar informações da mesa.";
                break;

            case ERRO_CARREGAMENTO_ITENS_CONSUMIDOS:
                when = "ao carregar itens consumidos.";
                break;

            case ERRO_IDENTIFICACAO_CLIENTE:
                when = "ao identificar o cliente.";
                break;

            case ERRO_FECHAMENTO_MESA:
                when = "ao fechar o consumo da mesa.";
                break;

            case ERRO_ENVIO_PEDIDO_RAPIDO:
                when = "ao enviar pedido rápido.";
                break;

            case ERRO_USUARIO_LOGADO_NULO:
                when = "Infelizmente o MyFast não conseguiu obter o usuário logado.";
                break;

            case ERRO_TRANSFERENCIA_ITENS_MESA:
                when = e.getMessage();
                break;

            case ERRO_BLOQUEIO_MESA:
                break;

            case ERRO_DESBLOQUEIO_MESA:
                break;
        }

        Timber.e(e, when);

        mensagem += when + "\nPor favor tente novamente. Esta falha será reportada para o "
                + "administrador do sistema!";

        App.getInstancia().exibirMensagem(this, MENSAGEM_ERRO, "Atenção", mensagem);
    }

    private void showDialogoProgresso(String mensagem) {
        if (mDialogoProgresso == null) {
            mDialogoProgresso = App.getInstancia().exibirProgresso(this,
                    "Processando", mensagem);
            mDialogoProgresso.exibir();
        } else {
            mDialogoProgresso.setTexto(mensagem);
        }
    }

    private void dismissDialogoProgresso() {
        if (mDialogoProgresso != null) {
            if (mDialogoProgresso.isExibindo()) {
                mDialogoProgresso.esconder();
            }

            mDialogoProgresso = null;
        }
    }

    private void displayListaItensConsumidos(List<Produto> itensMesa) {
        if (itensMesa == null || itensMesa.isEmpty()) {
            if (mItemConsumidoAdapter != null) {
                mItemConsumidoAdapter.clearData();
            }

            mFormLista.setVisibility(View.GONE);
            mContainerAjuda.setVisibility(View.VISIBLE);
        } else {
            if (mItemConsumidoAdapter == null) {
                mRecyclerViewItensConsumidos.setAdapter(
                        mItemConsumidoAdapter = new ItemConsumidoAdapter(itensMesa));
            } else {
                mItemConsumidoAdapter.swapItems(itensMesa);
            }

            mFormLista.setVisibility(View.VISIBLE);
            mContainerAjuda.setVisibility(View.GONE);
        }
    }

    private void requestFechamentoMesa() {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            initMesaService();
            if (mMesaService != null) {
                mMesaService.fecharMesa(mMesaEmAtendimento.getNumero(), CODIGO_CARDAPIO)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(
                                new Subscriber<ResultadoFecharMesa>() {
                                    @Override
                                    public void onStart() {
                                        showDialogoProgresso("Fechando o consumo da mesa...");
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        showError(e, ERRO_FECHAMENTO_MESA);
                                    }

                                    @Override
                                    public void onNext(ResultadoFecharMesa resultado) {
                                        dismissDialogoProgresso();
                                        if (resultado.mensagem.equalsIgnoreCase("OK")) {
                                            App.getInstancia()
                                                    .exibirMensagem(AtendimentoMesaActivity.this,
                                                            MENSAGEM_SUCESSO,
                                                            "Atenção", "Mesa em fechamento!",
                                                            new OnMensagemClickListener() {
                                                                @Override
                                                                public void onConfirmarClick() {
                                                                    requestInformacoesMesa();
                                                                }

                                                                @Override
                                                                public void onCancelarClick() {
                                                                }
                                                            });
                                        } else {
                                            showExcecaoNegocio(resultado.mensagem, null);
                                        }
                                    }

                                    @Override
                                    public void onCompleted() {
                                    }
                                }
                        );
            }
        } else {
            FeedbackHelper.showOfflineMessage(this, null,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            requestFechamentoMesa();
                        }
                    });
        }
    }

    private void displayOpcoesTroca() {
        if (mMesaEmAtendimento.getStatusMesa() == Mesa.MESA_DISPONIVEL) {
            App.getInstancia().exibirMensagem(this, MENSAGEM_ALERTA, "Atenção",
                    "Não é possível continuar, esta mesa ainda não tem consumo!");
            return;
        } else if (mMesaEmAtendimento.getStatusMesa() == Mesa.MESA_EM_FECHAMENTO) {
            App.getInstancia().exibirMensagem(this, MENSAGEM_ALERTA, "Atenção",
                    "Não é possível continuar, esta mesa está em fechamento!");
            return;
        }

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.dialog_selecionar_troca, null);

        final ImageButton btnConfirmar = ButterKnife.findById(view, R.id.btnConfirmar);
        ImageView imgFechar = ButterKnife.findById(view, R.id.imgFechar);
        final RadioGroup rdgOpcoes = ButterKnife.findById(view, R.id.rdgOpcoes);

        final RadioButton rdbTrocarMesa = ButterKnife.findById(view, R.id.rdbTrocarMesa);
        final RadioButton rdbTrocarProdutosMesa = ButterKnife.findById(view,
                R.id.rdbTrocarProdutosMesa);

        final CompoundButton.OnCheckedChangeListener radioButtonCheckedListener =
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked && !btnConfirmar.isEnabled()) {
                            btnConfirmar.setEnabled(true);
                        }
                    }
                };
        rdbTrocarMesa.setOnCheckedChangeListener(radioButtonCheckedListener);
        rdbTrocarProdutosMesa.setOnCheckedChangeListener(radioButtonCheckedListener);

        builder.setView(view);
        final AlertDialog dialog = builder.create();

        imgFechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnConfirmar.setEnabled(false);
        btnConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                int tipoAutenticacao;

                if (rdgOpcoes.getCheckedRadioButtonId() == rdbTrocarMesa.getId()) {
                    tipoAutenticacao = TIPO_TROCA_MESA;
                } else {
                    tipoAutenticacao = AutenticacaoOperadorActivity.TIPO_TROCA_PRODUTOS_MESA;
                }

                ActivityNavigator
                        .startAutenticacaoOperadorActivity(AtendimentoMesaActivity.this,
                                tipoAutenticacao, mMesaEmAtendimento);
            }
        });
        dialog.show();
    }

    private void requestTransferenciaItensMesa(final int numeroMesaOrigem, final int numeroMesaDestino,
            final List<Produto> produtos) {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            new PostTransferenciaItensMesa(numeroMesaOrigem, numeroMesaDestino,
                    mUsuarioLogado.getMatricula(), produtos, new TaskCallback<String>() {
                @Override
                public void onBegin() {
                    showDialogoProgresso("Trocando itens da mesa...");
                }

                @Override
                public void onSuccess(String result) {
                    dismissDialogoProgresso();
                    requestInformacoesMesa();
                    requestItensConsumidos();
                }

                @Override
                public void onResultNothing() {
                    dismissDialogoProgresso();
                }

                @Override
                public void onError(TaskException e) {
                    showError(e, ERRO_TRANSFERENCIA_ITENS_MESA);
                }
            }).execute();
        } else {
            FeedbackHelper.showOfflineMessage(this, null,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            requestTransferenciaItensMesa(
                                    numeroMesaOrigem, numeroMesaDestino, produtos);
                        }
                    });
        }
    }

    private void displayOpcoesTrocaProdutosMesa() {
        final ArrayAdapter<Mesa> mesaArrayAdapter = new ArrayAdapter<>(this, R.layout.spinner_mesa);

        for (int i = 0; i < App.getInstancia().getMesas().size(); i++) {
            Mesa mesaDestino = App.getInstancia().getMesas().get(i);
            if (!mesaDestino.equals(mMesaEmAtendimento)
                    && mesaDestino.isMesaDisponivelOuEmConsumo()) {
                mesaArrayAdapter.add(mesaDestino);
            }
        }

        if (!mesaArrayAdapter.isEmpty()) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            View view = getLayoutInflater().inflate(R.layout.dialog_trocar_produto_mesa, null);

            final ImageButton btnConfirmar = ButterKnife.findById(view, R.id.btnConfirmar);
            final ImageView imgFechar = ButterKnife.findById(view, R.id.imgFechar);

            final EditText edtMesaDestino = ButterKnife.findById(view, R.id.edtMesaDestino);

            final ListView listViewItens = ButterKnife.findById(view, R.id.listViewItens);
            final TrocaItensMesaAdapter adapter = new TrocaItensMesaAdapter(this,
                    mItemConsumidoAdapter.getItensConsumidos());
            listViewItens.setAdapter(adapter);

            builder.setView(view);
            final AlertDialog dialog = builder.create();

            imgFechar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            btnConfirmar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (TextUtils.isEmpty(edtMesaDestino.getText())) {
                        App.getInstancia().exibirMensagem(AtendimentoMesaActivity.this,
                                MENSAGEM_ALERTA, "Atenção", "Informe a mesa de destino!");
                        edtMesaDestino.requestFocus();
                        return;
                    }

                    final int numeroMesa = Integer.parseInt(edtMesaDestino.getText().toString());
                    Mesa mesaDestino = null;

                    for (Mesa mesa : App.getInstancia().getMesas()) {
                        if (mesa.getNumero() == numeroMesa) {
                            mesaDestino = mesa;
                            break;
                        }
                    }

                    if (mesaDestino == null) {
                        App.getInstancia().exibirMensagem(AtendimentoMesaActivity.this,
                                MENSAGEM_ALERTA, "Atenção",
                                "O número da mesa informada não existe!");
                        edtMesaDestino.requestFocus();
                        return;
                    }

                    if (numeroMesa != -1) {
                        if (mesaDestino.isMesaDisponivelOuEmConsumo()) {
                            if (!adapter.getSelecionados().isEmpty()) {
                                dialog.dismiss();
                                requestTransferenciaItensMesa(mMesaEmAtendimento.getNumero(),
                                        mesaDestino.getNumero(), adapter.getSelecionados());
                            } else {
                                App.getInstancia()
                                        .exibirMensagem(
                                                AtendimentoMesaActivity.this, MENSAGEM_ALERTA,
                                                "Atenção", "Não foi selecionado nenhum produto!");
                            }
                        } else {
                            App.getInstancia()
                                    .exibirMensagem(
                                            AtendimentoMesaActivity.this, MENSAGEM_ALERTA,
                                            "Atenção", "Só é permitido trocar para uma mesa " +
                                                    "disponível ou em consumo!");
                        }
                    } else {
                        App.getInstancia()
                                .exibirMensagem(
                                        AtendimentoMesaActivity.this, MENSAGEM_ALERTA,
                                        "Atenção", "Selecione a mesa de destino!");
                    }
                }
            });
            dialog.show();
        }
    }

    private void requestInformacoesMesa() {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            initMesaService();
            if (mMesaService != null) {
                mMesaService.obterMesa(mMesaEmAtendimento.getNumero(), CODIGO_CARDAPIO)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(
                                new Subscriber<ResultadoObterMesa>() {
                                    @Override
                                    public void onStart() {
                                        showDialogoProgresso("Atualizando informações da mesa...");
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        showError(e, ERRO_CARREGAMENTO_INFO_MESA);
                                    }

                                    @Override
                                    public void onNext(ResultadoObterMesa resultado) {
                                        mMesaEmAtendimento = resultado.mesa;

                                        mTextViewNomeCliente.setText(
                                                mMesaEmAtendimento.getNomeCliente());
                                        mTextViewValorTotal.setText(NumberUtil.formatAsCurrency(
                                                mMesaEmAtendimento.getValorTotal()));
                                        mTextViewValorPago.setText(NumberUtil.formatAsCurrency(
                                                mMesaEmAtendimento.getValorPago()));
                                        mTextViewSaldo.setText(NumberUtil.formatAsCurrency(
                                                mMesaEmAtendimento.getValorTotal()));
                                        mTextViewStatusPedido.setText(
                                                mMesaEmAtendimento.getStatus());

                                        prepareViewForStatusMesa();
                                        invalidateOptionsMenu();
                                    }

                                    @Override
                                    public void onCompleted() {
                                        dismissDialogoProgresso();
                                    }
                                }
                        );
            }
        } else {
            FeedbackHelper.showOfflineMessage(this, null,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            requestInformacoesMesa();
                        }
                    });
        }
    }

    private void requestItensConsumidos() {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            new GetItensMesaTask(
                    new TaskCallback<List<Produto>>() {
                        @Override
                        public void onBegin() {
                            showDialogoProgresso("Carregando os itens consumidos da mesa...");
                        }

                        @Override
                        public void onSuccess(List<Produto> result) {
                            dismissDialogoProgresso();
                            displayListaItensConsumidos(result);
                        }

                        @Override
                        public void onResultNothing() {
                            dismissDialogoProgresso();
                            if (!mMesaEmAtendimento.isMesaEmFechamento()) {
                                mFormLista.setVisibility(View.GONE);
                                mContainerAjuda.setVisibility(View.VISIBLE);
                            }
                        }

                        @Override
                        public void onError(TaskException e) {
                            showError(e, ERRO_CARREGAMENTO_ITENS_CONSUMIDOS);
                        }
                    }
            ).execute(mMesaEmAtendimento.getNumero());
        } else {
            FeedbackHelper.showOfflineMessage(this, null,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            requestItensConsumidos();
                        }
                    });
        }
    }

    private void requestCliente(final String cpf, final MaterialDialog dialogoCpf) {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            new GetClienteTask(cpf, mMesaEmAtendimento.getNumero(),
                    new TaskCallback<Cliente>() {
                        @Override
                        public void onBegin() {
                            showDialogoProgresso("Identificando cliente...");
                        }

                        @Override
                        public void onSuccess(Cliente cliente) {
                            dismissDialogoProgresso();
                            dialogoCpf.dismiss();
                            mMesaEmAtendimento.setCliente(cliente);
                            mTextViewNomeCliente.setText(cliente.getNome());
                            mFormLista.setVisibility(View.VISIBLE);
                            mContainerAjuda.setVisibility(View.GONE);
                        }

                        @Override
                        public void onResultNothing() {
                            dismissDialogoProgresso();
                            App.getInstancia()
                                    .exibirMensagem(AtendimentoMesaActivity.this, MENSAGEM_ALERTA,
                                            "Atenção",
                                            "Não foi possível localizar um cliente com este cpf!");
                        }

                        @Override
                        public void onError(TaskException e) {
                            showError(e, ERRO_IDENTIFICACAO_CLIENTE);
                        }
                    })
                    .execute(cpf);
        } else {
            FeedbackHelper.showOfflineMessage(this, null,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            requestCliente(cpf, dialogoCpf);
                        }
                    });
        }
    }

    private void setModoPedidoRapido(boolean habilitado) {
        if (mViewEmModoPedidoRapido != habilitado) {
            mViewEmModoPedidoRapido = habilitado;

            if (mViewEmModoPedidoRapido) {
                mFabConfirmaPedidoRapido.setVisibility(View.VISIBLE);
                mFormRodape.setVisibility(View.GONE);

                if (mToolbarActionbar != null) {
                    mToolbarContent.setVisibility(View.GONE);
                    mToolbarActionbar.getMenu()
                            .findItem(R.id.menu_trocar).setVisible(false);
                    mToolbarActionbar.getMenu()
                            .findItem(R.id.menu_produtos_avulsos).setVisible(false);
                    mToolbarActionbar.getMenu()
                            .findItem(R.id.menu_identificar_cliente).setVisible(false);
                }

                mPedidoRapidoAdapter = new PedidoRapidoAdapter(this,
                        mItemConsumidoAdapter.getItensConsumidos());
                mRecyclerViewItensConsumidos.setAdapter(mPedidoRapidoAdapter);
            } else {
                mFabConfirmaPedidoRapido.setVisibility(View.GONE);
                mFormRodape.setVisibility(View.VISIBLE);

                if (mToolbarActionbar != null) {
                    mToolbarContent.setVisibility(View.VISIBLE);
                    mToolbarActionbar.getMenu()
                            .findItem(R.id.menu_trocar).setVisible(true);
                    mToolbarActionbar.getMenu()
                            .findItem(R.id.menu_produtos_avulsos).setVisible(true);
                    mToolbarActionbar.getMenu()
                            .findItem(R.id.menu_identificar_cliente).setVisible(true);
                }

                mRecyclerViewItensConsumidos.setAdapter(mItemConsumidoAdapter);
            }
        }
    }

    private void initMesaService() {
        if (mMesaService == null) {
            mMesaService = ServiceGenerator.createService(MesaService.class, this);
        }
    }

    private void requestBloqueioMesa() {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            initMesaService();
            if (mMesaService != null) {
                mMesaService
                        .bloquearMesa(
                                mMesaEmAtendimento.getNumero(),
                                mUsuarioLogado.getMatricula(),
                                App.getInstancia().getCodigoCardapio()
                        )
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Subscriber<ResultadoBloqueioMesa>() {
                                    @Override
                                    public void onError(Throwable e) {
                                        showError(e, ERRO_BLOQUEIO_MESA);
                                    }

                                    @Override
                                    public void onNext(ResultadoBloqueioMesa resultado) {
                                        if (resultado.mensagem.equalsIgnoreCase("OK")) {
                                            setModoPedidoRapido(true);
                                        } else {
                                            showExcecaoNegocio(resultado.mensagem, null);
                                        }
                                    }

                                    @Override
                                    public void onCompleted() {
                                    }
                                }
                        );
            }
        } else {
            FeedbackHelper.showOfflineMessage(this,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            finish();
                        }
                    },
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            requestBloqueioMesa();
                        }
                    });
        }
    }

    private void requestDesbloqueioMesa() {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            initMesaService();
            if (mMesaService != null) {
                mMesaService
                        .desbloquearMesa(
                                mMesaEmAtendimento.getNumero(),
                                mUsuarioLogado.getMatricula(),
                                App.getInstancia().getCodigoCardapio()
                        )
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Subscriber<ResultadoDesbloqueioMesa>() {
                                    @Override
                                    public void onError(Throwable e) {
                                        showError(e, ERRO_DESBLOQUEIO_MESA);
                                    }

                                    @Override
                                    public void onNext(ResultadoDesbloqueioMesa resultado) {
                                        if (resultado.mensagem.equalsIgnoreCase("OK")) {
                                            setModoPedidoRapido(false);
                                        } else {
                                            showExcecaoNegocio(resultado.mensagem, null);
                                        }
                                    }

                                    @Override
                                    public void onCompleted() {
                                    }
                                }
                        );
            }
        } else {
            FeedbackHelper.showOfflineMessage(this, null,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            requestDesbloqueioMesa();
                        }
                    });
        }
    }

    private void sendPedidoRapido(List<Produto> produtosPedido, String outraMatriculaGarcom) {
        if (TextUtils.isEmpty(outraMatriculaGarcom)) {
            outraMatriculaGarcom = mUsuarioLogado != null ?
                    String.valueOf(mUsuarioLogado.getMatricula()) : "";
        }

        if (TextUtils.isEmpty(outraMatriculaGarcom)) {
            if (Fabric.isInitialized()) {
                Crashlytics.log(Log.INFO, TAG, "Não foi encontrado usuário logado no aplicativo.");
            }

            showError(new NullPointerException("Usuário logado retornou nulo"),
                    ERRO_USUARIO_LOGADO_NULO);
            return;
        }

        requestEnvioPedidoRapido(produtosPedido, outraMatriculaGarcom);
    }

    private void requestEnvioPedidoRapido(final List<Produto> produtosPedido,
            final String outraMatriculaGarcom) {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            new PostPedido(
                    false,
                    mMesaEmAtendimento,
                    outraMatriculaGarcom,
                    produtosPedido,
                    new TaskCallback<String>() {
                        @Override
                        public void onBegin() {
                            showDialogoProgresso("Enviando pedido rápido...");
                        }

                        @Override
                        public void onSuccess(String result) {
                            dismissDialogoProgresso();
                            App.getInstancia()
                                    .exibirMensagem(AtendimentoMesaActivity.this, MENSAGEM_SUCESSO,
                                            "Atenção", "Produtos adicionados com sucesso na mesa!",
                                            new OnMensagemClickListener() {
                                                @Override
                                                public void onConfirmarClick() {
                                                    requestDesbloqueioMesa();
                                                    requestInformacoesMesa();
                                                    requestItensConsumidos();
                                                }

                                                @Override
                                                public void onCancelarClick() {
                                                }
                                            });
                        }

                        @Override
                        public void onResultNothing() {}

                        @Override
                        public void onError(TaskException e) {
                            showError(e, ERRO_ENVIO_PEDIDO_RAPIDO);
                        }
                    }
            ).execute();
        } else {
            FeedbackHelper.showOfflineMessage(this, null,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            requestEnvioPedidoRapido(produtosPedido, outraMatriculaGarcom);
                        }
                    });
        }
    }

    private void prepareViewForStatusMesa() {
        if (mMesaEmAtendimento.isMesaEmFechamento()) {
            mRecyclerViewItensConsumidos.setLongClickable(false);
            if (mTouchListenerItensConsumidos != null) {
                mRecyclerViewItensConsumidos
                        .removeOnItemTouchListener(mTouchListenerItensConsumidos);
            }

            ButterKnife.findById(this, R.id.btnAdicionar).setEnabled(false);

            if (App.getInstancia().getIndicePagamentoMobile() == PAGAMENTO_MOBILE_DESABILITADO) {
                ButterKnife.findById(this, R.id.buttonbar).setVisibility(View.GONE);
            } else {
                final Button btnFinalizar = ButterKnife.findById(this, R.id.btnFinalizar);
                btnFinalizar.setText(R.string.title_button_pagar_conta);
            }
        } else {
            mRecyclerViewItensConsumidos.setLongClickable(true);

            if (mTouchListenerItensConsumidos == null) {
                mTouchListenerItensConsumidos = new OnTouchListener(this,
                        mRecyclerViewItensConsumidos, new OnClickListener() {
                    @Override
                    public void onSingleTapUp(View view, int position) {
                    }

                    @Override
                    public void onLongPress(View view, int position) {
                        requestBloqueioMesa();
                    }
                });
                mRecyclerViewItensConsumidos
                        .addOnItemTouchListener(mTouchListenerItensConsumidos);

                if (mMesaEmAtendimento.getStatusMesa() == Mesa.MESA_DISPONIVEL) {
                    mFormLista.setVisibility(View.GONE);
                    mContainerAjuda.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    protected int provideLayoutResource() {
        return R.layout.activity_atendimento_mesa;
    }

    @Override
    protected int provideUpIndicator() {
        return R.drawable.ic_arrow_back_white_24dp;
    }

    @Override
    protected boolean shouldDisplayHomeAsUpEnabled() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!getIntent().hasExtra(App.EXTRA_MESA_ATENDIMENTO)) {
            Toast.makeText(getApplicationContext(), "A mesa em atendimento não foi "
                            + "passada via Intent.putExtra(App.EXTRA_MESA_ATENDIMENTO, Parcelable)",
                    Toast.LENGTH_LONG).show();
            finish();
        }

        Timber.tag(TAG);

        mMesaEmAtendimento = getIntent().getParcelableExtra(App.EXTRA_MESA_ATENDIMENTO);

        mUsuarioLogado = UsuarioHelper.retrieveUsuarioLogado(this);
        if (mUsuarioLogado != null) {
            mTextViewOperador.setText(mUsuarioLogado.getNome());
        } else {
            if (Fabric.isInitialized()) {
                Crashlytics.log(Log.INFO, TAG, "Não foi encontrado usuário logado no aplicativo.");
            }
        }

        mTextViewNumMesa.setText(
                String.format(Locale.getDefault(), "%03d", mMesaEmAtendimento.getNumero()));
        mTextViewNomeCliente.setText(mMesaEmAtendimento.getNomeCliente());
        mTextViewStatusPedido.setText(mMesaEmAtendimento.getStatus());
        mTextViewValorTotal.setText(
                NumberUtil.formatAsCurrency(mMesaEmAtendimento.getValorTotal()));
        mTextViewValorPago.setText(NumberUtil.formatAsCurrency(mMesaEmAtendimento.getValorPago()));
        mTextViewSaldo.setText(NumberUtil.formatAsCurrency(mMesaEmAtendimento.getSaldo()));

        mRecyclerViewItensConsumidos.setHasFixedSize(true);
        mRecyclerViewItensConsumidos.setLayoutManager(new LinearLayoutManager(this));

        prepareViewForStatusMesa();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (mItemConsumidoAdapter == null && mMesaEmAtendimento.hasConsumo()) {
            requestItensConsumidos();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case App.AUTENTICAR_OPERADOR:
                if (resultCode == RESULT_OK
                        && data.getExtras().containsKey(EXTRA_TIPO_AUTENTICACAO)) {
                    final int tipoAutenticacao = data.getIntExtra(EXTRA_TIPO_AUTENTICACAO,
                            TIPO_TROCA_MESA);

                    if (tipoAutenticacao == TIPO_TROCA_MESA) {
                        requestInformacoesMesa();
                        requestItensConsumidos();

                        mFormLista.setVisibility(View.VISIBLE);
                        mContainerAjuda.setVisibility(View.GONE);
                        break;
                    } else {
                        displayOpcoesTrocaProdutosMesa();
                        break;
                    }
                } else {
                    Toast.makeText(this, "Autenticação cancelada.", Toast.LENGTH_SHORT).show();
                }
                break;

            case App.ADICIONAR_PRODUTO:
            case App.ADICIONAR_PRODUTO_AVULSO:
                if (resultCode == App.PRODUTO_ADICIONADO
                        || resultCode == App.PRODUTO_AVULSO_ADICIONADO) {
                    requestInformacoesMesa();
                    requestItensConsumidos();

                    mFormLista.setVisibility(View.VISIBLE);
                    mContainerAjuda.setVisibility(View.GONE);
                }
                break;

            case App.REQUEST_PAGAMENTO_CARTAO:
                if (resultCode == App.RESULT_PAGAMENTO_EFETUADO) {
                    requestInformacoesMesa();
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.fab_confirma_pedido_rapido)
    void onClickFabConfirmaPedidoRapido() {
        if (!mViewEmModoPedidoRapido) {
            return;
        }

        final List<Produto> lista = mPedidoRapidoAdapter.getProdutosSelecionados();

        if (lista == null || lista.isEmpty()) {
            App.getInstancia().exibirMensagem(this, MENSAGEM_ALERTA, "Atenção",
                    "Nenhum produto foi selecionado!");
            return;
        }

        new ConfirmaPedidosDialog(this, lista)
                .callback(this)
                .show();
    }

    @OnClick(R.id.btnAdicionar)
    void onClickBtnAdicionar() {
        if (mMesaEmAtendimento.isMesaDisponivelOuEmConsumo()) {
            ActivityNavigator.startProdutosActivity(this, mMesaEmAtendimento);
        } else {
            App.getInstancia().exibirMensagem(this, MENSAGEM_ALERTA, "Atenção",
                    "Só é permitido inserir novos produtos em mesas disponíveis "
                            + "ou em mesas que já estão em consumo!");
        }
    }

    @OnClick(R.id.btnFinalizar)
    void onClickBtnFinalizar() {
        if (mMesaEmAtendimento.isMesaEmFechamento()) {
            ActivityNavigator.startPagamentoCartaoActivity(this, mMesaEmAtendimento, false);
        } else if (mMesaEmAtendimento.getStatusMesa() == MESA_EM_CONSUMO) {
            App.getInstancia().exibirQuestionamento(this, "Atenção",
                    "Tem certeza que deseja fechar a conta desta mesa?",
                    new OnMensagemClickListener() {
                        @Override
                        public void onConfirmarClick() {
                            requestFechamentoMesa();
                        }

                        @Override
                        public void onCancelarClick() {
                        }
                    });
        } else {
            App.getInstancia().exibirMensagem(this, MENSAGEM_ALERTA, "Atenção",
                    "Esta mesa não está em consumo!");
        }
    }

    @OnClick(R.id.btnIdentificarCliente)
    void onClickBtnIdentificarCliente() {
        new MaterialDialog.Builder(this)
                .title("Identificando cliente")
                .content("Digite o cpf do cliente!")
                .autoDismiss(false)
                .inputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED)
                .input("CPF", "", false, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull MaterialDialog d, @NonNull CharSequence input) {
                        if (TextUtils.isDigitsOnly(input.toString())) {
                            requestCliente(input.toString(), d);
                        } else {
                            d.setContent("CPF inválido. Deve conter apenas números!");
                        }
                    }
                })
                .show();
    }

    @Override
    public void onBackPressed() {
        if (mViewEmModoPedidoRapido) {
            requestDesbloqueioMesa();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_atendimento_mesa, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.menu_trocar)
                .setEnabled(!mMesaEmAtendimento.isMesaEmFechamento());
        menu.findItem(R.id.menu_produtos_avulsos)
                .setEnabled(!mMesaEmAtendimento.isMesaEmFechamento());
        menu.findItem(R.id.menu_identificar_cliente)
                .setEnabled(!mMesaEmAtendimento.isMesaEmFechamento());
        menu.findItem(R.id.menu_pagamento_parcial)
                .setVisible(mMesaEmAtendimento.getStatusMesa() == MESA_EM_CONSUMO
                        && App.getInstancia().isPagamentoMobileHabilitado());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.menu_trocar:
                displayOpcoesTroca();
                return true;

            case R.id.menu_produtos_avulsos:
                if (mMesaEmAtendimento.isMesaDisponivelOuEmConsumo()) {
                    ActivityNavigator.startProdutosAvulsosActivity(this, mMesaEmAtendimento);
                } else {
                    App.getInstancia()
                            .exibirMensagem(this, MENSAGEM_ALERTA,
                                    "Atenção", "Só é permitido inserir novos produtos "
                                            + "em mesas disponível ou em mesas que já "
                                            + "estão em consumo!");
                }
                return true;

            case R.id.menu_identificar_cliente:
                onClickBtnIdentificarCliente();
                return true;

            case R.id.menu_pagamento_parcial:
                ActivityNavigator.startPagamentoCartaoActivity(this, mMesaEmAtendimento, true);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void finish() {
        setResult(App.ATENDIMENTO_MESA,
                new Intent().putExtra(App.EXTRA_MESA_ATENDIMENTO, mMesaEmAtendimento));
        super.finish();
    }

    @Override
    public void onPedidoConfirmado(String matriculaGarcom) {
        List<Produto> produtosSelecionados =
                mPedidoRapidoAdapter.getProdutosSelecionados();

        if (produtosSelecionados.isEmpty()) {
            App.getInstancia()
                    .exibirMensagem(AtendimentoMesaActivity.this, MENSAGEM_ALERTA,
                            "Atenção", "Todos itens foram removidos!");
            return;
        }

        sendPedidoRapido(produtosSelecionados, matriculaGarcom);
    }

    @Override
    public void onItemPedidoCancelado(int posicao) {
        if (!mViewEmModoPedidoRapido) {
            return;
        }

        mPedidoRapidoAdapter.removerProdutoSelecionado(posicao);
    }
}

package essenti.com.br.MyFast.ambiente;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import essenti.com.br.MyFast.R;
import java.util.List;

/**
 * @author Diego, Filipe Bezerra
 * @version 1.4.0, 16/03/2016
 * @since 1.0.0, 04/10/2015
 */
public class AmbienteAdapter extends ArrayAdapter<Ambiente> {
    private LayoutInflater mInflater;

    public AmbienteAdapter(Context context, List<Ambiente> objects) {
        super(context, 0, objects);
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getCodigo();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_lista_ambientes, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtNomeAmbiente.setText(getItem(position).getDescricao());
        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.txtNomeAmbiente) TextView txtNomeAmbiente;

        public ViewHolder(View itemView) {
            ButterKnife.bind(this, itemView);
        }
    }
}

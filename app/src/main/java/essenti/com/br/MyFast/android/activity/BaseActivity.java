package essenti.com.br.MyFast.android.activity;

import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import butterknife.BindView;
import butterknife.ButterKnife;
import essenti.com.br.MyFast.R;

/**
 * Activity base para todas {@link android.app.Activity}s do aplicativo.
 *
 * @author Filipe Bezerra
 */
public abstract class BaseActivity extends AppCompatActivity {
    protected static final int NO_UP_INDICATOR = -1;

    @Nullable @BindView(R.id.toolbar) protected Toolbar mToolbarActionbar;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(provideLayoutResource());
        ButterKnife.bind(this);
        setupToolbarAsActionBar();
    }

    private void setupToolbarAsActionBar() {
        if (mToolbarActionbar != null) {
            setSupportActionBar(mToolbarActionbar);
            final ActionBar actionBar = getSupportActionBar();

            if (actionBar != null && shouldDisplayHomeAsUpEnabled()) {
                actionBar.setDisplayHomeAsUpEnabled(true);

                if (provideUpIndicator() != NO_UP_INDICATOR) {
                    actionBar.setHomeAsUpIndicator(provideUpIndicator());
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    protected boolean shouldDisplayHomeAsUpEnabled() {
        return false;
    }

    @DrawableRes protected int provideUpIndicator() {
        return NO_UP_INDICATOR;
    }

    @LayoutRes protected abstract int provideLayoutResource();
}

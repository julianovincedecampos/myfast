package essenti.com.br.MyFast.usuario;

import com.google.gson.annotations.SerializedName;

/**
 * .
 *
 * @author Filipe Bezerra
 * @version 1.4.0, 27/03/2016
 * @since 1.4.0
 */
public class ResultadoAutenticacao {
    @SerializedName("AutenticarResult")
    public String mensagem;
}

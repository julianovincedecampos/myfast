package essenti.com.br.MyFast.a_migration.presentation.tabelapreco;

import essenti.com.br.MyFast.produto.Produto;
import java.util.List;

/**
 * @author Filipe Bezerra
 */
class ProdutosSelecionadosEvent {
    private final List<Produto> mProdutos;

    private ProdutosSelecionadosEvent(List<Produto> pProdutos) {
        mProdutos = pProdutos;
    }

    public static ProdutosSelecionadosEvent newEvent(List<Produto> pProdutos) {
        return new ProdutosSelecionadosEvent(pProdutos);
    }

    public List<Produto> getProdutos() {
        return mProdutos;
    }
}

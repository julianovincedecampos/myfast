package essenti.com.br.MyFast.produto;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import essenti.com.br.MyFast.R;
import timber.log.Timber;

import static essenti.com.br.MyFast.util.log.LogUtil.obtainPrettyPrintingOf;

/**
 * Adaptador da lista de {@link Complemento}s de um {@link Produto}.
 *
 * @author Filipe Bezerra
 * @version 1.2.2, 06/01/2016
 * @since 1.0.0
 */
public class ComplementoAdapter extends BaseAdapter {
    private static final String LOG = ComplementoAdapter.class.getSimpleName();

    private final LayoutInflater mInflater;
    private final List<Complemento> complementos;
    private final List<Complemento> complementosSelecionados;

    public ComplementoAdapter(Context context, List<Complemento> complementos) {
        Timber.i("Adaptador de complementos recebeu %d itens\n%s",
                complementos.size(), obtainPrettyPrintingOf(complementos));
        this.complementos = complementos;
        mInflater = LayoutInflater.from(context);
        complementosSelecionados = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return this.complementos.size();
    }

    @Override
    public Object getItem(int position) {
        return this.complementos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return this.complementos.get(position).getCodigo();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_lista_complemento, null);
        }
        final Complemento complemento = complementos.get(position);

        ImageButton btnAdicionar = (ImageButton) convertView.findViewById(R.id.btnAdicionarItem);
        ImageButton btnRemover = (ImageButton) convertView.findViewById(R.id.btnRemoverItem);
        btnAdicionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                complemento.setQuantidade(complemento.getQuantidade() + 1);
                Timber.i("Adicionado 1 unidade do complemento %s, total é %d",
                        complemento.getDescricao(), complemento.getQuantidade());

                if (!complementosSelecionados.contains(complemento)) {
                    complementosSelecionados.add(complemento);
                    Timber.i("O complemento %s foi selecionado", complemento.getDescricao());
                }
                notifyDataSetChanged();
            }
        });

        btnRemover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (complemento.getQuantidade() > 0) {
                    complemento.setQuantidade(complemento.getQuantidade() - 1);
                    Timber.i("Retirado 1 unidade do complemento %s, total é %d",
                            complemento.getDescricao(), complemento.getQuantidade());

                    if (complemento.getQuantidade() == 0) {
                        complementosSelecionados.remove(complemento);
                        Timber.i("O complemento %s foi deselecionado", complemento.getDescricao());
                    }
                    notifyDataSetChanged();
                } else {
                    Timber.d("Não é possível remover, o complemento %s não foi selecionado",
                            complemento.getDescricao());
                }
            }
        });

        TextView txtTotalItem = (TextView) convertView.findViewById(R.id.txtTotalItem);
        TextView txtValorUnitario = (TextView) convertView.findViewById(R.id.txtValorUnitario);
        TextView txtValorTotalItem = (TextView) convertView.findViewById(R.id.txtValorTotalItem);
        txtTotalItem.setText(String.format("%03d", complemento.getQuantidade()));
        TextView txtDescricaoItem = (TextView) convertView.findViewById(R.id.txtDescricaoItem);

        txtDescricaoItem.setText(complemento.getDescricao());
        txtValorUnitario.setText(String.format("Valor Unit. %s",
                NumberFormat.getCurrencyInstance().format(complemento.getValorUnitario())));
        txtValorTotalItem.setText(
                NumberFormat.getCurrencyInstance().format(complemento.getValorTotal()));

        return convertView;
    }

    public boolean hasComplementoSelecionado() {
        boolean itHas = !complementosSelecionados.isEmpty();
        Timber.i("%s complemento(s) selecionado(s)", itHas ? "Há" : "Não há");

        return itHas;
    }
}

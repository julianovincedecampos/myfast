package essenti.com.br.MyFast.relatorio;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.otto.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.util.eventbus.BusProvider;

public class RelatorioComissaoFragment extends Fragment {

    private ComissaoAdapter mComissaoAdapter;

    @BindView(R.id.listComissao)
    RecyclerView mListComissao;

    public static RelatorioComissaoFragment newInstance() {
        return new RelatorioComissaoFragment();
    }

    public RelatorioComissaoFragment() {}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle inState) {
        final View fragmentView = inflater.inflate(R.layout.fragment_relatorio_comissao, container, false);
        ButterKnife.bind(this, fragmentView);

        mListComissao.setHasFixedSize(true);
        mListComissao.setLayoutManager(new LinearLayoutManager(getContext()));

        return fragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    @Subscribe
    public void onResponseListaVendas(ListaVendas listaVendas) {
        if (mComissaoAdapter == null) {
            mListComissao.setAdapter(
                    mComissaoAdapter = new ComissaoAdapter(getContext(), listaVendas.comissoes)
            );
        } else {
            mComissaoAdapter.changeData(listaVendas.comissoes);
        }
    }
}

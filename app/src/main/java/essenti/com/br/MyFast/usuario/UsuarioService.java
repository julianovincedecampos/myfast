package essenti.com.br.MyFast.usuario;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

public interface UsuarioService {
    @GET("ServiceUsuario.svc/Usuarios")
    Observable<ListaUsuarios> todos();

    @GET("ServiceUsuario.svc/Autenticar/{matricula}/{senha}")
    Observable<ResultadoAutenticacao> autenticar(@Path("matricula") int matricula,
            @Path("senha") String senha);
}

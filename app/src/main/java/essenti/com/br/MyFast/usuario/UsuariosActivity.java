package essenti.com.br.MyFast.usuario;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;
import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.android.activity.BaseActivity;
import essenti.com.br.MyFast.android.recyclerview.DividerDecoration;
import essenti.com.br.MyFast.android.recyclerview.OnClickListener;
import essenti.com.br.MyFast.android.recyclerview.OnTouchListener;
import essenti.com.br.MyFast.android.widget.MensagemDialogo;
import essenti.com.br.MyFast.app.App;
import essenti.com.br.MyFast.util.FeedbackHelper;
import essenti.com.br.MyFast.util.NetworkUtil;
import essenti.com.br.MyFast.webservice.ServiceGenerator;
import io.fabric.sdk.android.Fabric;
import java.util.List;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class UsuariosActivity extends BaseActivity implements OnClickListener {
    public static final String EXTRA_OPERADOR_SELECIONADO = "operadorSelecionado";

    private Subscription mSubscription;

    private UsuarioAdapter mAdapter;

    private MensagemDialogo mDialogoProgresso;

    @BindView(android.R.id.list) RecyclerView mRecyclerView;

    private void setupAdapter(ListaUsuarios listaUsuarios) {
        mRecyclerView.setAdapter(
                mAdapter = new UsuarioAdapter(listaUsuarios.usuarios, UsuariosActivity.this));
        mRecyclerView.addOnItemTouchListener(new OnTouchListener(this, mRecyclerView, this));
    }

    private void storeUsuarios(List<Usuario> list) {
        App.getInstancia().setUsuarios(list);
    }

    private void showError(Throwable e) {
        dismissDialogoProgresso();

        if (Fabric.isInitialized()) {
            Crashlytics.logException(e);
        }

        App.getInstancia().exibirMensagem(this, MensagemDialogo.MENSAGEM_ERRO,
                "Atenção", "Infelizmente houve uma falha ao consultar os operadores. "
                        + "Por favor tente novamente.\n"
                        + "Esta falha será reportada para o administrador do sistema!");
    }

    private void showDialogoProgresso(String mensagem) {
        if (mDialogoProgresso == null) {
            mDialogoProgresso = App.getInstancia().exibirProgresso(this,
                    "Processando", mensagem);
            mDialogoProgresso.exibir();
        } else {
            mDialogoProgresso.setTexto(mensagem);
        }
    }

    private void dismissDialogoProgresso() {
        if (mDialogoProgresso != null) {
            if (mDialogoProgresso.isExibindo()) {
                mDialogoProgresso.esconder();
            }

            mDialogoProgresso = null;
        }
    }

    @Override
    protected int provideLayoutResource() {
        return R.layout.activity_usuarios;
    }

    @Override
    protected boolean shouldDisplayHomeAsUpEnabled() {
        return true;
    }

    @Override
    protected int provideUpIndicator() {
        return R.drawable.ic_arrow_back_white_24dp;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new DividerDecoration(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_usuarios, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_search);
        menuItem.expandActionView();

        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setQueryHint("Informe a matrícula");
        EditText searchEditText = ButterKnife.findById(searchView, R.id.search_src_text);
        searchEditText.setInputType(
                InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(final String newText) {
                if (mAdapter != null) {
                    mAdapter.getFilter().filter(newText);
                }
                return true;
            }
        });
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();

        requestListaUsuarios();
    }

    private void requestListaUsuarios() {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            final UsuarioService service = ServiceGenerator.createService(UsuarioService.class,
                    this);
            if (service != null) {
                mSubscription = service.todos()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(
                                new Subscriber<ListaUsuarios>() {
                                    @Override
                                    public void onStart() {
                                        showDialogoProgresso("Carregando os operadores...");
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        showError(e);
                                    }

                                    @Override
                                    public void onNext(ListaUsuarios listaUsuarios) {
                                        storeUsuarios(listaUsuarios.usuarios);
                                        setupAdapter(listaUsuarios);
                                    }

                                    @Override
                                    public void onCompleted() {
                                        dismissDialogoProgresso();
                                    }
                                }
                        );
            } else {
                App.getInstancia()
                        .exibirMensagem(this, MensagemDialogo.MENSAGEM_ALERTA,
                                "Configuração não realizada",
                                "Não foi possível listar os operadores cadastrados. "
                                        + "Faça a configuração primeiramente!");
            }
        } else {
            FeedbackHelper.showOfflineMessage(this,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            finish();
                        }
                    },
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            requestListaUsuarios();
                        }
                    });
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mSubscription != null && mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
    }

    @Override
    public void onSingleTapUp(View view, int position) {
        if (mAdapter != null) {
            Usuario usuario = mAdapter.getItem(position);
            if (usuario != null) {
                Intent intent = new Intent();
                intent.putExtra(EXTRA_OPERADOR_SELECIONADO, usuario);
                setResult(App.OPERADOR_SELECIONADO, intent);
                finish();
            }
        }
    }

    @Override
    public void onLongPress(View view, int position) {
    }
}

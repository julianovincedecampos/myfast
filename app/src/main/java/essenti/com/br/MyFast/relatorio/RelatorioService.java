package essenti.com.br.MyFast.relatorio;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * @author Filipe Bezerra
 * @version 1.4.0, 04/04/2016
 */
public interface RelatorioService {
    @GET("ServiceConsulta.svc/ListarVendas/{codigoCardapio}/{mesAno}/{matriculaGarcom}")
    Observable<ListaVendas> listarVendas(@Path("codigoCardapio") int codigoCardapio,
            @Path("mesAno") String mesAno, @Path("matriculaGarcom") int matriculaGarcom);
}

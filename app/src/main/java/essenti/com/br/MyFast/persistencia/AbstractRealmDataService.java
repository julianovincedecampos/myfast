package essenti.com.br.MyFast.persistencia;

import android.content.Context;
import essenti.com.br.MyFast.rx.RealmObservable;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import rx.Observable;
import rx.functions.Func1;

/**
 * @author Filipe Bezerra
 */
public abstract class AbstractRealmDataService<T, E extends RealmObject>
        implements DataService<T, E> {

    protected final Context mContext;

    protected final Class<E> mEntityClass;

    public AbstractRealmDataService(Context context, Class<E> entityClass) {
        mContext = context;
        mEntityClass = entityClass;
    }

    @Override
    public Observable<T> save(final T viewObject) {
        return RealmObservable
                .object(mContext, new Func1<Realm, E>() {
                    @Override
                    public E call(Realm realm) {
                        final E entity = toEntity(viewObject);
                        if (entity == null) {
                            return null;
                        }
                        return realm.copyToRealmOrUpdate(entity);
                    }
                })
                .map(new Func1<E, T>() {
                    @Override
                    public T call(E entity) {
                        if (entity == null) {
                            return null;
                        }
                        return toViewObject(entity);
                    }
                });
    }

    @Override
    public Observable<List<T>> saveAll(final List<T> viewObjects) {
        return RealmObservable
                .list(mContext, new Func1<Realm, RealmList<E>>() {
                    @Override
                    public RealmList<E> call(Realm realm) {
                        if (viewObjects == null || viewObjects.isEmpty()) {
                            return null;
                        }

                        List<E> newEntities = new ArrayList<>(viewObjects.size());

                        for (T object : viewObjects) {
                            newEntities.add(realm.copyToRealmOrUpdate(toEntity(object)));
                        }

                        RealmList<E> realmObjects = new RealmList<>();
                        realmObjects.addAll(newEntities);

                        return realmObjects;
                    }
                })
                .map(new Func1<RealmList<E>, List<T>>() {
                    @Override
                    public List<T> call(RealmList<E> entities) {
                        if (entities == null || entities.isEmpty()) {
                            return Collections.emptyList();
                        }

                        List<T> savedViewObjects = new ArrayList<>();

                        for (E entity : entities) {
                            savedViewObjects.add(toViewObject(entity));
                        }

                        return savedViewObjects;
                    }
                });
    }

    @Override
    public Observable<List<T>> list() {
        return RealmObservable
                .results(mContext, new Func1<Realm, RealmResults<E>>() {
                    @Override
                    public RealmResults<E> call(Realm realm) {
                        return realm.where(mEntityClass).findAll();
                    }
                })
                .map(new Func1<RealmResults<E>, List<T>>() {
                    @Override
                    public List<T> call(RealmResults<E> entities) {
                        if (entities == null || entities.isEmpty()) {
                            return Collections.emptyList();
                        }

                        final List<T> viewObjectList = new ArrayList<>();

                        for (E entity : entities) {
                            viewObjectList.add(toViewObject(entity));
                        }

                        return viewObjectList;
                    }
                });
    }

    protected abstract E toEntity(T viewObject);

    protected abstract T toViewObject(E entity);
}

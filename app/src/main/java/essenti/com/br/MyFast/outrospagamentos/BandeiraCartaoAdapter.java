package essenti.com.br.MyFast.outrospagamentos;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.squareup.picasso.Picasso;
import essenti.com.br.MyFast.R;

/**
 * @author Filipe Bezerra
 */
class BandeiraCartaoAdapter extends ArrayAdapter<BandeiraCartao> {

    private LayoutInflater mInflater;

    BandeiraCartaoAdapter(Context context) {
        super(context, 0, BandeirasCartao.getListaBandeiraCartao());
        setDropDownViewResource(R.layout.item_bandeira_cartao);
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_bandeira_cartao, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        BandeiraCartao bandeiraCartao = getItem(position);
        holder.textViewNomeCartao.setText(bandeiraCartao.getNome());
        Picasso.with(getContext())
                .load(bandeiraCartao.getIcone())
                .into(holder.imageViewBandeiraCartao);
        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.image_view_bandeira_cartao) ImageView imageViewBandeiraCartao;
        @BindView(R.id.text_view_nome_cartao) TextView textViewNomeCartao;

        public ViewHolder(View itemView) {
            ButterKnife.bind(this, itemView);
        }
    }

}

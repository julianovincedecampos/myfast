package essenti.com.br.MyFast.android.recyclerview;

import android.view.View;

public interface OnClickListener {
    void onSingleTapUp(View view, int position);

    void onLongPress(View view, int position);
}

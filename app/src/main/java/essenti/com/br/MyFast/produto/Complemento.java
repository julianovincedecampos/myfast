package essenti.com.br.MyFast.produto;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hannesdorfmann.parcelableplease.annotation.ParcelablePlease;

/**
 * Entidade Complemento
 *
 * @author Diego, Filipe Bezerra[
 * @version #, 13/01/2016
 * @since 1.0.0
 */
@ParcelablePlease
public class Complemento implements Parcelable {
    @SerializedName("Codcomp") @Expose int codigo;

    @SerializedName("Nomecomplemento") @Expose String descricao;

    int quantidade;

    @SerializedName("Precocomp") @Expose double valorUnitario;

    Boolean selecionado;

    public Complemento() {
        this.codigo = 0;
        this.descricao = "";
        this.quantidade = 0;
        this.valorUnitario = 0;
        this.selecionado = false;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
        this.selecionado = this.quantidade > 0;
    }

    public double getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public Boolean getSelecionado() {
        return selecionado;
    }

    public void setSelecionado(Boolean selecionado) {
        this.selecionado = selecionado;
    }
    public double getValorTotal() {
        return this.getValorUnitario() * this.getQuantidade();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ComplementoParcelablePlease.writeToParcel(this, dest, flags);
    }

    public static final Creator<Complemento> CREATOR = new Creator<Complemento>() {
        public Complemento createFromParcel(Parcel source) {
            Complemento target = new Complemento();
            ComplementoParcelablePlease.readFromParcel(target, source);
            return target;
        }

        public Complemento[] newArray(int size) {
            return new Complemento[size];
        }
    };
}

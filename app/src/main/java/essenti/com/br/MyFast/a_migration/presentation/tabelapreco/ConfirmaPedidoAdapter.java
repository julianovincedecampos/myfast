package essenti.com.br.MyFast.a_migration.presentation.tabelapreco;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.app.App;
import essenti.com.br.MyFast.produto.Produto;
import java.text.NumberFormat;
import java.util.List;

/**
 * @author Filipe Bezerra
 */
class ConfirmaPedidoAdapter extends RecyclerView.Adapter<ConfirmaPedidoViewHolder> {

    private static NumberFormat sNumberFormat = NumberFormat.getCurrencyInstance();

    private final Context mContext;

    private final List<Produto> mProdutos;

    ConfirmaPedidoAdapter(Context pContext, List<Produto> pProdutos) {
        mContext = pContext;
        mProdutos = pProdutos;
    }

    @Override
    public ConfirmaPedidoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext)
                .inflate(R.layout.item_lista_confirma_pedidos, parent, false);
        return new ConfirmaPedidoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ConfirmaPedidoViewHolder holder, int position) {
        final Produto produto = mProdutos.get(position);

        holder.txtDescricaoItem.setText(produto.getDescricao());

        holder.txtTotalItem.setText(
                String.format(App.BRAZIL_LOCALE, "%03d", produto.getQuantidade()));

        holder.txtValorUnitario.setText(
                mContext.getString(R.string.valor_unitario,
                        sNumberFormat.format(produto.getValorUnitario())));

        holder.txtValorTotalItem.setText(sNumberFormat.format(produto.getValorTotal()));

        holder.btnRemoverItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int adapterPosition = holder.getAdapterPosition();
                mProdutos.remove(adapterPosition);
                notifyItemRemoved(adapterPosition);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mProdutos.size();
    }
}

package essenti.com.br.MyFast.mesa;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class ListaMesas {
    @SerializedName("ListarMesasResult")
    public List<Mesa> mesas;
}


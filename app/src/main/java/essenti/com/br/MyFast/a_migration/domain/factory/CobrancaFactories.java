package essenti.com.br.MyFast.a_migration.domain.factory;

import java.util.ArrayList;
import java.util.List;

import essenti.com.br.MyFast.a_migration.domain.dto.CobrancaDto;
import essenti.com.br.MyFast.a_migration.domain.pojo.Cobranca;
import essenti.com.br.MyFast.a_migration.domain.utils.Preconditions;
import essenti.com.br.MyFast.a_migration.domain.utils.StringUtils;

/**
 * @author Filipe Bezerra
 */

public class CobrancaFactories {
    private CobrancaFactories() {/* No instances */}

    public static Cobranca createProduto(CobrancaDto cobrancaDto) {
        Preconditions.checkNotNull(
                cobrancaDto, "CobrancaDto can't be null");
        Preconditions.checkState(
                !StringUtils.isEmpty(cobrancaDto.codigo), "CobrancaDto.codigo can't be empty");
        Preconditions.checkState(
                !StringUtils.isEmpty(cobrancaDto.descricao), "CobrancaDto.descricao can't be empty");
        Preconditions.checkState(
                !StringUtils.isEmpty(cobrancaDto.cartao), "CobrancaDto.cartao can't be empty");

        return new Cobranca(
                cobrancaDto.cartao, cobrancaDto.codigo, cobrancaDto.descricao
        );
    }

    public static List<Cobranca> createListProduto(List<CobrancaDto> cobrancaDtos) {
        Preconditions.checkNotNull(
                cobrancaDtos, "List<CobrancaDto> can't be null");

        List<Cobranca> cobrancaList = new ArrayList<>();

        for (CobrancaDto dto : cobrancaDtos) {
            cobrancaList.add(createProduto(dto));
        }
        return cobrancaList;
    }
}

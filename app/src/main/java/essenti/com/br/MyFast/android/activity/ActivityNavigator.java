package essenti.com.br.MyFast.android.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import essenti.com.br.MyFast.a_migration.domain.pojo.Cobranca;
import essenti.com.br.MyFast.a_migration.presentation.listacobranca.ListaCobrancaActivity;
import essenti.com.br.MyFast.a_migration.presentation.tabelapreco.ConsultaTabelaPrecoActivity;
import essenti.com.br.MyFast.app.App;
import essenti.com.br.MyFast.app.PainelActivity;
import essenti.com.br.MyFast.atendimento.AtendimentoMesaActivity;
import essenti.com.br.MyFast.mesa.Mesa;
import essenti.com.br.MyFast.mesa.MesasActivity;
import essenti.com.br.MyFast.outrospagamentos.OutrosPagamentosActivity;
import essenti.com.br.MyFast.pagamento.PagamentosRealizadosActivity;
import essenti.com.br.MyFast.produto.ProdutosActivity;
import essenti.com.br.MyFast.produto.ProdutosAvulsosActivity;
import essenti.com.br.MyFast.relatorio.RelatoriosActivity;
import essenti.com.br.MyFast.usuario.AutenticacaoOperadorActivity;
import essenti.com.br.MyFast.usuario.UsuariosActivity;

import static essenti.com.br.MyFast.app.App.EXTRA_MESA_ATENDIMENTO;
import static essenti.com.br.MyFast.usuario.AutenticacaoOperadorActivity.EXTRA_TIPO_AUTENTICACAO;
import static essenti.com.br.MyFast.usuario.AutenticacaoOperadorActivity.TIPO_TROCA_MESA;
import static essenti.com.br.MyFast.usuario.AutenticacaoOperadorActivity.TIPO_TROCA_PRODUTOS_MESA;

/**
 * @author Filipe Bezerra
 * @since 1.4.0
 */
public class ActivityNavigator {
    public static final int REQUEST_OUTROS_PAGAMENTOS = 0X1;

    public static void startPagamentoCartaoActivity(
            @NonNull Activity activity, @NonNull Mesa mesa, boolean pagamentoParcial) {
        activity.startActivityForResult(
                new Intent(activity, PagamentosRealizadosActivity.class)
                        .putExtra(EXTRA_MESA_ATENDIMENTO, mesa)
                        .putExtra(
                                PagamentosRealizadosActivity.EXTRA_PAGAMENTO_PARCIAL,
                                pagamentoParcial),
                App.REQUEST_PAGAMENTO_CARTAO
        );
    }

    public static void startOutrosPagamentosActivity(
            @NonNull Activity activity, @NonNull Mesa mesa, Cobranca formaPagamento) {
        activity.startActivityForResult(
                new Intent(activity, OutrosPagamentosActivity.class)
                        .putExtra(EXTRA_MESA_ATENDIMENTO, mesa)
                        .putExtra(OutrosPagamentosActivity.EXTRA_FORMA_PAGAMENTO, formaPagamento),
                REQUEST_OUTROS_PAGAMENTOS
        );
    }

    public static void startProdutosActivity(@NonNull Activity activity, @NonNull Mesa mesa) {
        activity.startActivityForResult(
                new Intent(activity, ProdutosActivity.class)
                        .putExtra(EXTRA_MESA_ATENDIMENTO, mesa),
                App.ADICIONAR_PRODUTO
        );
    }

    public static void startProdutosAvulsosActivity(@NonNull Activity activity, @NonNull Mesa mesa) {
        activity.startActivityForResult(
                new Intent(activity, ProdutosAvulsosActivity.class)
                        .putExtra(EXTRA_MESA_ATENDIMENTO, mesa),
                App.ADICIONAR_PRODUTO_AVULSO
        );
    }

    public static void startAtendimentoMesaActivity(@NonNull Activity activity, @NonNull Mesa mesa) {
        activity.startActivityForResult(
                new Intent(activity, AtendimentoMesaActivity.class)
                        .putExtra(EXTRA_MESA_ATENDIMENTO, mesa),
                App.ATENDIMENTO_MESA
        );
    }

    public static void startMesasActivity(@NonNull Context context) {
        context.startActivity(new Intent(context, MesasActivity.class));
    }

    public static void startPainelActivity(@NonNull Context context) {
        context.startActivity(new Intent(context, PainelActivity.class));
    }

    public static void startRelatoriosActivity(@NonNull Context context) {
        context.startActivity(new Intent(context, RelatoriosActivity.class));
    }

    public static void startUsuariosActivity(@NonNull Activity activity) {
        activity.startActivityForResult(
                new Intent(activity, UsuariosActivity.class), App.SELECIONAR_OPERADOR);
    }

    public static void startAutenticacaoOperadorActivity(@NonNull Activity activity,
            @IntRange(from = TIPO_TROCA_MESA, to = TIPO_TROCA_PRODUTOS_MESA) int tipoAutenticacao,
            @NonNull Mesa mesa) {
        activity.startActivityForResult(
                new Intent(activity, AutenticacaoOperadorActivity.class)
                        .putExtra(EXTRA_MESA_ATENDIMENTO, mesa)
                        .putExtra(EXTRA_TIPO_AUTENTICACAO, tipoAutenticacao),
                App.AUTENTICAR_OPERADOR
        );
    }

    public static void startConsultaTabelaPrecoActivity(@NonNull Context pContext) {
        pContext.startActivity(new Intent(pContext, ConsultaTabelaPrecoActivity.class));
    }

    public static void startListaCobrancaActivity(@NonNull Activity pActivity, @NonNull Mesa mesa) {
        pActivity.startActivityForResult(
                new Intent(pActivity, ListaCobrancaActivity.class)
                        .putExtra(EXTRA_MESA_ATENDIMENTO, mesa), REQUEST_OUTROS_PAGAMENTOS);
    }

}

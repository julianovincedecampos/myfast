package essenti.com.br.MyFast.android.asynctask;

/**
 * MyFast.Android
 *
 * @author Filipe Bezerra on 14/12/2015.
 */
public interface TaskCallback<R> {
    void onBegin();
    void onSuccess(R result);
    void onResultNothing();
    void onError(TaskException e);
}

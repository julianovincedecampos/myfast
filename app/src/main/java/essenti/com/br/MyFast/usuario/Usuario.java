package essenti.com.br.MyFast.usuario;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.hannesdorfmann.parcelableplease.annotation.ParcelablePlease;

@ParcelablePlease
public class Usuario implements Parcelable {
    @SerializedName("Matricula") Integer matricula;

    @SerializedName("Nome") String nome;

    @SerializedName("SolicitaSenha") String solicitarSenha;

    public Usuario() {}

    public Usuario(Integer pMatricula, String pNome, String pSolicitarSenha) {
        this.matricula = pMatricula;
        this.nome = pNome;
        this.solicitarSenha = pSolicitarSenha;
    }

    public Integer getMatricula() {
        return matricula;
    }

    public String getNome() {
        return nome;
    }

    public String getSolicitarSenha() {
        return solicitarSenha;
    }

    @Override
    public String toString() {
        return getMatricula() + " - " + getNome();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        UsuarioParcelablePlease.writeToParcel(this, dest, flags);
    }

    public static final Creator<Usuario> CREATOR = new Creator<Usuario>() {
        public Usuario createFromParcel(Parcel source) {
            Usuario target = new Usuario();
            UsuarioParcelablePlease.readFromParcel(target, source);
            return target;
        }

        public Usuario[] newArray(int size) {
            return new Usuario[size];
        }
    };
}

package essenti.com.br.MyFast.persistencia;

import java.util.List;

import io.realm.RealmObject;
import rx.Observable;

public interface DataService<T, E extends RealmObject> {

    Observable<T> save(T viewObject);

    Observable<List<T>> saveAll(List<T> viewObjects);

    Observable<List<T>> list();
}

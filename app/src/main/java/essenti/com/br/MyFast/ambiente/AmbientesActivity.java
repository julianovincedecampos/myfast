package essenti.com.br.MyFast.ambiente;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import essenti.com.br.MyFast.app.App;
import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.app.ConfiguracaoActivity;
import essenti.com.br.MyFast.android.widget.MensagemDialogo;
import essenti.com.br.MyFast.cardapio.Cardapio;

/**
 * Controlador da tela Configuração do {@link Cardapio} padrão. Esta tela apresenta a listagem
 * dos cardápios cadastrados.
 *
 * @author Filipe Bezerra
 * @version 1.4.0, 16/03/2016
 * @since 1.0.0, 04/12/2015
 */
public class AmbientesActivity extends AppCompatActivity {
    public static final String EXTRA_URL = "URL";

    @BindView(R.id.listViewAmbientes) ListView mListViewAmbientes;

    private AmbienteAdapter mAmbienteAdapter;
    private List<Ambiente> mAmbientes = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ambientes);
        ButterKnife.bind(this);

        mAmbienteAdapter = new AmbienteAdapter(this, mAmbientes);
        mListViewAmbientes.setFastScrollEnabled(true);
        mListViewAmbientes.setAdapter(mAmbienteAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GetAmbientesTask().execute(getIntent().getStringExtra(EXTRA_URL));
    }

    @OnItemClick(R.id.listViewAmbientes)
    public void onItemClick(int position) {
        final Ambiente ambiente = mAmbienteAdapter.getItem(position);
        Intent intent = new Intent();
        intent.putExtra(ConfiguracaoActivity.EXTRA_CODIGO_AMBIENTE, ambiente.getCodigo());
        setResult(App.AMBIENTE_SELECIONADO, intent);
        finish();
    }

    private class GetAmbientesTask extends AsyncTask<String, Void, List<Ambiente>> {
        private final MensagemDialogo mDialogo;

        public GetAmbientesTask() {
            mDialogo = App.getInstancia().exibirProgresso(AmbientesActivity.this,
                    "Processando", "Carregando os ambientes disponíveis...");
        }

        @Override
        protected void onPreExecute() {
            mDialogo.exibir();
        }

        @Override
        protected List<Ambiente> doInBackground(String... params) {
            List<Ambiente> registros = new ArrayList<>();
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httpget = new HttpGet(params[0]);

            try {
                HttpResponse response = httpclient.execute(httpget);
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    InputStream instream = entity.getContent();
                    String json = toString(instream);
                    instream.close();
                    registros.addAll(getRegistros(json));
                    return registros;
                }
            } catch (Exception e) {
                Log.e("MyFastWCF", "Falha ao acessar Web service", e);
            }

            return registros;
        }

        @Override
        protected void onPostExecute(List<Ambiente> registros) {
            mAmbientes.clear();
            mAmbientes.addAll(registros);
            mAmbienteAdapter.notifyDataSetChanged();
            mDialogo.esconder();
        }

        private List<Ambiente> getRegistros(String jsonString) {
            List<Ambiente> registros = new ArrayList<>();

            try {
                JSONObject jsonObject = new JSONObject(jsonString);
                JSONArray jsonArray = jsonObject.getJSONArray("ListarAmbientesResult");
                JSONObject jsonAmbiente;

                for (int i = 0; i < jsonArray.length(); i++) {
                    jsonAmbiente = jsonArray.getJSONObject(i);
                    Ambiente registro = new Ambiente(
                            jsonAmbiente.getInt("Codambiente"), jsonAmbiente.getString("Descricao"));
                    registros.add(registro);
                }
            } catch (JSONException e) {
                Log.e("MyFastService", "Erro no parsing do JSON", e);
            }
            return registros;
        }

        private String toString(InputStream is) throws IOException {
            byte[] bytes = new byte[1024];
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            int lidos;
            while ((lidos = is.read(bytes)) > 0) {
                baos.write(bytes, 0, lidos);
            }
            return new String(baos.toByteArray());
        }
    }
}

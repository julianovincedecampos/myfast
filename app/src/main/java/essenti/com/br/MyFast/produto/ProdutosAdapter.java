package essenti.com.br.MyFast.produto;

import android.app.AlertDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.implments.SwipeItemRecyclerMangerImpl;
import com.daimajia.swipe.interfaces.SwipeAdapterInterface;
import com.daimajia.swipe.interfaces.SwipeItemMangerInterface;
import com.daimajia.swipe.util.Attributes;
import com.h6ah4i.android.widget.advrecyclerview.expandable.ExpandableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemViewHolder;
import com.h6ah4i.android.widget.advrecyclerview.utils.RecyclerViewAdapterUtils;
import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.android.widget.MensagemDialogo;
import essenti.com.br.MyFast.app.App;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Adaptador da tela {@link ProdutosActivity}
 *
 * @author Filipe Bezerra
 * @since 09/12/2015
 */
public class ProdutosAdapter
        extends AbstractExpandableItemAdapter<
        ProdutosAdapter.GruposViewHolder, ProdutosAdapter.ProdutosViewHolder>
        implements Filterable, SwipeItemMangerInterface, SwipeAdapterInterface {

    public static final int CODIGO_GRUPO_FILTRADO = 9999;
    public static final String DESCRICAO_GRUPO_FILTRADO = "Resultado do Filtro";
    @NonNull
    private List<Grupo> mGrupos;

    /**
     * Filtro ativado pela pesquisa do usuário. Este filtro está desativado por padrão,
     * e sua referência deve ser removida sempre que o filtro for removido.
     */
    @Nullable
    private List<Grupo> mGruposFiltrados;

    @NonNull
    private final RecyclerViewExpandableItemManager mExpandableItemManager;
    @NonNull
    private final Context mContext;

    /**
     * Lista de produtos selecionados pelo usuário. Esta lista aumenta quando o
     * usuário adiciona 1 quantidade do produto e diminui quando o usuário
     * remove a quantidade até completar zero.
     */
    private List<Produto> mProdutosSelecionados = new ArrayList<>();

    /**
     * Lista de referência de {@link #mProdutosSelecionados}.
     * <p/>
     * É composta por pares do índice do grupo e do filho.
     */
    private List<Pair<Integer, Integer>> mReferenciaProdutosSelecionados
            = new ArrayList<>();

    private View.OnClickListener mItemOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onClickItemView(v);
        }
    };

    public SwipeItemRecyclerMangerImpl mItemManger = new SwipeItemRecyclerMangerImpl(this);

    /**
     * Inicia o adaptador com uma lista de grupos fornecida. A lista fornecida via argumento
     * não deve ser nula.
     *
     * @param context               contexto da tela.
     * @param grupos                lista de grupos inicializada.
     * @param expandableItemManager gerenciador de expansão dos itens.
     */
    public ProdutosAdapter(@NonNull Context context, @NonNull List<Grupo> grupos,
            @NonNull RecyclerViewExpandableItemManager expandableItemManager) {
        mContext = context;
        mGrupos = grupos;
        mExpandableItemManager = expandableItemManager;
        setHasStableIds(true);
        setMode(Attributes.Mode.Multiple);
    }

    /**
     * Verifica se existe filtro ativo na lista de grupos.
     *
     * @return se existe filtro ativo na lista de grupos.
     */
    private boolean isFiltroGruposAtivado() {
        return mGruposFiltrados != null;
    }

    /**
     * Se o filtro estiver ativo, esvazia e libera a referência da lista de grupos filtrados.
     */
    private void desativarFiltroGrupos() {
        if (isFiltroGruposAtivado()) {
            //noinspection ConstantConditions
            mGruposFiltrados.clear();
            mGruposFiltrados = null;
        }
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public int getGroupCount() {
        return isFiltroGruposAtivado() ?
                mGruposFiltrados.size() : mGrupos.size();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public int getChildCount(int groupPosition) {
        if (groupPosition < 0 || groupPosition >= getGroupCount()) {
            return RecyclerView.NO_POSITION;
        }

        return isFiltroGruposAtivado() ?
                mGruposFiltrados.get(groupPosition).getProdutos().size() :
                mGrupos.get(groupPosition).getProdutos().size();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public long getGroupId(int groupPosition) {
        if (groupPosition < 0 || groupPosition >= getGroupCount()) {
            return RecyclerView.NO_ID;
        }

        return isFiltroGruposAtivado() ?
                mGruposFiltrados.get(groupPosition).getCodigo() :
                mGrupos.get(groupPosition).getCodigo();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        if (groupPosition < 0 || groupPosition >= getGroupCount()) {
            return RecyclerView.NO_ID;
        }

        if (childPosition < 0 || childPosition >= getChildCount(groupPosition)) {

            return RecyclerView.NO_ID;
        }

        return (long) (isFiltroGruposAtivado() ?
                mGruposFiltrados.get(groupPosition).getProdutos().get(childPosition)
                        .getSequencia() :
                mGrupos.get(groupPosition).getProdutos().get(childPosition)
                        .getSequencia());
    }

    @Override
    public int getGroupItemViewType(int groupPosition) {
        return 0;
    }

    @Override
    public int getChildItemViewType(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public GruposViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(mContext)
                .inflate(R.layout.item_grupo, parent, false);
        return new GruposViewHolder(itemView);
    }

    @Override
    public ProdutosViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext)
                .inflate(R.layout.item_produto, parent, false);
        return new ProdutosViewHolder(itemView, mItemOnClickListener);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onBindGroupViewHolder(GruposViewHolder holder, int groupPosition, int viewType) {
        if (groupPosition == RecyclerView.NO_POSITION) return;

        final Grupo grupo = isFiltroGruposAtivado() ?
                mGruposFiltrados.get(groupPosition) :
                mGrupos.get(groupPosition);

        holder.txtTituloGrupo.setText(grupo.getDescricao());

        final int stateFlags = holder.getExpandStateFlags();

        if ((stateFlags & ExpandableItemConstants.STATE_FLAG_IS_UPDATED) != 0) {
            if ((stateFlags & ExpandableItemConstants.STATE_FLAG_IS_EXPANDED) != 0) {
                holder.imgStatusGrupo.setImageResource(R.drawable.ic_contrair);
            } else {
                holder.imgStatusGrupo.setImageResource(R.drawable.ic_expandir);
            }
        }
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onBindChildViewHolder(ProdutosViewHolder holder, int groupPosition,
            int childPosition, int viewType) {
        if (groupPosition == RecyclerView.NO_POSITION ||
                childPosition == RecyclerView.NO_POSITION) return;

        final Produto produto = isFiltroGruposAtivado() ?
                mGruposFiltrados.get(groupPosition).getProdutos().get(childPosition) :
                mGrupos.get(groupPosition).getProdutos().get(childPosition);

        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);

        holder.txtTotalItem.setText(
                String.format(App.BRAZIL_LOCALE, "%03d", produto.getQuantidade()));
        holder.txtDescricaoItem.setText(produto.getDescricao());
        holder.txtValorUnitario.setText(
                String.format(App.BRAZIL_LOCALE, "Valor Unit. %s",
                        NumberFormat.getCurrencyInstance().format(produto.getValorUnitario())));
        holder.txtValorTotalItem.setText(
                NumberFormat.getCurrencyInstance().format(produto.getValorTotal()));

        RecyclerView.ViewHolder vh = RecyclerViewAdapterUtils.getViewHolder(holder.itemView);
        if (vh == null) {
            return;
        }

        int flatPosition = vh.getAdapterPosition();

        if (flatPosition == RecyclerView.NO_POSITION) {
            return;
        }

        long expandablePosition = mExpandableItemManager.getExpandablePosition(flatPosition);
        int packedPositionChild = RecyclerViewExpandableItemManager.getPackedPositionChild(expandablePosition);

        mItemManger.bindView(holder.itemView, packedPositionChild);
    }

    @Override
    public boolean onCheckCanExpandOrCollapseGroup(GruposViewHolder holder,
            int groupPosition, int x, int y, boolean expand) {
        return holder.itemView.isEnabled() && holder.itemView.isClickable();
    }

    @Override
    public Filter getFilter() {
        return new ProdutosFilter(this);
    }

    public List<Produto> getProdutosSelecionados() {
        return new ArrayList<>(mProdutosSelecionados);
    }

    @SuppressWarnings("ConstantConditions")
    public boolean isEmpty() {
        return isFiltroGruposAtivado() ?
                mGruposFiltrados.isEmpty() : mGrupos.isEmpty();
    }

    void onClickItemView(View v) {
        RecyclerView.ViewHolder vh = RecyclerViewAdapterUtils.getViewHolder(v);
        if (vh == null) {
            return;
        }

        int flatPosition = vh.getAdapterPosition();

        if (flatPosition == RecyclerView.NO_POSITION) {
            return;
        }

        long expandablePosition = mExpandableItemManager.getExpandablePosition(flatPosition);
        int groupPosition = RecyclerViewExpandableItemManager.getPackedPositionGroup(expandablePosition);
        int childPosition = RecyclerViewExpandableItemManager.getPackedPositionChild(expandablePosition);

        switch (v.getId()) {
            case R.id.btnAdicionarItem:
                handleOnClickBtnAdicionarItem(groupPosition, childPosition);
                break;
            case R.id.btnRemoverItem:
                handleOnClickBtnRemoverItem(groupPosition, childPosition);
                break;
            case R.id.lnlAdicional:
                handleOnClickLnlAdicional(groupPosition, childPosition);
                break;
            case R.id.lnlObservacao:
                handleOnClickLnlObservacao(groupPosition, childPosition);
                break;
        }
    }

    @SuppressWarnings("ConstantConditions")
    private void handleOnClickLnlObservacao(int groupPosition, int childPosition) {
        final Produto produto = obtainProduto(groupPosition, childPosition);

        if (produto.getQuantidade() > 0) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_observacao_produto,
                    null);

            ImageButton btnConfirmar = (ImageButton) view.findViewById(R.id.btnConfirmar);
            ImageView imgFechar = (ImageView) view.findViewById(R.id.imgFechar);
            final EditText edtObservacoes = (EditText) view.findViewById(R.id.edtObservacoes);
            edtObservacoes.setText(produto.getObservacao());
            builder.setView(view);
            final AlertDialog dialog = builder.create();

            imgFechar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            btnConfirmar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    produto.setObservacao(edtObservacoes.getText().toString());
                    dialog.dismiss();

                }
            });
            dialog.show();
        } else {
            App.getInstancia()
                    .exibirMensagem(mContext, MensagemDialogo.MENSAGEM_ALERTA,
                            "Atenção", "Informe primeiro a quantidade deste produto!");
        }
    }

    @SuppressWarnings("ConstantConditions")
    private void handleOnClickLnlAdicional(final int groupPosition, final int childPosition) {
        final Produto produto = obtainProduto(groupPosition, childPosition);

        if (produto.getQuantidade() > 0) {
            if (!produto.getComplementos().isEmpty()) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                View view = LayoutInflater.from(mContext)
                        .inflate(R.layout.dialog_selecionar_complemento_produto, null);

                ImageButton btnConfirmar = (ImageButton) view.findViewById(R.id.btnConfirmar);
                ImageView imgFechar = (ImageView) view.findViewById(R.id.imgFechar);


                final ListView listViewItens = (ListView) view.findViewById(R.id.listViewItens);
                final ComplementoAdapter complementoArrayAdapter =
                        new ComplementoAdapter(mContext, produto.getComplementos());
                listViewItens.setAdapter(complementoArrayAdapter);

                builder.setView(view);
                final AlertDialog dialog = builder.create();

                imgFechar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                btnConfirmar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!complementoArrayAdapter.hasComplementoSelecionado()) {
                            App.getInstancia().exibirMensagem(
                                    mContext,
                                    MensagemDialogo.MENSAGEM_ALERTA,
                                    "Atenção",
                                    "Nenhum complemento foi adicionado!");
                            return;
                        }

                        dialog.dismiss();
                        mExpandableItemManager.notifyChildItemChanged(groupPosition, childPosition);
                    }
                });
                dialog.show();
            } else {
                App.getInstancia()
                        .exibirMensagem(mContext, MensagemDialogo.MENSAGEM_ALERTA, "Atenção",
                                "Não existe adicional para este produto!");
            }
        } else {
            App.getInstancia()
                    .exibirMensagem(mContext, MensagemDialogo.MENSAGEM_ALERTA,
                            "Atenção", "Informe primeiro a quantidade deste produto!");
        }
    }

    @SuppressWarnings("ConstantConditions")
    private void handleOnClickBtnAdicionarItem(int groupPosition, int childPosition) {
        final Produto produto = obtainProduto(groupPosition, childPosition);

        produto.setQuantidade(produto.getQuantidade() + 1);

        if (!mProdutosSelecionados.contains(produto)) {
            mProdutosSelecionados.add(produto);
            mReferenciaProdutosSelecionados.add(Pair.create(groupPosition, childPosition));
        }

        mExpandableItemManager.notifyChildItemChanged(groupPosition, childPosition);
    }

    @SuppressWarnings("ConstantConditions")
    private void handleOnClickBtnRemoverItem(int groupPosition, int childPosition) {
        final Produto produto = obtainProduto(groupPosition, childPosition);

        if (produto.getQuantidade() > 0) {
            produto.setQuantidade(produto.getQuantidade() - 1);
            if (produto.getQuantidade() == 0) {
                mReferenciaProdutosSelecionados.remove(mProdutosSelecionados.indexOf(produto));
                mProdutosSelecionados.remove(produto);
            }

            mExpandableItemManager.notifyChildItemChanged(groupPosition, childPosition);
        }
    }

    @SuppressWarnings("ConstantConditions")
    public void removerProdutoSelecionado(int posicao) {
        if (posicao >= 0 && posicao < mProdutosSelecionados.size()) {
            final Produto produtoSelecionado = mProdutosSelecionados.get(posicao);

            if (produtoSelecionado != null) {
                final Pair<Integer, Integer> groupChildReference =
                        mReferenciaProdutosSelecionados.get(posicao);
                final Produto produto = obtainProduto(groupChildReference.first,
                        groupChildReference.second);
                produto.setQuantidade(0);

                mReferenciaProdutosSelecionados.remove(mProdutosSelecionados.indexOf(produto));
                mProdutosSelecionados.remove(produto);

                mExpandableItemManager.notifyChildItemChanged(groupChildReference.first,
                        groupChildReference.second);
            }
        }
    }

    @SuppressWarnings("ConstantConditions")
    private Produto obtainProduto(int groupPosition, int childPosition) {
        return isFiltroGruposAtivado() ?
                mGruposFiltrados.get(groupPosition).getProdutos().get(childPosition) :
                mGrupos.get(groupPosition).getProdutos().get(childPosition);
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipeLayoutProduto;
    }

    @Override
    public void openItem(int position) {
        mItemManger.openItem(position);
    }

    @Override
    public void closeItem(int position) {
        mItemManger.closeItem(position);
    }

    @Override
    public void closeAllExcept(SwipeLayout layout) {
        mItemManger.closeAllExcept(layout);
    }

    @Override
    public void closeAllItems() {
        mItemManger.closeAllItems();
    }

    @Override
    public List<Integer> getOpenItems() {
        return mItemManger.getOpenItems();
    }

    @Override
    public List<SwipeLayout> getOpenLayouts() {
        return mItemManger.getOpenLayouts();
    }

    @Override
    public void removeShownLayouts(SwipeLayout layout) {
        mItemManger.removeShownLayouts(layout);
    }

    @Override
    public boolean isOpen(int position) {
        return mItemManger.isOpen(position);
    }

    @Override
    public Attributes.Mode getMode() {
        return mItemManger.getMode();
    }

    @Override
    public void setMode(Attributes.Mode mode) {
        mItemManger.setMode(mode);
    }

    public static class GruposViewHolder extends AbstractExpandableItemViewHolder {
        @BindView(R.id.imgIconeDepartamento) ImageView imgIconeDepartamento;
        @BindView(R.id.txtTituloGrupo) TextView txtTituloGrupo;
        @BindView(R.id.imgStatusGrupo) ImageView imgStatusGrupo;

        public GruposViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static class ProdutosViewHolder extends AbstractExpandableItemViewHolder {
        @BindView(R.id.swipeLayoutProduto) SwipeLayout swipeLayout;
        @BindView(R.id.txtTotalItem) TextView txtTotalItem;
        @BindView(R.id.txtDescricaoItem) TextView txtDescricaoItem;
        @BindView(R.id.txtValorUnitario) TextView txtValorUnitario;
        @BindView(R.id.txtValorTotalItem) TextView txtValorTotalItem;
        @BindView(R.id.btnAdicionarItem) ImageButton btnAdicionarItem;
        @BindView(R.id.btnRemoverItem) ImageButton btnRemoverItem;
        @BindView(R.id.lnlAdicional) LinearLayout lnlAdicional;
        @BindView(R.id.lnlObservacao) LinearLayout lnlObservacao;

        public ProdutosViewHolder(View itemView, View.OnClickListener clickListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            btnAdicionarItem.setOnClickListener(clickListener);
            btnRemoverItem.setOnClickListener(clickListener);
            lnlAdicional.setOnClickListener(clickListener);
            lnlObservacao.setOnClickListener(clickListener);
        }
    }

    private class ProdutosFilter extends Filter {
        @NonNull
        private final ProdutosAdapter mAdapter;

        private ProdutosFilter(@NonNull ProdutosAdapter adapter) {
            mAdapter = adapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            final FilterResults results = new FilterResults();

            if (constraint.length() == 0) {
                results.count = 0;
                results.values = null;
                return results;
            } else {
                final String filterText = constraint.toString().trim().toLowerCase();
                final Grupo grupoProdutosFiltrados =
                        new Grupo(CODIGO_GRUPO_FILTRADO, DESCRICAO_GRUPO_FILTRADO);

                for (Grupo grupo : mGrupos) {
                    for (Produto produto : grupo.getProdutos()) {
                        if (produto.getDescricao().toLowerCase().contains(filterText)
                                || produto.getCodigoAuxiliar().contains(filterText)) {
                            grupoProdutosFiltrados.getProdutos().add(produto);
                        }
                    }
                }

                results.count = 1;
                results.values = grupoProdutosFiltrados;
                return results;
            }

            /*FilterResults results = new FilterResults();
            List<Grupo> lista = new ArrayList<>();

            if (constraint != null && constraint.length() > 0) {
                Grupo registrosFiltro = new Grupo(CODIGO_GRUPO_FILTRADO, DESCRICAO_GRUPO_FILTRADO);

                constraint = constraint.toString().toLowerCase();
                for (int i = 0; i < mGrupos.size(); i++) {
                    Grupo grupo = mGrupos.get(i);
                    for (int x = 0; x < grupo.getProdutos().size(); x++) {
                        Produto produto = grupo.getProdutos().get(x);
                        if (produto.getDescricao().toLowerCase().contains(constraint.toString())) {
                            registrosFiltro.getProdutos().add(produto);
                        }
                    }
                }

                if (!registrosFiltro.getProdutos().isEmpty()) {
                    lista.add(registrosFiltro);
                }


            } else {
                lista = mGrupos;
            }

            results.count = lista.size();
            results.values = lista;
            return results;*/
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.values == null) {
                // Remove o filtro se o termo pesquisado for vazio ou se o adaptador estiver
                // sem itens
                mAdapter.removeFilter();
            } else {
                mAdapter.publishFilterResults((Grupo) results.values);
            }

            //final int size = mGruposFiltrados.size();
            //mGruposFiltrados = (List<Grupo>) results.values;

            //TODO o codigo abaixo lança exceção assim como notifyDataSetChanged()
            //mExpandableItemManager.notifyGroupItemRangeRemoved(0, size);
            //TODO este grupo deve estar expandido
            //mExpandableItemManager.notifyGroupItemInserted(0, true);
        }
    }

    /**
     * Remove o filtro do grupo e notifica os observadores da alteração realizada. Esta
     * operação implica em remover a referência do grupo filtrado e repreencher o adaptador
     * com a lista padrão dos grupos.
     */
    public void removeFilter() {
        desativarFiltroGrupos();
        notifyDataSetChanged();
    }

    /**
     * Desativa o filtro do grupo, repreenche com a nova lista e notifica os
     * observadores da alteração realizada.
     *
     * @param grupoProdutosFiltrados grupo com produtos filtrados.
     */
    @SuppressWarnings("ConstantConditions")
    public void publishFilterResults(@NonNull Grupo grupoProdutosFiltrados) {
        if (grupoProdutosFiltrados.getProdutos().isEmpty()) {
            if (isFiltroGruposAtivado()) {
                mGruposFiltrados.clear();
            }
        } else if (isFiltroGruposAtivado()) {
            mGruposFiltrados.clear();
            mGruposFiltrados.add(grupoProdutosFiltrados);
        } else {
            mGruposFiltrados = new ArrayList<>(Collections.singletonList(grupoProdutosFiltrados));
        }

        notifyDataSetChanged();
    }
}

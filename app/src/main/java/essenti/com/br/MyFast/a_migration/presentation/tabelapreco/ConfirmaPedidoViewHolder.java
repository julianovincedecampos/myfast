package essenti.com.br.MyFast.a_migration.presentation.tabelapreco;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import essenti.com.br.MyFast.R;

/**
 * @author Filipe Bezerra
 */
class ConfirmaPedidoViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.txtTotalItem) TextView txtTotalItem;
    @BindView(R.id.txtDescricaoItem) TextView txtDescricaoItem;
    @BindView(R.id.txtValorUnitario) TextView txtValorUnitario;
    @BindView(R.id.txtValorTotalItem) TextView txtValorTotalItem;
    @BindView(R.id.btnRemoverItem) ImageButton btnRemoverItem;

    ConfirmaPedidoViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}

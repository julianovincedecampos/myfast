package essenti.com.br.MyFast.mesa;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.produto.Produto;

/**
 * Created by diego_000 on 26/10/2015.
 */
public class TrocaItensMesaAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<Produto> itens;

    public TrocaItensMesaAdapter(Context context, List<Produto> itens) {
        this.itens = itens;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return this.itens.size();
    }

    @Override
    public Object getItem(int position) {
        return this.itens.get(position);
    }

    @Override
    public long getItemId(int position) {
        return this.itens.get(position).getCodigo();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_entrega, null);
        }
        final Produto produto = itens.get(position);
        final TextView txtDescricao = (TextView) convertView.findViewById(R.id.txtDescricao);
        final CheckBox chkSelecionado = (CheckBox) convertView.findViewById(R.id.chkSelecionado);
        txtDescricao.setText(String.format("(%03d) %s", produto.getQuantidade(), produto.getDescricao()));
        chkSelecionado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                produto.setSelecionado(!produto.getSelecionado());
                notifyDataSetChanged();
            }
        });

        txtDescricao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                produto.setSelecionado(!produto.getSelecionado());
                notifyDataSetChanged();
            }
        });

        chkSelecionado.setChecked(produto.getSelecionado());
        if (produto.getSelecionado()) {
            txtDescricao.setPaintFlags(txtDescricao.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            txtDescricao.setPaintFlags(txtDescricao.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
        }

        return convertView;
    }

    public List<Produto> getSelecionados() {
        List<Produto> lista = new ArrayList<Produto>();
        for (int i = 0; i < this.itens.size(); i++) {
            Produto item = this.itens.get(i);
            if (item.getSelecionado()) {
                lista.add(item);
            }
        }
        return lista;
    }
}
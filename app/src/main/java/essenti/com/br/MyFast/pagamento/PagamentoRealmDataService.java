package essenti.com.br.MyFast.pagamento;

import android.content.Context;
import essenti.com.br.MyFast.persistencia.AbstractRealmDataService;
import essenti.com.br.MyFast.rx.RealmObservable;
import io.realm.Realm;
import io.realm.RealmResults;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import rx.Observable;
import rx.functions.Func1;

public class PagamentoRealmDataService
        extends AbstractRealmDataService<Pagamento, PagamentoEntity> {

    public PagamentoRealmDataService(Context context, Class<PagamentoEntity> entityClass) {
        super(context, entityClass);
    }

    public Observable<List<Pagamento>> findByMesa(final int mesa) {
        return RealmObservable
                .results(mContext, new Func1<Realm, RealmResults<PagamentoEntity>>() {
                    @Override
                    public RealmResults<PagamentoEntity> call(Realm realm) {
                        return realm.where(mEntityClass)
                                .equalTo(PagamentoEntity.FIELD_MESA, mesa)
                                .findAll();
                    }
                })
                .map(new Func1<RealmResults<PagamentoEntity>, List<Pagamento>>() {
                    @Override
                    public List<Pagamento> call(RealmResults<PagamentoEntity> entities) {
                        if (entities == null || entities.isEmpty()) {
                            return Collections.emptyList();
                        }

                        final List<Pagamento> viewObjectList = new ArrayList<>();

                        for (PagamentoEntity entity : entities) {
                            viewObjectList.add(toViewObject(entity));
                        }

                        return viewObjectList;
                    }
                });
    }

    @Override
    protected PagamentoEntity toEntity(Pagamento viewObject) {
        return new PagamentoEntity(viewObject);
    }

    @Override
    protected Pagamento toViewObject(PagamentoEntity entity) {
        return new Pagamento(entity);
    }
}

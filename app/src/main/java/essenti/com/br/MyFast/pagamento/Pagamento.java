package essenti.com.br.MyFast.pagamento;

import com.google.gson.annotations.SerializedName;

import essenti.com.br.MyFast.util.Preconditions;
import essenti.com.br.MyFast.util.date.DateUtil;
import essenti.com.br.MyFast.util.number.NumberUtil;

/**
 * Classe que representa uma pagamento feito com cartão de crédito. Esta classe é usada
 * para visualizar os dados do pagamento na interface do usuário.
 *
 * @author Filipe Bezerra
 * @since 1.3.0
 */
public class Pagamento {
    public static final int DEFAULT_NO_ID = 0;

    private final int id;

    @SerializedName("Mesa")
    private final int mesa;

    @SerializedName("CodCardapio")
    private final int codCardapio;

    @SerializedName("Captura")
    private final String captura;

    @SerializedName("CodAutorizacao")
    private final String codAutorizacao;

    @SerializedName("CodProdutoMatriz")
    private final String codProdutoMatriz;

    @SerializedName("CodProdutoSecundario")
    private final String codProdutoSecundario;

    @SerializedName("EstAcquirer")
    private final String estAcquirer;

    @SerializedName("EstVenda")
    private final String estVenda;

    @SerializedName("Fluxo")
    private final String fluxo;

    @SerializedName("IdAplicacao")
    private final String idAplicacao;

    @SerializedName("IdTransacao")
    private final String idTransacao;

    @SerializedName("NomeProdutoMatriz")
    private final String nomeProdutoMatriz;

    @SerializedName("NomeProdutoSecundario")
    private final String nomeProdutoSecundario;

    @SerializedName("NSU")
    private final String nsu;

    @SerializedName("Referencia")
    private final String referencia;

    @SerializedName("RetornoAplicacao")
    private final String retornoAplicacao;

    @SerializedName("TipoTransacao")
    private final String tipoTransacao;

    @SerializedName("Valor")
    private final double valor;

    @SerializedName("VersaoAppFinanceira")
    private final String versaoAppFinanceira;

    @SerializedName("ComprovanteCliente")
    private final String comprovanteCliente;

    @SerializedName("ComprovanteVendedor")
    private final String comprovanteVendedor;

    @SerializedName("PAN")
    private final String pan;

    @SerializedName("EMail")
    private final String email;

    @SerializedName("NomeBandeira")
    private final String nomeBandeira;

    @SerializedName("Parcelas")
    private final int parcelas = 0;

    @SerializedName("DtPagamento")
    private final String dataPagamento;

    @SerializedName("CodCobranca")
    private final String codCobranca;

    public Pagamento(PagamentoCielo pagamentoCielo, int numeroMesa, int codCardapio) {
        this.id = DEFAULT_NO_ID;
        this.mesa = numeroMesa;
        this.codCardapio = codCardapio;
        this.captura = pagamentoCielo.captura;
        this.codAutorizacao = pagamentoCielo.codAutorizacao;
        this.codProdutoMatriz = pagamentoCielo.codProdutoMatriz;
        this.codProdutoSecundario = pagamentoCielo.codProdutoSecundario;
        this.estAcquirer = pagamentoCielo.estAcquirer;
        this.estVenda = pagamentoCielo.estVenda;
        this.fluxo = pagamentoCielo.fluxo;
        this.idAplicacao = pagamentoCielo.idAplicacao;
        this.idTransacao = pagamentoCielo.idTransacao;
        this.nomeProdutoMatriz = pagamentoCielo.nomeProdutoMatriz;
        this.nomeProdutoSecundario = pagamentoCielo.nomeProdutoSecundario;
        this.nsu = pagamentoCielo.nsu;
        this.referencia = pagamentoCielo.referencia;
        this.retornoAplicacao = pagamentoCielo.retornoAplicacao;
        this.tipoTransacao = pagamentoCielo.tipoTransacao;
        this.valor = NumberUtil.formatAsNumber(pagamentoCielo.valor);
        this.versaoAppFinanceira = pagamentoCielo.versaoAppFinanceira;
        this.comprovanteVendedor = pagamentoCielo.comprovanteVendedor;
        this.comprovanteCliente = pagamentoCielo.comprovanteCliente;
        this.pan = pagamentoCielo.pan;
        this.email = pagamentoCielo.email;
        this.nomeBandeira = pagamentoCielo.nomeBandeira;
        this.dataPagamento = null;
        this.codCobranca = null;
    }

    public Pagamento(PagamentoEntity entity) {
        Preconditions.checkNotNull(entity, "Cannot convert null PagamentoEntity to object.");
        id = entity.getId();
        idTransacao = entity.getIdTransacao();
        mesa = entity.getMesa();
        codCardapio = entity.getCodCardapio();
        captura = entity.getCaptura();
        codAutorizacao = entity.getCodAutorizacao();
        codProdutoMatriz = entity.getCodProdutoMatriz();
        codProdutoSecundario = entity.getCodProdutoSecundario();
        estAcquirer = entity.getEstAcquirer();
        estVenda = entity.getEstVenda();
        fluxo = entity.getFluxo();
        idAplicacao = entity.getIdAplicacao();
        nomeProdutoMatriz = entity.getNomeProdutoMatriz();
        nomeProdutoSecundario = entity.getNomeProdutoSecundario();
        nsu = entity.getNsu();
        referencia = entity.getReferencia();
        retornoAplicacao = entity.getRetornoAplicacao();
        tipoTransacao = entity.getTipoTransacao();
        valor = entity.getValor();
        versaoAppFinanceira = entity.getVersaoAppFinanceira();
        comprovanteCliente = entity.getComprovanteCliente();
        comprovanteVendedor = entity.getComprovanteVendedor();
        pan = entity.getPan();
        email = entity.getEmail();
        nomeBandeira = entity.getNomeBandeira();
        dataPagamento = entity.getDataPagamento();
        codCobranca = entity.getCodigoCobranca();
    }

    public Pagamento(String formaPagamento, double valorPagamento, int mesa, int codCardapio,
            String codCobranca) {
        this.id = DEFAULT_NO_ID;
        this.mesa = mesa;
        this.codCardapio = codCardapio;
        this.captura = "";
        this.codAutorizacao = "";
        this.codProdutoMatriz = "";
        this.codProdutoSecundario = "";
        this.estAcquirer = "";
        this.estVenda = "";
        this.fluxo = "";
        this.idAplicacao = "";
        this.idTransacao = "";
        this.nomeProdutoMatriz = formaPagamento;
        this.nomeProdutoSecundario = "";
        this.nsu = "";
        this.referencia = "";
        this.retornoAplicacao = "";
        this.tipoTransacao = "";
        this.valor = valorPagamento;
        this.versaoAppFinanceira = "";
        this.comprovanteVendedor = "";
        this.comprovanteCliente = "";
        this.pan = "";
        this.email = "";
        this.nomeBandeira = "";
        this.dataPagamento = DateUtil.formatCurrentDateAsString();
        this.codCobranca = codCobranca;
    }

    public Pagamento(String formaPagamento, double valorPagamento, String nomeBandeira,
            String nsu, String codAutorizacao,  int mesa, int codCardapio, String codCobranca) {
        this.id = DEFAULT_NO_ID;
        this.mesa = mesa;
        this.codCardapio = codCardapio;
        this.captura = "";
        this.codAutorizacao = codAutorizacao;
        this.codProdutoMatriz = "";
        this.codProdutoSecundario = "";
        this.estAcquirer = "";
        this.estVenda = "";
        this.fluxo = "";
        this.idAplicacao = "";
        this.idTransacao = "";
        this.nomeProdutoMatriz = formaPagamento;
        this.nomeProdutoSecundario = "";
        this.nsu = nsu;
        this.referencia = "";
        this.retornoAplicacao = "";
        this.tipoTransacao = "";
        this.valor = valorPagamento;
        this.versaoAppFinanceira = "";
        this.comprovanteVendedor = "";
        this.comprovanteCliente = "";
        this.pan = "";
        this.email = "";
        this.nomeBandeira = nomeBandeira;
        this.dataPagamento = DateUtil.formatCurrentDateAsString();
        this.codCobranca = codCobranca;
    }

    public int getId() {
        return id;
    }

    public int getMesa() {
        return mesa;
    }

    public int getCodCardapio() {
        return codCardapio;
    }

    public String getCaptura() {
        return captura;
    }

    public String getCodAutorizacao() {
        return codAutorizacao;
    }

    public String getCodProdutoMatriz() {
        return codProdutoMatriz;
    }

    public String getCodProdutoSecundario() {
        return codProdutoSecundario;
    }

    public String getEstAcquirer() {
        return estAcquirer;
    }

    public String getEstVenda() {
        return estVenda;
    }

    public String getFluxo() {
        return fluxo;
    }

    public String getIdAplicacao() {
        return idAplicacao;
    }

    public String getIdTransacao() {
        return idTransacao;
    }

    public String getNomeProdutoMatriz() {
        return nomeProdutoMatriz;
    }

    public String getNomeProdutoSecundario() {
        return nomeProdutoSecundario;
    }

    public String getNsu() {
        return nsu;
    }

    public String getReferencia() {
        return referencia;
    }

    public String getRetornoAplicacao() {
        return retornoAplicacao;
    }

    public String getTipoTransacao() {
        return tipoTransacao;
    }

    public double getValor() {
        return valor;
    }

    public String getVersaoAppFinanceira() {
        return versaoAppFinanceira;
    }

    public String getComprovanteCliente() {
        return comprovanteCliente;
    }

    public String getComprovanteVendedor() {
        return comprovanteVendedor;
    }

    public String getPan() {
        return pan;
    }

    public String getEmail() {
        return email;
    }

    public String getNomeBandeira() {
        return nomeBandeira;
    }

    public int getParcelas() {
        return parcelas;
    }

    public String getDataPagamento() {
        return dataPagamento;
    }

    public String getCodCobranca() {
        return codCobranca;
    }
}

package essenti.com.br.MyFast.a_migration.presentation.tabelapreco;

import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;
import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.android.asynctask.TaskCallback;
import essenti.com.br.MyFast.android.asynctask.TaskException;
import essenti.com.br.MyFast.android.widget.MensagemDialogo;
import essenti.com.br.MyFast.app.App;
import essenti.com.br.MyFast.mesa.Mesa;
import essenti.com.br.MyFast.mesa.MesaService;
import essenti.com.br.MyFast.mesa.PostPedido;
import essenti.com.br.MyFast.mesa.ResultadoObterMesa;
import essenti.com.br.MyFast.produto.Produto;
import essenti.com.br.MyFast.usuario.Usuario;
import essenti.com.br.MyFast.usuario.UsuarioHelper;
import essenti.com.br.MyFast.util.FeedbackHelper;
import essenti.com.br.MyFast.util.NetworkUtil;
import essenti.com.br.MyFast.util.eventbus.BusProvider;
import essenti.com.br.MyFast.webservice.ServiceGenerator;
import io.fabric.sdk.android.Fabric;
import java.util.ArrayList;
import java.util.List;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

import static essenti.com.br.MyFast.android.widget.MensagemDialogo.MENSAGEM_ALERTA;
import static essenti.com.br.MyFast.android.widget.MensagemDialogo.MENSAGEM_ERRO;
import static essenti.com.br.MyFast.android.widget.MensagemDialogo.MENSAGEM_SUCESSO;

/**
 * @author Filipe Bezerra
 */
public class ConfirmaPedidoFragment extends Fragment {

    private static final String EXTRA_PRODUTOS_SELECIONADOS =
            ConfirmaPedidoFragment.class.getSimpleName() + ".extraProdutosSelecionados";

    private static final int CODIGO_CARDAPIO = App.getInstancia().getCodigoCardapio();

    private static final int ERRO_CARREGAMENTO_INFO_MESA = 1;
    private static final int ERRO_ENVIANDO_PEDIDO = 2;

    @IntDef({ ERRO_CARREGAMENTO_INFO_MESA, ERRO_ENVIANDO_PEDIDO }) private @interface TipoError {
    }

    @BindView(R.id.input_layout_numero_mesa) protected TextInputLayout mTextInputLayoutNumeroMesa;
    @BindView(R.id.list) protected RecyclerView mRecyclerViewProdutosSelecionados;

    private List<Produto> mProdutosSelecionados;

    private ConfirmaPedidoAdapter mConfirmaPedidoAdapter;

    private Usuario mUsuarioLogado;

    private MesaService mMesaService;

    private MensagemDialogo mDialogoProgresso;

    public static ConfirmaPedidoFragment newInstance(List<Produto> pProdutos) {
        ConfirmaPedidoFragment fragment = new ConfirmaPedidoFragment();

        Bundle arguments = new Bundle();
        arguments.putParcelableArrayList(EXTRA_PRODUTOS_SELECIONADOS, new ArrayList<>(pProdutos));
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mProdutosSelecionados = getArguments().getParcelableArrayList(EXTRA_PRODUTOS_SELECIONADOS);
        mUsuarioLogado = UsuarioHelper.retrieveUsuarioLogado(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        final View fragmentView
                = inflater.inflate(R.layout.fragment_confirma_inclusao_produtos, container, false);
        ButterKnife.bind(this, fragmentView);
        return fragmentView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mRecyclerViewProdutosSelecionados.setHasFixedSize(true);
        mRecyclerViewProdutosSelecionados.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerViewProdutosSelecionados.setAdapter(
                mConfirmaPedidoAdapter = new ConfirmaPedidoAdapter(
                        getContext(), mProdutosSelecionados));
    }

    @OnClick(R.id.button_confirmar)
    void onClickButtonConfirmar() {
        mTextInputLayoutNumeroMesa.setError(null);

        if (mConfirmaPedidoAdapter.getItemCount() == 0) {
            App.getInstancia()
                    .exibirMensagem(getActivity(), MENSAGEM_ALERTA,
                            getString(R.string.titulo_atencao),
                            getString(R.string.mensagem_todos_itens_foram_removidos));
            return;
        }

        final String numeroMesaStr = mTextInputLayoutNumeroMesa.getEditText().getText().toString();
        if (TextUtils.isEmpty(numeroMesaStr)) {
            mTextInputLayoutNumeroMesa.setError(getString(R.string.error_numero_mesa_requerido));
            return;
        }

        requestAtualizacaoMesa(Integer.parseInt(numeroMesaStr));
    }

    private void requestAtualizacaoMesa(final int pNumeroMesa) {
        if (NetworkUtil.isDeviceConnectedToInternet(getContext())) {
            if (mMesaService == null) {
                mMesaService = ServiceGenerator.createService(MesaService.class, getContext());
            }

            if (mMesaService != null) {
                mMesaService.obterMesa(pNumeroMesa, CODIGO_CARDAPIO)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Subscriber<ResultadoObterMesa>() {
                                    @Override
                                    public void onStart() {
                                        showDialogoProgresso(
                                                getString(
                                                        R.string.mensagem_carregando_informacoes_mesa));
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        showError(e, ERRO_CARREGAMENTO_INFO_MESA);
                                    }

                                    @Override
                                    public void onNext(ResultadoObterMesa resultado) {
                                        App.getInstancia().setMesa(resultado.mesa);
                                        requestEnvioPedido(resultado.mesa);
                                    }

                                    @Override
                                    public void onCompleted() {
                                        dismissDialogoProgresso();
                                    }
                                }
                        );
            }
        } else {
            FeedbackHelper.showOfflineMessage(getContext(), null,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            requestAtualizacaoMesa(pNumeroMesa);
                        }
                    });
        }
    }

    private void requestEnvioPedido(final Mesa pMesa) {
        if (NetworkUtil.isDeviceConnectedToInternet(getContext())) {
            if (pMesa.getStatusMesa() == Mesa.MESA_EM_FECHAMENTO) {
                App.getInstancia()
                        .exibirMensagem(getActivity(), MENSAGEM_ALERTA,
                                getString(R.string.titulo_atencao),
                                getString(R.string.mensagem_mesa_em_fechamento));
                return;
            }

            final String matriculaGarcom = String.valueOf(mUsuarioLogado.getMatricula());

            new PostPedido(true, pMesa, matriculaGarcom, mProdutosSelecionados,
                    new TaskCallback<String>() {
                        @Override
                        public void onBegin() {
                            showDialogoProgresso(
                                    getString(R.string.mensagem_progresso_enviando_pedido));
                        }

                        @Override
                        public void onSuccess(String result) {
                            dismissDialogoProgresso();
                            App.getInstancia().zerarQuantidadeProdutos(mProdutosSelecionados);
                            App.getInstancia()
                                    .exibirMensagem(getActivity(), MENSAGEM_SUCESSO,
                                            getString(R.string.titulo_atencao),
                                            getString(R.string.mensagem_pedido_enviado),
                                            new MensagemDialogo.OnMensagemClickListener() {
                                                @Override
                                                public void onConfirmarClick() {
                                                    BusProvider.getInstance().post(
                                                            PedidoEnviadoEvent.newEvent());
                                                }

                                                @Override
                                                public void onCancelarClick() {}
                                            });
                        }

                        @Override
                        public void onResultNothing() {
                            dismissDialogoProgresso();
                        }

                        @Override
                        public void onError(TaskException e) {
                            showError(e, ERRO_ENVIANDO_PEDIDO);
                        }
                    }
            ).execute();
        } else {
            FeedbackHelper.showOfflineMessage(getContext(), null,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            requestEnvioPedido(pMesa);
                        }
                    });
        }
    }

    private void showDialogoProgresso(String mensagem) {
        if (mDialogoProgresso == null) {
            mDialogoProgresso = App.getInstancia().exibirProgresso(getActivity(),
                    "Processando", mensagem);
            mDialogoProgresso.exibir();
        } else {
            mDialogoProgresso.setTexto(mensagem);
        }
    }

    private void dismissDialogoProgresso() {
        if (mDialogoProgresso != null) {
            if (mDialogoProgresso.isExibindo()) {
                mDialogoProgresso.esconder();
            }

            mDialogoProgresso = null;
        }
    }

    private void showError(Throwable e, @TipoError int tipoErro) {
        dismissDialogoProgresso();

        if (Fabric.isInitialized()) {
            Crashlytics.logException(e);
        }

        String mensagem = getString(R.string.mensagem_ocorreu_falha);
        String when = "";
        switch (tipoErro) {
            case ERRO_CARREGAMENTO_INFO_MESA: {
                when = getString(R.string.mensagem_erro_carregando_informacoes_mesa);
                break;
            }
            case ERRO_ENVIANDO_PEDIDO:
                when = getString(R.string.mensagem_erro_enviando_pedido);
                break;
        }

        Timber.e(e, when);

        mensagem += when + getString(R.string.mensagem_complemento_ocorreu_falha);

        App.getInstancia().exibirMensagem(getActivity(), MENSAGEM_ERRO,
                getString(R.string.titulo_atencao), mensagem);
    }
}

package essenti.com.br.MyFast.a_migration.presentation.listacobranca;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.a_migration.domain.pojo.Cobranca;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Filipe Bezerra
 */
class ListaCobrancaAdapter extends RecyclerView.Adapter<ListaCobrancaViewHolder>
        implements Filterable {

    private final Context mContext;

    private List<Cobranca> mCobrancas;

    private ListaCobrancaFilter mFilter;

    private List<Cobranca> mCobrancasOriginalValues;

    ListaCobrancaAdapter(Context pContext, List<Cobranca> pCobrancas) {
        mContext = pContext;
        mCobrancas = pCobrancas;
    }

    @Override public ListaCobrancaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext)
                .inflate(R.layout.item_lista_cobranca, parent, false);
        return new ListaCobrancaViewHolder(itemView);
    }

    @Override public void onBindViewHolder(ListaCobrancaViewHolder holder, int position) {
        Cobranca cobranca = mCobrancas.get(position);
        holder.textViewNomeCobranca.setText(cobranca.getDescricao());
    }

    @Override public int getItemCount() {
        return mCobrancas.size();
    }

    @Override public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new ListaCobrancaFilter();
        }
        return mFilter;
    }

    public Cobranca getItem(int pPosition) {
        if (pPosition >= 0 && pPosition < mCobrancas.size()) {
            return mCobrancas.get(pPosition);
        }
        return null;
    }

    private class ListaCobrancaFilter extends Filter {
        @Override protected FilterResults performFiltering(CharSequence prefix) {
            final FilterResults results = new FilterResults();

            if (mCobrancasOriginalValues == null) {
                mCobrancasOriginalValues = new ArrayList<>(mCobrancas);
            }

            if (TextUtils.isEmpty(prefix)) {
                List<Cobranca> list = new ArrayList<>(mCobrancasOriginalValues);
                results.values = list;
                results.count = list.size();
            } else {
                final String prefixString = prefix.toString().toLowerCase();

                List<Cobranca> values = new ArrayList<>(mCobrancasOriginalValues);

                final List<Cobranca> newValues = new ArrayList<>();

                for(Cobranca cobranca : values) {
                    if (cobranca.getDescricao().toLowerCase().contains(prefixString)) {
                        newValues.add(cobranca);
                    }
                }

                results.values = newValues;
                results.count = newValues.size();
            }

            return results;
        }

        @Override protected void publishResults(CharSequence constraint, FilterResults results) {
            //noinspection unchecked
            mCobrancas = (List<Cobranca>) results.values;
            notifyDataSetChanged();
        }
    }
}

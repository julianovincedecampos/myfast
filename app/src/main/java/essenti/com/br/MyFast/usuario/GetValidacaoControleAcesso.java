package essenti.com.br.MyFast.usuario;

import android.net.Uri;
import android.support.annotation.NonNull;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.InputStream;

import essenti.com.br.MyFast.android.asynctask.AbstractAsyncTask;
import essenti.com.br.MyFast.android.asynctask.TaskCallback;
import essenti.com.br.MyFast.android.asynctask.TaskException;
import essenti.com.br.MyFast.app.App;

/**
 * Tarefa assíncrona para validação de controle de acesso via Web service.
 *
 * @author Filipe Bezerra
 * @version 1.4.0, 11/04/2016
 * @since 1.4.0
 */
public class GetValidacaoControleAcesso extends AbstractAsyncTask<Void, Void, String> {
    private static final String PATH_VALIDAR_CONTROLE_ACESSO
            = "ServiceUsuario.svc/ValidarControleAcesso";

    private final String mCodigoIdentificacao;
    private final String mMatriculaOperadorLogado;
    private final String mSenhaOperadorLogado;
    private final TipoAutenticacao mTipoAutenticacao;

    public GetValidacaoControleAcesso(String matricula, String senha, String codigoIdentificacao,
            TipoAutenticacao tipoAutenticacao, @NonNull TaskCallback<String> callback) {
        super(callback);
        mMatriculaOperadorLogado = matricula;
        mSenhaOperadorLogado = senha;
        mCodigoIdentificacao = codigoIdentificacao;
        mTipoAutenticacao = tipoAutenticacao;
    }

    @Override
    protected String doInBackground(Void... params) {
        final String url = Uri.parse(App.getInstancia().getURL())
                .buildUpon()
                .appendPath(PATH_VALIDAR_CONTROLE_ACESSO)
                .appendPath(mMatriculaOperadorLogado)
                .appendPath(mSenhaOperadorLogado)
                .appendPath(mCodigoIdentificacao)
                .appendPath(mTipoAutenticacao.getDescricao())
                .build()
                .toString();

        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(url);

        try {
            HttpResponse response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream instream = entity.getContent();
                String json = getStreamData(instream);
                instream.close();
                JSONObject objeto = new JSONObject(json);

                final String resultado = objeto.getString("ValidarControleAcessoResult");

                if (resultado.equals("OK")) {
                    return resultado;
                } else {
                    mException = TaskException.of(resultado);
                    return null;
                }
            }
        } catch (Exception e) {
            mException = TaskException.of(String
                    .format("Ocorreu um erro ao validar controle de acesso! Erro: %s",
                            e.getMessage()));
        }

        return null;
    }
}

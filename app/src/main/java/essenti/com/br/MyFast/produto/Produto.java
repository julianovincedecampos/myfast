package essenti.com.br.MyFast.produto;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hannesdorfmann.parcelableplease.annotation.ParcelablePlease;
import essenti.com.br.MyFast.atendimento.PedidoRapidoAdapter;
import java.util.ArrayList;
import java.util.List;

/**
 * Entidade Produto, pode representar tanto um produto por {@link Grupo} ou um produto avulso,
 * ou seja, não consta no cardápio. <br><br>
 *
 * Representa também um produto consumido em uma {@link essenti.com.br.MyFast.mesa.Mesa},
 * instanciando pelo construtor {@link #Produto(Produto, boolean)} passando segundo parâmetro
 * como {@code true}. Ao definir {@link #emModoPedidoRapido} é influenciado como o produto
 * é comparado com outra instância no método {@link #equals(Object)}.
 *
 * @author Filipe Bezerra, Diego
 * @version 1.3.1, 14/03/2016
 * @since 1.0.0
 */
@ParcelablePlease
public class Produto implements Parcelable {
    @SerializedName("Codprod") @Expose int codigo;

    @SerializedName("Descricao") @Expose String descricao;

    int quantidade;

    @SerializedName("Preco") @Expose double valorUnitario;

    boolean produtoEntregue;

    // Chave composta no WS para identificar unicamente um produto consumido na mesa
    int numeroSequencia;

    double precoComplemento;

    Boolean selecionado;

    String observacao;

    @SerializedName("Complementos") @Expose List<Complemento> complementos;

    @SerializedName("Codauxiliar") @Expose String codigoAuxiliar;

    // Código para controle interno no app, por exemplo em listas
    @SerializedName("Codigo") @Expose int sequencia;

    // Usado para determinar o produto como igual (método equals(Produto p)) somente
    // pelo código do produto
    boolean emModoPedidoRapido;

    public Produto() {
        this.codigo = 0;
        this.descricao = "";
        this.quantidade = 0;
        this.valorUnitario = 0;
        this.precoComplemento = 0;
        this.numeroSequencia = 0;
        this.selecionado = false;
        this.observacao = "";
        this.sequencia = 0;
    }

    public Produto(int codigo, String descricao, double valorUnitario) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.quantidade = 0;
        this.valorUnitario = valorUnitario;
        this.precoComplemento = 0;
        this.numeroSequencia = 0;
        this.selecionado = false;
        this.observacao = "";
        this.sequencia = 0;
    }

    /**
     * Construtor usado para fazer uma cópia do produto.
     *
     * @param origem produto origem para nova cópia.
     *
     * @see PedidoRapidoAdapter
     */
    public Produto(Produto origem, boolean emModoPedidoRapido) {
        this.codigo = origem.getCodigo();
        this.descricao = origem.getDescricao();
        this.quantidade = 0;
        this.valorUnitario = origem.getValorUnitario();
        this.precoComplemento = origem.getPrecoComplemento();
        this.numeroSequencia = origem.getNumeroSequencia();
        this.selecionado = false;
        this.observacao = "";
        this.sequencia = origem.getSequencia();
        this.codigoAuxiliar = origem.getCodigoAuxiliar();
        this.emModoPedidoRapido = emModoPedidoRapido;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    /**
     * Obtém o código sequencial de consumo na mesa.
     *
     * Este código é de uso para identificação do produto consumido em uma mesa pois
     * este produto poderá ser consumido inúmeras vezes em uma mesma mesa.
     *
     * Este código não tem relevância no uso dentro do aplicativo.
     *
     * @return código sequencial de consumo na mesa.
     */
    public int getNumeroSequencia() {
        return numeroSequencia;
    }

    /**
     * Define o código sequencial de consumo na mesa.
     *
     * @param numeroSequencia código sequencial de consumo.
     */
    public void setNumeroSequencia(int numeroSequencia) {
        this.numeroSequencia = numeroSequencia;
    }

    public double getPrecoComplemento() {
        return precoComplemento;
    }

    public void setPrecoComplemento(double precoComplemento) {
        this.precoComplemento = precoComplemento;
    }

    public boolean isProdutoEntregue() {
        return produtoEntregue;
    }

    public void setProdutoEntregue(boolean produtoEntregue) {
        this.produtoEntregue = produtoEntregue;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
        if (this.quantidade == 0)
        {
            this.observacao = "";
            for (int i = 0;i < this.getComplementos().size(); i++)
            {
                Complemento complemento = this.getComplementos().get(i);
                complemento.setQuantidade(0);
            }
        }
    }

    public double getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public double getValorTotal() {
        return this.getValorUnitario() * this.getQuantidade();
    }

    public Boolean getSelecionado() {
        return selecionado;
    }

    public void setSelecionado(Boolean selecionado) {
        this.selecionado = selecionado;
    }

    public List<Complemento> getComplementos() {
        if (this.complementos == null) {
            this.complementos = new ArrayList<Complemento>();
        }
        return this.complementos;
    }

    public String getCodigoAuxiliar() {
        return codigoAuxiliar;
    }

    public void setCodigoAuxiliar(String codigoAuxiliar) {
        this.codigoAuxiliar = codigoAuxiliar;
    }

    public int getSequencia() {
        return sequencia;
    }

    public void setSequencia(int sequencia) {
        this.sequencia = sequencia;
    }


    public boolean isEmModoPedidoRapido() {
        return emModoPedidoRapido;
    }

    /**
     * Define o modo como o Produto é avaliado no método {@link #equals(Object)}. Este
     * modo é usado pelo {@link PedidoRapidoAdapter} para
     * preencher sua lista interna de produtos de forma distintamente somente pelo
     * código do produto.
     */
    public Produto setEmModoPedidoRapido(boolean emModoPedidoRapido) {
        this.emModoPedidoRapido = emModoPedidoRapido;
        return this;
    }

    @Override
    public String toString() {
        return getCodigo() + " " + getDescricao();
    }

    @Override
    public boolean equals(Object o) {
        if (o != null && o instanceof Produto) {
            final Produto outroProduto = (Produto) o;

            if (isEmModoPedidoRapido()) {
                return outroProduto.getCodigo() == getCodigo();
            } else {
                return outroProduto.getCodigo() == getCodigo()
                        && outroProduto.getSequencia() == getSequencia();
            }
        }
        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ProdutoParcelablePlease.writeToParcel(this, dest, flags);
    }

    public static final Creator<Produto> CREATOR = new Creator<Produto>() {
        public Produto createFromParcel(Parcel source) {
            Produto target = new Produto();
            ProdutoParcelablePlease.readFromParcel(target, source);
            return target;
        }

        public Produto[] newArray(int size) {
            return new Produto[size];
        }
    };
}

package essenti.com.br.MyFast.a_migration.data.repositorio;

import essenti.com.br.MyFast.a_migration.data.util.RealmAutoIncrement;
import essenti.com.br.MyFast.pagamento.PagamentoEntity;
import io.realm.DynamicRealm;
import io.realm.DynamicRealmObject;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;

/**
 * @author Filipe Bezerra
 */
@SuppressWarnings("UnusedAssignment") public class RealmMigrationImpl implements RealmMigration {
    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        final RealmSchema schema = realm.getSchema();

        if (oldVersion == 0) {
            schema
                    .get(PagamentoEntity.class.getSimpleName())
                    .setRequired("idTransacao", false)
                    .transform(new RealmObjectSchema.Function() {
                        @Override
                        public void apply(DynamicRealmObject obj) {
                            if (obj.isNull("id") || obj.getInt("id") == 0) {
                                obj.setInt("id", RealmAutoIncrement
                                        .getInstance(PagamentoEntity.class)
                                        .getNextIdFromModel());
                            }
                        }
                    });
            oldVersion++;
        }
    }
}

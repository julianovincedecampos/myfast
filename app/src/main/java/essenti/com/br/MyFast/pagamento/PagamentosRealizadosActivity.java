package essenti.com.br.MyFast.pagamento;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.OnClick;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.android.activity.ActivityNavigator;
import essenti.com.br.MyFast.android.activity.BaseActivity;
import essenti.com.br.MyFast.android.widget.MensagemDialogo;
import essenti.com.br.MyFast.android.widget.MensagemDialogo.OnMensagemClickListener;
import essenti.com.br.MyFast.app.App;
import essenti.com.br.MyFast.cielo.CieloUtil;
import essenti.com.br.MyFast.mesa.Mesa;
import essenti.com.br.MyFast.mesa.MesaService;
import essenti.com.br.MyFast.mesa.ResultadoObterMesa;
import essenti.com.br.MyFast.util.FeedbackHelper;
import essenti.com.br.MyFast.util.NetworkUtil;
import essenti.com.br.MyFast.util.number.NumberUtil;
import essenti.com.br.MyFast.webservice.ServiceGenerator;
import io.fabric.sdk.android.Fabric;
import java.util.List;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import timber.log.Timber;

import static android.text.InputType.TYPE_CLASS_NUMBER;
import static android.text.InputType.TYPE_NUMBER_FLAG_DECIMAL;
import static essenti.com.br.MyFast.android.widget.MensagemDialogo.MENSAGEM_ALERTA;
import static essenti.com.br.MyFast.android.widget.MensagemDialogo.MENSAGEM_ERRO;
import static essenti.com.br.MyFast.android.widget.MensagemDialogo.MENSAGEM_SUCESSO;

/**
 * @author Filipe Bezerra
 */
public class PagamentosRealizadosActivity extends BaseActivity {

    private static final String TAG = PagamentosRealizadosActivity.class.getSimpleName();

    private static final int ERRO_CARREGAMENTO_INFO_MESA = 1;
    private static final int ERRO_ENVIO_PAGAMENTO = 2;
    private static final int ERRO_CARREGAMENTO_PAGAMENTOS = 3;
    private static final int ERRO_OBTENDO_ID_TRANSACAO = 4;

    @IntDef({
            ERRO_CARREGAMENTO_INFO_MESA,
            ERRO_ENVIO_PAGAMENTO,
            ERRO_CARREGAMENTO_PAGAMENTOS,
            ERRO_OBTENDO_ID_TRANSACAO
    })
    private @interface TipoError{}

    private static final int CODIGO_CARDAPIO = App.getInstancia().getCodigoCardapio();

    public static final String EXTRA_PAGAMENTO_PARCIAL = "extraPagamentoParcial";

    public static final int OPCAO_PAGAMENTO_CARTAO_INTEGRACAO_CIELO = 0;

    private static Gson sGson = new GsonBuilder().create();

    private PagamentoCartaoAdapter mPagamentoCartaoAdapter;

    private Mesa mMesaEmAtendimento;

    private MensagemDialogo mDialogoProgresso;

    private PagamentoRealmDataService mDataService;

    private PagamentoService mPagamentoService;

    private MesaService mMesaService;

    private int mResultCode = Activity.RESULT_CANCELED;

    private boolean mIsPagamentoParcial;

    @BindView(R.id.totalConsumido) TextView mTotalConsumidoView;
    @BindView(R.id.valorPago) TextView mValorPagoView;
    @BindView(R.id.saldo) TextView mSaldoView;
    @BindView(R.id.list) RecyclerView mListaPagamentosView;
    @BindView(R.id.container_ajuda) LinearLayout mContainerAjudaView;

    private void showExcecaoNegocio(String mensagem) {
        if (Fabric.isInitialized()) {
            Crashlytics.log(Log.WARN, TAG, mensagem);
        }

        Timber.w(mensagem);

        App.getInstancia().exibirMensagem(this, MENSAGEM_ALERTA, "Atenção", mensagem);
    }

    private void showExcecao(Throwable e, @TipoError int tipoErro) {
        dismissDialogoProgresso();

        if (Fabric.isInitialized()) {
            Crashlytics.logException(e);
        }

        String mensagem = "Infelizmente houve uma falha ";
        String when = "";
        switch (tipoErro) {
            case ERRO_CARREGAMENTO_INFO_MESA: {
                when = "ao carregar informações da mesa.";
                break;
            }
            case ERRO_ENVIO_PAGAMENTO: {
                when = "ao enviar pagamento registrado.";
                break;
            }
            case ERRO_CARREGAMENTO_PAGAMENTOS: {
                when = "ao carregar pagamentos efetuados.";
                break;
            }
            case ERRO_OBTENDO_ID_TRANSACAO: {
                when = "ao iniciar a transação do pagamento.";
                break;
            }
        }

        Timber.e(e, when);

        mensagem += when + "\nPor favor tente novamente. Esta falha será reportada para o "
                + "administrador do sistema!";

        App.getInstancia().exibirMensagem(this, MENSAGEM_ERRO, "Atenção", mensagem);
    }

    private void showDialogoProgresso(String mensagem) {
        if (mDialogoProgresso == null) {
            mDialogoProgresso = App.getInstancia().exibirProgresso(this,
                    "Processando", mensagem);
            mDialogoProgresso.exibir();
        } else {
            mDialogoProgresso.setTexto(mensagem);
        }
    }

    private void dismissDialogoProgresso() {
        if (mDialogoProgresso != null) {
            if (mDialogoProgresso.isExibindo()) {
                mDialogoProgresso.esconder();
            }

            mDialogoProgresso = null;
        }
    }

    private void displayTotaisMesa(Mesa mesa) {
        mMesaEmAtendimento = mesa;

        if (mMesaEmAtendimento != null) {
            mTotalConsumidoView
                    .setText(NumberUtil.formatAsCurrency(mMesaEmAtendimento.getValorTotal()));
            mValorPagoView
                    .setText(NumberUtil.formatAsCurrency(mMesaEmAtendimento.getValorPago()));
            mSaldoView
                    .setText(NumberUtil.formatAsCurrency(mMesaEmAtendimento.getSaldo()));
        }
    }

    private void requestAtualizacaoMesa() {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            if (mMesaService == null) {
                mMesaService = ServiceGenerator.createService(MesaService.class, this);
            }

            if (mMesaService != null) {
                mMesaService.obterMesa(mMesaEmAtendimento.getNumero(), CODIGO_CARDAPIO)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Subscriber<ResultadoObterMesa>() {
                                    @Override
                                    public void onStart() {
                                        showDialogoProgresso("Carregando informações da mesa...");
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        showExcecao(e, ERRO_CARREGAMENTO_INFO_MESA);
                                    }

                                    @Override
                                    public void onNext(ResultadoObterMesa resultado) {
                                        App.getInstancia().setMesa(resultado.mesa);
                                        displayTotaisMesa(resultado.mesa);
                                    }

                                    @Override
                                    public void onCompleted() {
                                        dismissDialogoProgresso();
                                    }
                                }
                        );
            }
        } else {
            FeedbackHelper.showOfflineMessage(this, null,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            requestAtualizacaoMesa();
                        }
                    });
        }
    }

    private void sendRespostaPagamento(final Pagamento pagamento) {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            if (mPagamentoService != null) {
                mPagamentoService.inserirPagamento(pagamento)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Subscriber<PagamentoService.InserirPagamentoResponse>() {
                                    @Override
                                    public void onStart() {
                                        showDialogoProgresso("Registrando pagamento...");
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        showExcecao(e, ERRO_ENVIO_PAGAMENTO);
                                    }

                                    @Override
                                    public void onNext(
                                            PagamentoService.InserirPagamentoResponse response) {
                                        if (response.result.equalsIgnoreCase("OK")) {
                                            mResultCode = App.RESULT_PAGAMENTO_EFETUADO;
                                            requestAtualizacaoMesa();
                                        } else {
                                            showExcecaoNegocio(response.result);
                                        }
                                    }

                                    @Override
                                    public void onCompleted() {
                                        dismissDialogoProgresso();
                                    }
                                }
                        );
            }
        } else {
            FeedbackHelper.showOfflineMessage(this, null,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            sendRespostaPagamento(pagamento);
                        }
                    });
        }
    }

    private void displayPagamento(Pagamento pagamento) {
        if (mPagamentoCartaoAdapter == null) {
            mPagamentoCartaoAdapter = new PagamentoCartaoAdapter(this, pagamento);
        } else {
            mPagamentoCartaoAdapter.addItem(pagamento);
        }

        mListaPagamentosView.setVisibility(View.VISIBLE);
        mContainerAjudaView.setVisibility(View.GONE);
    }

    private void persistRespostaPagamento(Pagamento pagamento) {
        mDataService.save(pagamento)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Action1<Pagamento>() {
                            @Override
                            public void call(Pagamento pagamento) {
                                displayPagamento(pagamento);
                                sendRespostaPagamento(pagamento);
                            }
                        }
                );
    }

    private void handleRespostaPagamento(PagamentoCielo pagamentoCielo) {
        final Pagamento pagamento = new Pagamento(pagamentoCielo,
                mMesaEmAtendimento.getNumero(), App.getInstancia().getCodigoCardapio());

        persistRespostaPagamento(pagamento);
    }

    private void showDialogRespostaPagamento(final PagamentoCielo pagamentoCielo) {
        String mensagemResultado;
        final boolean pagamentoAprovado;

        if (CieloUtil.isPaymentApproved(pagamentoCielo)) {
            pagamentoAprovado = true;
            mensagemResultado = String.format("Pagamento no %s com cartão %s:%s aprovado!",
                    pagamentoCielo.nomeProdutoSecundario,
                    pagamentoCielo.pan,
                    pagamentoCielo.nomeProdutoMatriz);
        } else {
            pagamentoAprovado = false;
            mensagemResultado = CieloUtil.obtainApplicationReturnMessage(pagamentoCielo);
        }

        if (!TextUtils.isEmpty(mensagemResultado)) {
            App.getInstancia()
                    .exibirMensagem(this, pagamentoAprovado ? MENSAGEM_SUCESSO : MENSAGEM_ERRO,
                            pagamentoAprovado ? "Aprovado" : "Não aprovado", mensagemResultado,
                            false, false, new OnMensagemClickListener() {
                                @Override
                                public void onConfirmarClick() {
                                    if (pagamentoAprovado) {
                                        handleRespostaPagamento(pagamentoCielo);
                                    }
                                }

                                @Override
                                public void onCancelarClick() {
                                }
                            }
                    );
        }
    }

    private void obtainRespostaPagamento(Intent intent) {
        final String jsonMensagem = intent.getData().getQueryParameter(
                CieloUtil.EXTRA_MESSAGE.toLowerCase());

        if (jsonMensagem != null) {
            final PagamentoCielo pagamento = sGson.fromJson(jsonMensagem, PagamentoCielo.class);
            showDialogRespostaPagamento(pagamento);
        }
    }

    private void startCieloMobile(final double valorPagamento, final Integer idTransacao) {
        if (CieloUtil.isCieloMobileInstalled(getPackageManager())) {
            if (valorPagamento > mMesaEmAtendimento.getSaldo()) {
                App.getInstancia().exibirMensagem(this, MENSAGEM_ALERTA, "Valor não aceito",
                        "Este valor está acima do saldo da mesa", new OnMensagemClickListener() {
                            @Override
                            public void onConfirmarClick() {
                                readValorPagamento(idTransacao);
                            }

                            @Override
                            public void onCancelarClick() {
                            }
                        });
                return;
            }

            final String mensagemVenda = CieloUtil.obtainTransactionFromJson(
                    idTransacao, valorPagamento);

            final Uri uri = Uri.parse(CieloUtil.URI_STRING);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri)
                    .putExtra(CieloUtil.EXTRA_MESSAGE, mensagemVenda)
                    .setPackage(CieloUtil.PACKAGE_NAME);
            startActivity(intent);
        }
    }

    private void readValorPagamento(final int idTransacao) {
        new MaterialDialog.Builder(PagamentosRealizadosActivity.this)
                .title("Confirme o valor do pagamento")
                .content("Exemplo: 100.00")
                .inputType(TYPE_CLASS_NUMBER | TYPE_NUMBER_FLAG_DECIMAL)
                .input("Valor do pagamento",
                        NumberUtil.formatWithDot(mMesaEmAtendimento.getSaldo()),
                        false, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(@NonNull MaterialDialog dialog,
                                    CharSequence input) {
                                startCieloMobile(Double.valueOf(input.toString()), idTransacao);
                            }
                        })
                .positiveText("CONFIRMAR")
                .negativeText("CANCELAR")
                .show();
    }

    private void requestListaPagamentos() {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            if (mPagamentoService != null) {
                mPagamentoService
                        .listarPagamentos(mMesaEmAtendimento.getNumero(), CODIGO_CARDAPIO)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                new Subscriber<PagamentoService.ListarPagamentoResponse>() {
                                    @Override
                                    public void onStart() {
                                        showDialogoProgresso("Obtendo pagamentos efetuados...");
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        showExcecao(e, ERRO_CARREGAMENTO_PAGAMENTOS);
                                    }

                                    @Override
                                    public void onNext(
                                            PagamentoService.ListarPagamentoResponse response) {
                                        persistListaPagamentos(response.result);
                                    }

                                    @Override
                                    public void onCompleted() {
                                        dismissDialogoProgresso();
                                    }
                                }
                        );
            }
        } else {
            FeedbackHelper.showOfflineMessage(this, null,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(
                                @NonNull MaterialDialog d, @NonNull DialogAction action) {
                            requestListaPagamentos();
                        }
                    });
        }
    }

    private void persistListaPagamentos(List<Pagamento> pagamentos) {
        mDataService.saveAll(pagamentos)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Action1<List<Pagamento>>() {
                            @Override
                            public void call(List<Pagamento> pagamentos) {
                                displayListaPagamentos(pagamentos);
                            }
                        }
                );
    }

    private void displayListaPagamentos(List<Pagamento> pagamentos) {
        if (pagamentos == null || pagamentos.isEmpty()) {
            if (mPagamentoCartaoAdapter != null) {
                mPagamentoCartaoAdapter.clearData();
            }

            mListaPagamentosView.setVisibility(View.GONE);
            mContainerAjudaView.setVisibility(View.VISIBLE);
        } else {
            if (mPagamentoCartaoAdapter == null) {
                mListaPagamentosView.setAdapter(mPagamentoCartaoAdapter =
                        new PagamentoCartaoAdapter(PagamentosRealizadosActivity.this, pagamentos));
            } else {
                mPagamentoCartaoAdapter.swapItems(pagamentos);
            }

            mListaPagamentosView.setVisibility(View.VISIBLE);
            mContainerAjudaView.setVisibility(View.GONE);
        }
    }

    @Override
    protected int provideLayoutResource() {
        return R.layout.activity_pagamentos_realizados;
    }

    @Override
    protected boolean shouldDisplayHomeAsUpEnabled() {
        return true;
    }

    @Override
    protected int provideUpIndicator() {
        return R.drawable.ic_arrow_back_white_24dp;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!getIntent().hasExtra(App.EXTRA_MESA_ATENDIMENTO)) {
            Toast.makeText(getApplicationContext(), "A mesa em atendimento não foi "
                            + "passada via Intent.putExtra(App.EXTRA_MESA_ATENDIMENTO, Parcelable)",
                    Toast.LENGTH_LONG).show();
            finish();
        }

        Timber.tag(TAG);

        mMesaEmAtendimento = getIntent().getParcelableExtra(App.EXTRA_MESA_ATENDIMENTO);
        mIsPagamentoParcial = getIntent().getBooleanExtra(EXTRA_PAGAMENTO_PARCIAL, false);

        mListaPagamentosView.setHasFixedSize(true);
        mListaPagamentosView.setLayoutManager(new LinearLayoutManager(this));

        mDataService = new PagamentoRealmDataService(this, PagamentoEntity.class);

        mPagamentoService = ServiceGenerator.createService(PagamentoService.class, this);

        displayTotaisMesa(mMesaEmAtendimento);
        retrieveListaPagamentos();
    }

    private void retrieveListaPagamentos() {
        mDataService.findByMesa(mMesaEmAtendimento.getNumero())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Action1<List<Pagamento>>() {
                            @Override
                            public void call(List<Pagamento> pagamentos) {
                                displayListaPagamentos(pagamentos);
                                requestListaPagamentos();
                            }
                        }
                );
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.getData() != null) {
            obtainRespostaPagamento(intent);
        }
    }

    @OnClick(R.id.btnNovoPagamento)
    void onClickBtnNovoPagamento() {
        if (!(mMesaEmAtendimento.getSaldo() > 0)) {
            new MaterialDialog.Builder(this)
                    .title(R.string.dialog_title_sem_saldo)
                    .content(R.string.dialog_content_sem_saldo)
                    .positiveText(android.R.string.ok)
                    .show();
            return;
        }

        new MaterialDialog.Builder(this)
                .title(R.string.dialog_title_selecione_opcao_pagamento)
                .items(R.array.opcoes_pagamento)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(
                            MaterialDialog d, View itemView, int which, CharSequence text) {
                        if (which == OPCAO_PAGAMENTO_CARTAO_INTEGRACAO_CIELO) {
                            startPagamentoCartaoIntegracaoCielo();
                        } else {
                            ActivityNavigator.startListaCobrancaActivity(
                                    PagamentosRealizadosActivity.this, mMesaEmAtendimento);
                        }
                        return false;
                    }
                })
                .positiveText(R.string.dialog_select)
                .show();
    }

    private void startPagamentoCartaoIntegracaoCielo() {
        if (CieloUtil.isCieloMobileInstalled(getPackageManager())) {
            if (NetworkUtil.isDeviceConnectedToInternet(this)) {
                if (mPagamentoService != null) {
                    mPagamentoService.obterTransacaoPagamento()
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    new Subscriber<PagamentoService.ObterTransacaoPagamentoResponse>() {
                                        @Override
                                        public void onStart() {
                                            showDialogoProgresso("Obtendo identificador da transação...");
                                        }

                                        @Override
                                        public void onError(Throwable e) {
                                            showExcecao(e, ERRO_OBTENDO_ID_TRANSACAO);
                                        }

                                        @Override
                                        public void onNext(
                                                PagamentoService.ObterTransacaoPagamentoResponse response) {
                                            readValorPagamento(Integer.parseInt(response.result));
                                        }

                                        @Override
                                        public void onCompleted() {
                                            dismissDialogoProgresso();
                                        }
                                    }
                            );
                } else {
                    FeedbackHelper.showOfflineMessage(this, null,
                            new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(
                                        @NonNull MaterialDialog d, @NonNull DialogAction action) {
                                    startPagamentoCartaoIntegracaoCielo();
                                }
                            });
                }
            }
        } else {
            App.getInstancia().exibirMensagem(this, MENSAGEM_ERRO, "Aplicativo não instalado",
                    "O aplicativo Cielo Mobile não está instalado. Não é possível continuar.");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == App.RESULT_PAGAMENTO_EFETUADO
                && requestCode == ActivityNavigator.REQUEST_OUTROS_PAGAMENTOS) {
            mResultCode = App.RESULT_PAGAMENTO_EFETUADO;
            requestAtualizacaoMesa();
            requestListaPagamentos();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.btnConcluir)
    void onClickBtnConcluir() {
        finish();
    }

    @Override
    public void finish() {
        setResult(mResultCode);
        super.finish();
    }
}
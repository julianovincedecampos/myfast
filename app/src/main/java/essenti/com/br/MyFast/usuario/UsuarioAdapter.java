package essenti.com.br.MyFast.usuario;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import essenti.com.br.MyFast.R;
import java.util.Locale;

public class UsuarioAdapter extends RecyclerView.Adapter<UsuarioAdapter.UsuarioViewHolder>
        implements Filterable {
    @NonNull private List<Usuario> mUsuarios;
    @NonNull private Context mContext;

    private List<Usuario> mOriginalValues;
    private final Object mLock = new Object();
    private UsuarioFilter mUsuarioFilter;

    public UsuarioAdapter(@NonNull List<Usuario> usuarios, @NonNull Context context) {
        mUsuarios = usuarios;
        mContext = context;
    }

    @Override
    public UsuarioViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_lista_operador,
                parent, false);
        return new UsuarioViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(UsuarioViewHolder holder, int position) {
        final Usuario usuario = mUsuarios.get(position);
        holder.txtNomeOperador.setText(String.format(Locale.getDefault(), "%d - %s",
                usuario.getMatricula(), usuario.getNome()));
    }

    @Override
    public int getItemCount() {
        return mUsuarios.size();
    }

    public Usuario getItem(int position) {
        if (position < 0 || position >= mUsuarios.size()) {
            return null;
        }

        return mUsuarios.get(position);
    }

    @Override
    public Filter getFilter() {
        if (mUsuarioFilter == null) {
            mUsuarioFilter = new UsuarioFilter();
        }
        return mUsuarioFilter;
    }

    class UsuarioViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtNomeOperador) TextView txtNomeOperador;

        public UsuarioViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private class UsuarioFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            FilterResults results = new FilterResults();

            if (mOriginalValues == null) {
                synchronized (mLock) {
                    mOriginalValues = new ArrayList<>(mUsuarios);
                }
            }

            if (TextUtils.isEmpty(prefix)) {
                List<Usuario> list;
                synchronized (mLock) {
                    list = new ArrayList<>(mOriginalValues);
                }
                results.values = list;
                results.count = list.size();
            } else {
                final String prefixString = prefix.toString().toLowerCase();

                List<Usuario> values;
                synchronized (mLock) {
                    values = new ArrayList<>(mOriginalValues);
                }

                final List<Usuario> newValues = new ArrayList<>();

                for(Usuario usuario : values) {
                    final String valueText = String.valueOf(usuario.getMatricula());

                    if (valueText.contains(prefixString)) {
                        newValues.add(usuario);
                    }
                }

                results.values = newValues;
                results.count = newValues.size();
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            //noinspection unchecked
            mUsuarios = (List<Usuario>) results.values;
            notifyDataSetChanged();
        }
    }
}

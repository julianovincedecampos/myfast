package essenti.com.br.MyFast.util.log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Métodos utilitários para auxiliar na depuração e obtenção de logs.
 *
 * @author Filipe Bezerra
 * @version 1.2.2, 06/01/2016
 * @since 1.2.2
 */
public class LogUtil {
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    private LogUtil() {
    }

    public static String obtainPrettyPrintingOf(final Object obj) {
        return GSON.toJson(obj);
    }
}

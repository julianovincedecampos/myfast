package essenti.com.br.MyFast.mesa;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import essenti.com.br.MyFast.android.asynctask.AbstractAsyncTask;
import essenti.com.br.MyFast.android.asynctask.TaskCallback;
import essenti.com.br.MyFast.android.asynctask.TaskException;
import essenti.com.br.MyFast.app.App;
import essenti.com.br.MyFast.produto.Produto;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Classe de execução assíncrona para comunicar com Web service e obter a lista itens
 * consumidos da {@link essenti.com.br.MyFast.mesa.Mesa}.
 *
 * @author Filipe Bezerra
 * @version 1.4.0, 05/04/2016
 * @since 1.4.0
 */
public class GetItensMesaTask extends AbstractAsyncTask<Integer, Void, List<Produto>> {
    private static final String PATH_OBTER_ITENS_MESAS = "ServiceMesa.svc/ItensMesas";

    public GetItensMesaTask(@NonNull TaskCallback<List<Produto>> callback) {
        super(callback);
    }

    @Override
    protected List<Produto> doInBackground(Integer... params) {
        if (!checkParamsNotNull(params)) {
            return null;
        }

        if (params.length != 1) {
            mException = TaskException.of(
                    new IllegalArgumentException("A lista de argumentos está incorreta. " +
                            "Deve haver somente o argumento \"Número da mesa\"."));
            return null;
        }

        final Integer codigoMesa = params[0];

        final String url = Uri.parse(App.getInstancia().getURL())
                .buildUpon()
                .appendPath(PATH_OBTER_ITENS_MESAS)
                .appendPath(String.valueOf(codigoMesa))
                .appendPath(String.valueOf(App.getInstancia().getCodigoCardapio()))
                .build()
                .toString();

        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(url);

        try {
            HttpResponse response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream instream = entity.getContent();
                String json = getStreamData(instream);
                instream.close();
                return convertJsonToProdutos(json);
            }
        } catch (JSONException | IOException e) {
            mException = TaskException.of(e);
        }

        return null;
    }

    private List<Produto> convertJsonToProdutos(String jsonString) throws JSONException {
        if (TextUtils.isEmpty(jsonString)) {
            return Collections.emptyList();
        }

        final JSONObject jsonObject = new JSONObject(jsonString);

        if (jsonObject.isNull("ListarItensMesasResult")) {
            return Collections.emptyList();
        }

        final JSONArray jsonArrayProdutoAvulso = jsonObject.getJSONArray("ListarItensMesasResult");

        final List<Produto> itensConsumidos = new ArrayList<>();
        JSONObject jsonItemConsumido;

        for (int i = 0; i < jsonArrayProdutoAvulso.length(); i++) {
            jsonItemConsumido = jsonArrayProdutoAvulso.getJSONObject(i);
            Produto produto = new Produto();
            produto.setCodigoAuxiliar(jsonItemConsumido.getString("CodAuxiliar"));
            produto.setCodigo(jsonItemConsumido.getInt("Codigo"));
            produto.setDescricao(jsonItemConsumido.getString("Descricao"));

            produto.setSequencia(jsonItemConsumido.getInt("Numseq"));
            produto.setNumeroSequencia(jsonItemConsumido.getInt("Numseq"));

            produto.setValorUnitario(jsonItemConsumido.getDouble("Preco"));
            produto.setPrecoComplemento(jsonItemConsumido.getDouble("PrecoComplemento"));
            produto.setQuantidade(jsonItemConsumido.getInt("Quantidade"));

            itensConsumidos.add(produto);
        }

        return itensConsumidos;
    }
}

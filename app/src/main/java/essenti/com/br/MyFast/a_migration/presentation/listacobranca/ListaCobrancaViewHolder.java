package essenti.com.br.MyFast.a_migration.presentation.listacobranca;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import essenti.com.br.MyFast.R;

/**
 * @author Filipe Bezerra
 */
class ListaCobrancaViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.text_view_nome_cobranca) TextView textViewNomeCobranca;

    ListaCobrancaViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}

package essenti.com.br.MyFast.atendimento;

import android.app.AlertDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import essenti.com.br.MyFast.app.App;
import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.produto.ComplementoAdapter;
import essenti.com.br.MyFast.android.widget.MensagemDialogo;
import essenti.com.br.MyFast.produto.Produto;

public class PedidoRapidoAdapter extends RecyclerView.Adapter<PedidoRapidoAdapter.ViewHolder> {
    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtTotalItem) TextView txtTotalItem;
        @BindView(R.id.txtDescricaoItem) TextView txtDescricaoItem;
        @BindView(R.id.txtValorUnitario) TextView txtValorUnitario;
        @BindView(R.id.txtValorTotalItem) TextView txtValorTotalItem;
        @BindView(R.id.btnAdicionarItem) ImageButton btnAdicionarItem;
        @BindView(R.id.btnRemoverItem) ImageButton btnRemoverItem;
        @BindView(R.id.lnlAdicional) LinearLayout lnlAdicional;
        @BindView(R.id.lnlObservacao) LinearLayout lnlObservacao;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    private Context mContext;
    @NonNull
    private List<Produto> mProdutosConsumidos;

    public PedidoRapidoAdapter(@NonNull Context context, @NonNull List<Produto> itensConsumidos) {
        mContext = context;

        mProdutosConsumidos = new ArrayList<>();
        for (Produto itemConsumido : itensConsumidos) {
            final Produto copiaProduto = new Produto(itemConsumido, true);

            if (!mProdutosConsumidos.contains(copiaProduto)) {
                mProdutosConsumidos.add(copiaProduto);
            }
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_pedido_rapido, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Produto produto = mProdutosConsumidos.get(position);

        holder.txtTotalItem
                .setText(String.format(Locale.getDefault(), "%03d", produto.getQuantidade()));
        holder.txtDescricaoItem
                .setText(produto.getDescricao());
        holder.txtValorUnitario
                .setText(String.format("Valor Unit. %s",
                        NumberFormat.getCurrencyInstance()
                                .format(produto.getValorUnitario())));
        holder.txtValorTotalItem.setText(
                NumberFormat.getCurrencyInstance()
                        .format(produto.getValorTotal()));

        holder.btnAdicionarItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                produto.setQuantidade(produto.getQuantidade() + 1);
                notifyItemChanged(position);
            }
        });

        holder.btnRemoverItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (produto.getQuantidade() > 0) {
                    produto.setQuantidade(produto.getQuantidade() - 1);
                    notifyItemChanged(position);
                }
            }
        });

        holder.lnlAdicional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (produto.getQuantidade() > 0) {
                    if (!produto.getComplementos().isEmpty()) {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                        final View view = LayoutInflater.from(mContext)
                                .inflate(R.layout.dialog_selecionar_complemento_produto, null);

                        ImageButton btnConfirmar = (ImageButton) view
                                .findViewById(R.id.btnConfirmar);
                        ImageView imgFechar = (ImageView) view.findViewById(R.id.imgFechar);

                        final ListView listViewItens = (ListView) view
                                .findViewById(R.id.listViewItens);
                        final ComplementoAdapter complementoArrayAdapter =
                                new ComplementoAdapter(mContext, produto.getComplementos());
                        listViewItens.setAdapter(complementoArrayAdapter);

                        builder.setView(view);
                        final AlertDialog dialog = builder.create();

                        imgFechar.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        btnConfirmar.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();

                            }
                        });
                        dialog.show();
                    } else {
                        App.getInstancia().exibirMensagem(mContext,
                                MensagemDialogo.MENSAGEM_ALERTA,
                                "Atenção", "Não existe adicional para este produto!");
                    }
                } else {
                    App.getInstancia().exibirMensagem(mContext,
                            MensagemDialogo.MENSAGEM_ALERTA,
                            "Atenção", "Informe primeiro a quantidade deste produto!");
                }
            }
        });

        holder.lnlObservacao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (produto.getQuantidade() > 0) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    final View view = LayoutInflater.from(mContext)
                            .inflate(R.layout.dialog_observacao_produto, null);

                    ImageButton btnConfirmar = (ImageButton) view
                            .findViewById(R.id.btnConfirmar);
                    ImageView imgFechar = (ImageView) view.findViewById(R.id.imgFechar);
                    final EditText edtObservacoes = (EditText) view
                            .findViewById(R.id.edtObservacoes);
                    edtObservacoes.setText(produto.getObservacao());
                    builder.setView(view);
                    final AlertDialog dialog = builder.create();

                    imgFechar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    btnConfirmar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            produto.setObservacao(edtObservacoes.getText().toString());
                            dialog.dismiss();

                        }
                    });
                    dialog.show();
                } else {
                    App.getInstancia().exibirMensagem(mContext,
                            MensagemDialogo.MENSAGEM_ALERTA,
                            "Atenção", "Informe primeiro a quantidade deste produto!");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mProdutosConsumidos.size();
    }

    public List<Produto> getProdutosSelecionados() {
        List<Produto> produtosSelecionados = new ArrayList<>();
        for (Produto produto : mProdutosConsumidos) {
            if (produto.getQuantidade() > 0) {
                produtosSelecionados.add(produto);
            }
        }

        return produtosSelecionados;
    }

    public void removerProdutoSelecionado(int posicao) {
        final List<Produto> produtosSelecionados = getProdutosSelecionados();

        if (posicao < 0 && posicao >= produtosSelecionados.size()) {
            return;
        }

        final Produto produtoSelecionado = produtosSelecionados.get(posicao);

        //noinspection ConstantConditions
        final Produto produtoPedidoRapido = mProdutosConsumidos.get(
                mProdutosConsumidos.indexOf(produtoSelecionado));

        produtoPedidoRapido.setQuantidade(0);
        notifyDataSetChanged();
    }
}

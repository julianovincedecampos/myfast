package essenti.com.br.MyFast.ambiente;

/**
 * MyFast.Android
 *
 * @author Filipe Bezerra on 04/12/2015.
 */
public class Ambiente {
    private int codigo;
    private String descricao;

    public Ambiente(int codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}

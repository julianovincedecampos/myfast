package essenti.com.br.MyFast.app;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import essenti.com.br.MyFast.util.FeedbackHelper;
import essenti.com.br.MyFast.util.NetworkUtil;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import essenti.com.br.MyFast.R;
import essenti.com.br.MyFast.ambiente.Ambiente;
import essenti.com.br.MyFast.ambiente.AmbientesActivity;
import essenti.com.br.MyFast.android.widget.MensagemDialogo;
import essenti.com.br.MyFast.cardapio.Cardapio;
import essenti.com.br.MyFast.cardapio.CardapiosActivity;

import static essenti.com.br.MyFast.app.App.AMBIENTE_SELECIONADO;
import static essenti.com.br.MyFast.app.App.CARDAPIO_SELECIONADO;
import static essenti.com.br.MyFast.app.App.SELECIONAR_AMBIENTE;
import static essenti.com.br.MyFast.app.App.SELECIONAR_CARDAPIO;

/**
 * Controlador da tela Configurações. Esta tela apresenta configuração do Web service,
 * {@link Cardapio} padrão,
 * {@link Ambiente} padrão e a forma de pagamento utilizada.
 *
 * @author Diego, Filipe Bezerra
 * @version 1.4.0, 16/03/2016
 * @since 1.0.0
 */
public class ConfiguracaoActivity extends AppCompatActivity {
    public static final String EXTRA_CODIGO_CARDAPIO = "CodigoCardapio";
    public static final String EXTRA_CODIGO_AMBIENTE = "CodigoAmbiente";

    @BindView(R.id.txtUrlWS) EditText mTxtUrlWS;
    @BindView(R.id.btnConfiguraCardapio) Button mBtnConfiguraCardapio;
    @BindView(R.id.btnConfiguraAmbiente) Button mBtnConfiguraAmbiente;
    @BindView(R.id.btnConfiguraPagamentoMobile) Button mBtnConfiguraPagamentoMobile;

    private int mCodigoCardapio;

    private int mCodigoAmbiente;

    private int mIndiceFormaPagamento = 0;

    private boolean mOpcaoPermitirAlterarGarcomHabilitada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracao);
        ButterKnife.bind(this);

        final String url = App.getInstancia().getURL();
        mTxtUrlWS.setText(url != null ? url : "");

        mCodigoCardapio = App.getInstancia().getCodigoCardapio();
        mCodigoAmbiente = App.getInstancia().getCodigoAmbiente();
        mIndiceFormaPagamento = App.getInstancia().getIndicePagamentoMobile();
        mOpcaoPermitirAlterarGarcomHabilitada = App.getInstancia().isPermitidoAlterarGarcom();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case SELECIONAR_CARDAPIO:
                if (resultCode == CARDAPIO_SELECIONADO) {
                    if (data.hasExtra(EXTRA_CODIGO_CARDAPIO)) {
                        mCodigoCardapio = data.getExtras().getInt(EXTRA_CODIGO_CARDAPIO);
                        mBtnConfiguraAmbiente.setEnabled(true);
                    } else {
                        mBtnConfiguraAmbiente.setEnabled(false);
                    }
                }
                break;

            case SELECIONAR_AMBIENTE:
                if ((resultCode == AMBIENTE_SELECIONADO)
                        && (data.hasExtra(EXTRA_CODIGO_AMBIENTE))) {
                    mCodigoAmbiente = data.getExtras().getInt(EXTRA_CODIGO_AMBIENTE);
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnEditorAction(R.id.txtUrlWS)
    public boolean onEditorActionTxtUrlWS(int actionId) {
        if (actionId == R.id.testar || actionId == EditorInfo.IME_NULL) {
            testarConexao();
            return true;
        }
        return false;
    }

    @OnClick(R.id.btnTestaConexao)
    public void onClickBtnTestaConexao() {
        testarConexao();
    }

    private void testarConexao() {
        if (!TextUtils.isEmpty(mTxtUrlWS.getText())) {
            final StringBuilder urlBuilder = new StringBuilder(
                    mTxtUrlWS.getText().toString().replaceAll("\\s", ""));

            if (!urlBuilder.toString().endsWith("/")) {
                urlBuilder.append("/");
            }

            final String url = urlBuilder.toString();
            if (!url.equals(mTxtUrlWS.getText().toString())) {
                mTxtUrlWS.setText(url);
            }

            requestTesteConexao(url);
        } else {
            App.getInstancia().exibirMensagem(ConfiguracaoActivity.this,
                    MensagemDialogo.MENSAGEM_ERRO, "Atenção", "É necessário informar o endereço!");
        }
    }

    private void requestTesteConexao(final String url) {
        if (NetworkUtil.isDeviceConnectedToInternet(this)) {
            new TestaConexaoWsTask().execute(url);
        } else {
            FeedbackHelper.showOfflineMessage(this,
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            finish();
                        }
                    },
                    new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog,
                                @NonNull DialogAction which) {
                            requestTesteConexao(url);
                        }
                    });
        }
    }

    @OnClick(R.id.btnConfiguraCardapio)
    public void onClickBtnConfiguraCardapio() {
        Intent intent = new Intent(this, CardapiosActivity.class);
        intent.putExtra(CardapiosActivity.EXTRA_URL,
                String.format(Locale.getDefault(), "%s/ServiceProduto.svc/Cardapios",
                        mTxtUrlWS.getText().toString()));
        startActivityForResult(intent, SELECIONAR_CARDAPIO);
    }

    @OnClick(R.id.btnConfiguraAmbiente)
    public void onClickBtnConfiguraAmbiente() {
        Intent intent = new Intent(ConfiguracaoActivity.this, AmbientesActivity.class);
        intent.putExtra(CardapiosActivity.EXTRA_URL,
                String.format(Locale.getDefault(), "%s/ServiceProduto.svc/Ambientes/%d",
                        mTxtUrlWS.getText().toString(), mCodigoCardapio));
        startActivityForResult(intent, SELECIONAR_AMBIENTE);
    }

    @OnClick(R.id.btnConfiguraPagamentoMobile)
    public void onClickBtnConfiguraPagamentoMobile() {
        new MaterialDialog.Builder(this)
                .title("Pagamento mobile")
                .items(R.array.formas_pagamento)
                .itemsCallbackSingleChoice(mIndiceFormaPagamento,
                        new MaterialDialog.ListCallbackSingleChoice() {
                            @Override
                            public boolean onSelection(MaterialDialog dialog, View itemView,
                                                       int which, CharSequence text) {
                                mIndiceFormaPagamento = which;
                                return false;
                            }
                        })
                .positiveText("OK")
                .cancelable(false)
                .show();
    }

    @OnClick(R.id.btnCancelar)
    public void onClickBtnCancelar() {
        finish();
    }

    @OnClick(R.id.btnSalvar)
    public void onClickBtnSalvar() {
        View viewRequerida = null;
        String mensagemUsuario = null;

        if (TextUtils.isEmpty(mTxtUrlWS.getText())) {
            viewRequerida = mTxtUrlWS;
            mensagemUsuario = "É necessário configurar a URL do WebService!";
        } else if (mCodigoCardapio == 0) {
            viewRequerida = mBtnConfiguraCardapio;
            mensagemUsuario = "É necessário configurar o cardápio padrão!";
        } else if (mCodigoAmbiente == 0) {
            viewRequerida = mBtnConfiguraAmbiente;
            mensagemUsuario = "É necessário configurar o ambiente padrão!!";
        }

        if (viewRequerida != null) {
            final View finalViewRequerida = viewRequerida;

            App.getInstancia().exibirMensagem(ConfiguracaoActivity.this,
                    MensagemDialogo.MENSAGEM_ALERTA, "Atenção", mensagemUsuario,
                    new MensagemDialogo.OnMensagemClickListener() {
                        @Override
                        public void onConfirmarClick() {
                            if (finalViewRequerida instanceof Button) {
                                finalViewRequerida.performClick();
                            } else {
                                finalViewRequerida.requestFocus();
                            }
                        }

                        @Override
                        public void onCancelarClick() {
                        }
                    });
        } else {
            App.getInstancia().setURL(mTxtUrlWS.getText().toString());
            App.getInstancia().setCodigoCardapio(mCodigoCardapio);
            App.getInstancia().setCodigoAmbiente(mCodigoAmbiente);
            App.getInstancia().setIndicePagamentoMobile(mIndiceFormaPagamento);
            App.getInstancia().setPermitidoAlterarGarcom(mOpcaoPermitirAlterarGarcomHabilitada);

            App.getInstancia().exibirMensagem(ConfiguracaoActivity.this,
                    MensagemDialogo.MENSAGEM_SUCESSO, "Atenção",
                    "Configuração salva com sucesso!",
                    new MensagemDialogo.OnMensagemClickListener() {
                        @Override
                        public void onConfirmarClick() {
                            finish();
                        }

                        @Override
                        public void onCancelarClick() {}
                    });
        }
    }

    public void configurarPermitirAlterarGarcom(View view) {
        final int opcao = mOpcaoPermitirAlterarGarcomHabilitada ? 1 : 0;
        new MaterialDialog.Builder(this)
                .title("Permitir alterar garçom")
                .content("Na confirmação do pedido, permitir que o garçom solicitante seja alterado.")
                .items(R.array.opcoes_permitir_alterar_garcom)
                .itemsCallbackSingleChoice(opcao,
                        new MaterialDialog.ListCallbackSingleChoice() {
                            @Override
                            public boolean onSelection(MaterialDialog dialog, View itemView,
                                    int which, CharSequence text) {
                                mOpcaoPermitirAlterarGarcomHabilitada = which == 1;
                                return false;
                            }
                        })
                .autoDismiss(true)
                .cancelable(false)
                .show();
    }

    public class TestaConexaoWsTask extends AsyncTask<String, Void, String> {
        private final MensagemDialogo dialogo;

        TestaConexaoWsTask() {
            this.dialogo = App.getInstancia().exibirProgresso(ConfiguracaoActivity.this,
                    "Processando", "Testando o endereço informado...");

        }

        @Override
        protected void onPreExecute() {
            dialogo.exibir();
        }

        @Override
        protected String doInBackground(String... params) {
            String mensagem = null;

            String URL = params[0] + "/ServiceUsuario.svc";
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httpget = new HttpGet(URL);
            try {
                HttpResponse response = httpclient.execute(httpget);
                if (response.getStatusLine().getStatusCode() != 200) {
                    mensagem = "Verifique se o endereço está correto e tente novamente.";
                }

            } catch (Exception e) {
                mensagem = String.format("Ocorreu um erro ao testar o endereço informado! %s",
                        e.getMessage());
            }

            return mensagem;
        }

        @Override
        protected void onPostExecute(String mensagem) {
            dialogo.esconder();
            if (mensagem != null) {
                mBtnConfiguraCardapio.setEnabled(false);
                mBtnConfiguraPagamentoMobile.setEnabled(false);
                App.getInstancia().exibirMensagem(ConfiguracaoActivity.this,
                        MensagemDialogo.MENSAGEM_ERRO, "Atenção", mensagem);
            } else {
                mBtnConfiguraCardapio.setEnabled(true);
                mBtnConfiguraPagamentoMobile.setEnabled(true);
                App.getInstancia().exibirMensagem(ConfiguracaoActivity.this,
                        MensagemDialogo.MENSAGEM_SUCESSO, "Atenção", "Teste efetuado com sucesso");
            }

        }

    }
}

package essenti.com.br.MyFast.relatorio;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Filipe Bezerra
 * @version 1.4.0, 04/04/2016
 */
public class ListaVendas {
    @SerializedName("ListarVendasResult")
    public List<Comissao> comissoes;
}

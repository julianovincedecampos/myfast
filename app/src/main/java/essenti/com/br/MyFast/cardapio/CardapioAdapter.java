package essenti.com.br.MyFast.cardapio;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import essenti.com.br.MyFast.R;

/**
 * @author Diego, Filipe Bezerra
 * @version 1.4.0, 16/03/2016
 * @since 1.0.0, 04/10/2015
 */
public class CardapioAdapter extends ArrayAdapter<Cardapio> {
    private LayoutInflater mInflater;

    public CardapioAdapter(Context context, List<Cardapio> cardapios) {
        super(context, 0, cardapios);
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getCodigo();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_lista_cardapio, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.txtNomeCardapio.setText(getItem(position).getDescricao());
        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.txtNomeCardapio) TextView txtNomeCardapio;

        public ViewHolder(View itemView) {
            ButterKnife.bind(this, itemView);
        }
    }
}
